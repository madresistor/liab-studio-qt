/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "interface.h"
#include <QStandardPaths>

/**
 * All Interface need to sub class LsInterface.
 * An Interface is a widget that gather data for user, /Usually/ from a DAQ.
 */

LsInterface::LsInterface(LsBank *bank, QWidget *parent) :
	QMainWindow(parent)
{
	m_bank = bank;
}

/**
 * Path to directory where user configuration are stored.
 * An sub-class can load/store configuration to directory (usually by showing a dialog)
 * @note DO NOT assume that as de-facto configuration path,
 *   this is just a TIP for a starting point
 * @param path to configuration directory
 */
QString LsInterface::configDir()
{
	return QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
}
