/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_SOURCE_H
#define LIAB_STUDIO_INTERFACES_BOX0_SOURCE_H

#include <QObject>
#include <QWidget>
#include "bank.h"
#include <libbox0/libbox0.h>

#include "widgets/table/table.h"
#include "widgets/graph/graph.h"

#include <QJsonObject>

class LsBox0Source : public QWidget
{
	Q_OBJECT

	public:
		LsBox0Source(QWidget *parent);

		/**
		 * return the JSON configuration of the source
		 * @return JSON object containing configuration
		 */
		virtual QJsonObject jsonConfig() = 0;

		/**
		 * Set the JSON configuration to source
		 * @param config JSON Object that contain configuration
		 */
		virtual void setJsonConfig(QJsonObject config) = 0;

	public Q_SLOTS:
		virtual void onStop() = 0;
		/*
		 * -ve means error
		 * 0 means not enabled
		 * +ve mean success
		 */
		virtual int onStart(LsTable *table, LsGraph *graph, bool assign_marker, uint time) = 0;

	public:
		virtual void setDevice(b0_device *dev) = 0;
};

#endif
