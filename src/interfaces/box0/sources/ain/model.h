/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_AIN_MODEL_H
#define LIAB_STUDIO_INTERFACES_BOX0_AIN_MODEL_H

#include <QWidget>
#include <QObject>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QModelIndex>
#include <QObject>
#include <QSize>
#include <QPointer>
#include <QJsonArray>
#include <muParser.h>

#include "widgets/table/table.h"
#include "widgets/graph/graph.h"
#include "widgets/graph/curve.h"

#include <libbox0/libbox0.h>

struct LsBox0AinModelEntry
{
	QString channel;

	bool enable;
	QString name;
	QString type;
	int averaging;

	/* transferFunction */
	struct {
		QString equation;
		mu::Parser *parser;
		double output;
	} transferFunc;

	struct {
		size_t total_sample_limit, sample_processed;
		size_t row;

		struct {
			int column;
			qreal value;
			int averaging_i;
		} y;

		struct {
			int column;
		} x;
	} feed;

	enum PhotoGateState {
		UNKNOWN, /**< State undefined */
		SYNC, /**< synchronisation event sense waiting */
		READY /** Processing events */
	};

	union {
		struct {
			qreal threshold;
			qreal trigger_time;

			/* Blocked zone (means the gate is block by black strip) */
			bool blocked;

			enum PhotoGateState state;

			/* Index of the event column */
			int state_column;
		} photo_gate;
	} sensor_specific;
};

class LsBox0AinModel : public QAbstractTableModel
{
	Q_OBJECT

	public:
		LsBox0AinModel(b0_ain *ain, QWidget *parent = Q_NULLPTR);
		QVariant headerData(int section, Qt::Orientation orientation, int role) const;
		QVariant data(const QModelIndex &index, int role) const;
		int columnCount(const QModelIndex &parent) const;
		int rowCount(const QModelIndex &parent) const;
		Qt::ItemFlags flags(const QModelIndex & index) const;
		bool setData(const QModelIndex & index, const QVariant & value, int role);

		QString testEquation(int index);
		QModelIndex transferFunctionIndex(int row);

		QJsonArray toJsonArray();
		void fromJsonArray(QJsonArray arr);

		bool anyColumnEnabled();
		void userEditable(bool e);
		bool anyEnabledColumnRequireStreaming();
		void acceptAveraging(bool);

	Q_SIGNALS:
		void errorOccured(QString &details);

	/* stream */
	private:
		void feed_stream(LsBox0AinModelEntry *entry,
			float *data, size_t size, size_t offset, size_t stride);

		void feed_stream_photogate(LsBox0AinModelEntry *entry,
				qreal time, qreal value);
		void feed_stream_equation(LsBox0AinModelEntry *entry,
				qreal time, qreal value);

	public:
		QVector<uint> prepareColumns_stream(LsTable *table, LsGraph *graph,
			bool assign_marker,  uint time, uint sampling_freq);

		void feed_stream(float *data, size_t size);

	/* snapshot */
	private:
		void feed_snapshot(LsBox0AinModelEntry *entry,
			float *data, size_t size, size_t offset, size_t stride, qreal time);

	public:
		QVector<uint> prepareColumns_snapshot(LsTable *table, LsGraph *graph,
			bool assign_marker,  uint time, uint speed);

		void feed_snapshot(float *data, size_t size, qreal time);

	private:
		void prepareColumns(LsTable *table, LsGraph *graph, bool assign_marker);

		QList<LsBox0AinModelEntry *> entries;
		bool editable = true;
		uint m_sampling_freq;
		QPointer<LsTable> m_table;
		QVector<uint> m_chan_seq;
		bool accept_averaging;
};

#endif
