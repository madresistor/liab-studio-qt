/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"

#include "extra/multi_proc.h"
#include "extra/sigproc/array.h"

QVector<uint> LsBox0AinModel::prepareColumns_snapshot(LsTable *table,
	LsGraph *graph, bool assign_marker, uint time, uint speed)
{
	prepareColumns(table, graph, assign_marker);

	size_t chan_seq_len = m_chan_seq.size();
	for (size_t i = 0; i < chan_seq_len; i++) {
		LsBox0AinModelEntry *entry = entries.at(m_chan_seq.at(i));

		entry->feed.sample_processed = 0;
		entry->feed.total_sample_limit = time * speed;

		/* for future use, store the sampling frequency
		 *   at which the data was captured */
		LsTableDataColumn *y_col = table->tableData->columns.at(entry->feed.y.column);
		y_col->sampling_freq = speed;
	}

	return m_chan_seq;
}

void LsBox0AinModel::feed_snapshot(float *data, size_t size, qreal time)
{
	/* closed while running? */
	if (!m_table) {
		return;
	}

	size_t stride = (size_t) m_chan_seq.size();

	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < stride; i++) {
		LsBox0AinModelEntry *entry = entries.at(m_chan_seq.at(i));
		feed_snapshot(entry, data, size, i, stride, time);
	}

	/* signal tableData that data has been updated */
	m_table->update();
}


void LsBox0AinModel::feed_snapshot(LsBox0AinModelEntry *entry,
	float *data, size_t size, size_t offset, size_t stride, qreal time)
{
	/* keep a bar on total samples to recevied */
	if (entry->feed.total_sample_limit > 0) {
		if (entry->feed.sample_processed > entry->feed.total_sample_limit) {
			return;
		}
	}

	qreal value = ls_array_avg(data, size, offset, stride);

	/* only equations are possible, not Photo Gate */
	if (entry->transferFunc.parser != Q_NULLPTR) {
		entry->transferFunc.output = value;
		value = entry->transferFunc.parser->Eval();
	}

	/* current row */
	int row = entry->feed.row++;
	entry->feed.sample_processed++;
	/* write data */
	m_table->setDataDirect(row, entry->feed.x.column, time);
	m_table->setDataDirect(row, entry->feed.y.column, value);
}
