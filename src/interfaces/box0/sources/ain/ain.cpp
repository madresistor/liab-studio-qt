/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ain.h"
#include <QDebug>
#include <QMessageBox>

#include "delegates/type.h"
#include "delegates/averaging.h"

#include "interfaces/box0/extra/result_code.h"

LsBox0Ain::LsBox0Ain(QWidget *parent) :
	LsBox0Source(parent)
{
	ui.setupUi(this);

	/* when mode changed, select the appropriate mode */
	connect(ui.mode, SIGNAL(currentChanged(int)), SLOT(mode_changed()));
}

void LsBox0Ain::mode_changed()
{
	/* model is NULL, nothing can be done */
	if (model == Q_NULLPTR) {
		return;
	}

	QWidget *curr = ui.mode->currentWidget();

	if (curr == ui.snapshot) {
		model->acceptAveraging(false);
	} else if (curr == ui.stream) {
		model->acceptAveraging(true);
	} else {
		/* ?? */
	}
}

void LsBox0Ain::setDevice(b0_device *dev)
{
	if (dev == NULL) {
		/* remove device */

		if (module != NULL) {
			onStop();

			LsBox0ResultCode::handleInternal(
				b0_ain_close(module), "unable to unload AIN");
		}

		module = NULL;

		/* TODO: delete sampler */
	} else if (B0_ERR_RC(b0_ain_open(dev, &module, 0))) {
		qDebug() << "no ain in device";
		module = NULL;
	}

	setEnabled(module != NULL);

	if (module == NULL) {
		qDebug() << "no AIN module";
		model = NULL;
		ui.table->setModel(Q_NULLPTR);
		return;
	}

	model = new LsBox0AinModel(module, this);
	ui.table->setModel(model);
	ui.table->setItemDelegateForColumn(2,
		new LsBox0AinDelegateAveraging(this));
	ui.table->setItemDelegateForColumn(3,
		new LsBox0AinDelegateType(this));

	connect(model, SIGNAL(errorOccured(QString &)),
		this, SLOT(errorOccured(QString &)));

	bool stream = module->stream.count > 0;
	ui.stream->setEnabled(stream);
	if (stream) {
		setDevice_stream();
	}

	bool snapshot = module->snapshot.count > 0;
	ui.snapshot->setEnabled(snapshot);
	if (snapshot) {
		setDevice_snapshot();
	}

	/* force update of mode */
	mode_changed();
}

int LsBox0Ain::onStart(LsTable *table, LsGraph *graph,
	bool assign_marker, uint time)
{
	if (module == NULL) {
		qDebug() << "no AIN module";
		return 0;
	}

	if (model == Q_NULLPTR) {
		qDebug() << "no AIN model";
		return -1;
	}

	if (!model->anyColumnEnabled()) {
		qDebug() << "no column enabled";
		return 0;
	}

	if (ui.mode->currentWidget() == ui.stream) {
		int mode = onStart_stream(table, graph, assign_marker, time);
		if (mode != 1) {
			return mode;
		}
	} else if (ui.mode->currentWidget() == ui.snapshot) {
		if (model->anyEnabledColumnRequireStreaming()) {
			QMessageBox::warning(this, "Streaming required", "Atleast one sensor (Photo Gate?) require streaming");
			return -1;
		}

		int mode = onStart_snapshot(table, graph, assign_marker, time);
		if (mode != 1) {
			return mode;
		}
	} else {
		qWarning() << "What is going on?";
		return -1;
	}

	ui.mode->setEnabled(false);
	m_running = true;
	model->userEditable(false);

	return 1;
}

void LsBox0Ain::onStop()
{
	if (!m_running) {
		return;
	}

	if (ui.mode->currentWidget() == ui.stream) {
		onStop_stream();
	} else if (ui.mode->currentWidget() == ui.snapshot) {
		onStop_snapshot();
	} else {
		qWarning() << "What is going on?";
	}

	ui.mode->setEnabled(true);
	model->userEditable(true);
	m_running = false;
}

void LsBox0Ain::errorOccured(QString &details)
{
	QMessageBox::warning(this, tr("Error"), details);
}

QJsonObject LsBox0Ain::jsonConfig()
{
	return {
		{"channels", model->toJsonArray()},
		{"stream", QJsonObject({
			{"bitsize", ui.stream_bitsize->currentData().toInt()},
			{"speed", ui.stream_speed->currentData().toInt()}
		})},
		{"snapshot", QJsonObject({
			{"bitsize", ui.snapshot_bitsize->currentData().toInt()},
			{"speed", ui.snapshot_speed->value()}
		})}
	};
}

void LsBox0Ain::setJsonConfig(QJsonObject config)
{
	QJsonObject stream, snapshot;
	int bitsize, speed;

	bitsize = stream.value("bitsize").toInt(0);
	if (bitsize > 0) {
		/* search for bitsize and set as current */
		for (int i = 0; i < ui.stream_bitsize->count(); i++) {
			if (ui.stream_bitsize->itemData(i).toInt() == bitsize) {
				ui.stream_bitsize->setCurrentIndex(i);
				break;
			}
		}
	}

	speed = stream.value("speed").toInt(0);
	if (speed > 0) {
		/* search for the speed and set it as current */
		for (int i = 0; i < ui.stream_speed->count(); i++) {
			if (ui.stream_speed->itemData(i).toInt() == speed) {
				ui.stream_speed->setCurrentIndex(i);
				break;
			}
		}
	}

	bitsize = snapshot.value("bitsize").toInt(0);
	if (bitsize > 0) {
		/* search for bitsize and set as current */
		for (int i = 0; i < ui.snapshot_bitsize->count(); i++) {
			if (ui.snapshot_bitsize->itemData(i).toInt() == bitsize) {
				ui.snapshot_bitsize->setCurrentIndex(i);
				break;
			}
		}
	}

	speed = snapshot.value("speed").toInt(5);
	if (speed > 0) {
		speed = qMax(1, qMin(speed, 50));
		ui.snapshot_speed->setValue(speed);
	}

	model->fromJsonArray(config.value("channels").toArray());
}
