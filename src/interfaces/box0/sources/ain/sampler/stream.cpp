/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stream.h"
#include "interfaces/box0/extra/result_code.h"

LsBox0AinStreamSampler::LsBox0AinStreamSampler(QWidget *parent) :
	QThread(parent)
{
}

/**
 * Start the sampler
 * @param mod Module to sample data from
 * @param count Number of sample per @a output() to generate
 */
void LsBox0AinStreamSampler::start(b0_ain *mod, uint count)
{
	this->module = mod;
	this->count = count;
	QThread::start();
}

/* FIXME: memory allocation is an expensive operation, reuse memory */

/**
 * Read data from AIN stream and push it to @a output()
 * @note thread will exists when @a requestInterruption() is called
 * @note the array allocated by this method should be free'd
 *  (using delete[]) by the receiver of the data
 */
void LsBox0AinStreamSampler::run()
{
	float *values = NULL;
	size_t filled = 0;

	Q_FOREVER {
		if (isInterruptionRequested()) {
			break;
		}

		if (values == NULL) {
			values = new float [count];
		}

		size_t rem = count - filled;
		size_t ret;
		b0_result_code r;

		r = b0_ain_stream_read_float(module, &values[filled], rem, &ret);
		if (B0_ERR_RC(r)) {
			LsBox0ResultCode::handleInternal(r, "failed to b0_ain_stream_read_float()");
			break;
		}

		if (isInterruptionRequested()) {
			break;
		}

		filled += ret;

		if (!(filled < count)) {
			/* NOTE: the "values" array need to be freed by the receiver */
			Q_EMIT output(values, count);
			values = NULL;
			filled = 0;
		} else {
			/* wait since there is no data,
			 * better wait then to waste computation */
			msleep(10);
		}
	}

	if (values) {
		delete values;
	}
}

void LsBox0AinStreamSampler::dispose(float *data)
{
	delete[] data;
}
