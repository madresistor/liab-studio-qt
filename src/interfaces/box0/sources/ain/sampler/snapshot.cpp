/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "snapshot.h"
#include <QTimer>
#include <QDateTime>
#include "interfaces/box0/extra/result_code.h"

LsBox0AinSnapshotSampler::LsBox0AinSnapshotSampler(QWidget *parent) :
	QThread(parent)
{
}

/**
 * Start the sampler.
 * @param ain AIN module
 */
void LsBox0AinSnapshotSampler::start(b0_ain *ain, uint rate, uint samples)
{
	module = ain;
	this->rate = rate;
	this->sample_count = samples;
	QThread::start();
}

/**
 * Request the thread to exit.
 */
void LsBox0AinSnapshotSampler::requestInterruption()
{
	exit(0);
	b0_ain_stream_stop(module);
	QThread::requestInterruption();
}

/**
 * Sample data from module and pass on the data to "output" signal.
 * It is the duty of the receiver to free the memory (of the values).
 * This method will query the bitsize and speed.
 *  and accordingly calculate the maximum number of samples to fetch.
 * @note the array allocated by this method should be free'd
 *  (using delete[]) by the receiver of the data
 */
void LsBox0AinSnapshotSampler::sample()
{
	b0_result_code r;

	float *data = new float [sample_count];

	qint64 before = QDateTime::currentMSecsSinceEpoch();

	r = b0_ain_snapshot_start_float(module, data, sample_count);
	if (!LsBox0ResultCode::handleInternal(r, "b0_ain_snapshot_start_float failed")) {
		/* TODO: "data" leak */
		return;
	}

	qint64 after = QDateTime::currentMSecsSinceEpoch();
	qint64 mid = (before + after) / 2;
	qreal time  = (mid - start_time) / 1000.0;

	Q_EMIT output(data, sample_count, time);
}

void LsBox0AinSnapshotSampler::run()
{
	QTimer timer;

	start_time = QDateTime::currentMSecsSinceEpoch();

	/* NOTE: directly calling the thread object method sample() */
	connect(&timer, SIGNAL(timeout()), this, SLOT(sample()), Qt::DirectConnection);
	timer.start(1000.0 / rate);

	/* collect for first time */
	sample();

	exec();
}

void LsBox0AinSnapshotSampler::dispose(float *data)
{
	delete[] data;
}
