/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_AIN_SNAPSHOT_SAMPLER_H
#define LIAB_STUDIO_INTERFACES_BOX0_AIN_SNAPSHOT_SAMPLER_H

#include <QWidget>
#include <QObject>
#include <QThread>
#include <libbox0/libbox0.h>
#include <QTimer>
#include <QMutex>

/**
 * AIN static mode sampler.
 * It can be used to sample data form the specified module
 */
class LsBox0AinSnapshotSampler : public QThread
{
	Q_OBJECT

	public:
		LsBox0AinSnapshotSampler(QWidget *parent);
		void start(b0_ain *ain, uint speed, uint size);
		void requestInterruption();
		void dispose(float *);

	Q_SIGNALS:
		/**
		 * Output data from the sampler
		 * @param data pointer to data
		 * @param size number of samples in @a data
		 * @note receiver need to delete @a delete
		 */
		void output(float *data, uint size, qreal time);

	private Q_SLOTS:
		void sample();

	protected:
		void run();
		b0_ain *module;
		uint rate;
		uint sample_count;
		qint64 start_time;
};

#endif
