/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include "interfaces/box0/extra/sensor.h"

#include "extra/multi_proc.h"

QVector<uint> LsBox0AinModel::prepareColumns_stream(LsTable *table,
	LsGraph *graph, bool assign_marker, uint time, uint sampling_freq)
{
	prepareColumns(table, graph, assign_marker);

	m_sampling_freq = sampling_freq;

	size_t chan_seq_len = m_chan_seq.size();
	size_t samples_per_channel = (time * sampling_freq) / chan_seq_len;
	qreal sampling_freq_per_ch = sampling_freq / (qreal) chan_seq_len;
	for (size_t i = 0; i < chan_seq_len; i++) {
		LsBox0AinModelEntry *entry = entries.at(m_chan_seq.at(i));

		entry->feed.y.averaging_i = 0;
		entry->feed.y.value = 0;

		entry->feed.sample_processed = 0;
		entry->feed.total_sample_limit = samples_per_channel;

		if (entry->averaging < 1) {
			entry->averaging = 1;
		}

		/* for future use, store the sampling frequency
		 *   at which the data was captured */
		LsTableDataColumn *y_col = table->tableData->columns.at(entry->feed.y.column);
		y_col->sampling_freq = sampling_freq_per_ch / entry->averaging;
	}

	return m_chan_seq;
}

void LsBox0AinModel::feed_stream(float *data, size_t size)
{
	/* closed while running? */
	if (!m_table) {
		return;
	}

	size_t stride = (size_t) m_chan_seq.size();

	MULTI_PROC_FOR_LOOP
	for (size_t i = 0; i < stride; i++) {
		LsBox0AinModelEntry *entry = entries.at(m_chan_seq.at(i));
		feed_stream(entry, data, size, i, stride);
	}

	/* signal tableData that data has been updated */
	m_table->update();
}

void LsBox0AinModel::feed_stream(LsBox0AinModelEntry *entry,
	float *data, size_t size, size_t offset, size_t stride)
{
	/* perform averaging style iteration */
	for (size_t i = offset; i < size; i += stride) {

		/* keep a bar on total samples to recevied */
		if (entry->feed.total_sample_limit > 0) {
			if (entry->feed.sample_processed > entry->feed.total_sample_limit) {
				break;
			}
		}

		entry->feed.y.value += data[i];
		entry->feed.y.averaging_i++;

		if (entry->feed.y.averaging_i >= entry->averaging) {
			qreal value = entry->feed.y.value / entry->feed.y.averaging_i;

			/* x value */
			qreal time = stride * entry->feed.sample_processed;
			time += offset;
			time /= m_sampling_freq;

			if (entry->type == "PHOTO_GATE") {
				feed_stream_photogate(entry, time, value);
			} else {
				feed_stream_equation(entry, time, value);
			}

			entry->feed.y.value = 0;
			entry->feed.y.averaging_i = 0;
		}

		entry->feed.sample_processed++;
	}
}

void LsBox0AinModel::feed_stream_equation(LsBox0AinModelEntry *entry,
		qreal time, qreal value)
{
	/* we have equation? */
	if (entry->transferFunc.parser != Q_NULLPTR) {
		entry->transferFunc.output = value;
		value = entry->transferFunc.parser->Eval();
	}

	/* current row */
	int row = entry->feed.row++;

	/* write data */
	m_table->setDataDirect(row, entry->feed.x.column, time);
	m_table->setDataDirect(row, entry->feed.y.column, value);
}

void LsBox0AinModel::feed_stream_photogate(LsBox0AinModelEntry *entry,
		qreal time, qreal value)
{
	bool blocked = (value < entry->sensor_specific.photo_gate.threshold);

	if (entry->sensor_specific.photo_gate.state == LsBox0AinModelEntry::UNKNOWN) {
		/* inital state of the gate is unknown, storing it */
		entry->sensor_specific.photo_gate.blocked = blocked;
		entry->sensor_specific.photo_gate.state = LsBox0AinModelEntry::SYNC;
		return;
	} else if (entry->sensor_specific.photo_gate.state == LsBox0AinModelEntry::SYNC) {
		if (entry->sensor_specific.photo_gate.blocked != blocked) {
			/* got a synchronising event (first event) */
			entry->sensor_specific.photo_gate.trigger_time = time;
			entry->sensor_specific.photo_gate.blocked = blocked;
			entry->sensor_specific.photo_gate.state = LsBox0AinModelEntry::READY;
		}

		return;
	}

	if (entry->sensor_specific.photo_gate.blocked == blocked) {
		/* state has not changed. */
		return;
	}

	qreal col_value = time - entry->sensor_specific.photo_gate.trigger_time;
	qreal col_state = entry->sensor_specific.photo_gate.blocked ? 0 : 1;

	m_table->setDataDirect(entry->feed.row, entry->feed.x.column, time);
	m_table->setDataDirect(entry->feed.row, entry->feed.y.column, col_value);
	m_table->setDataDirect(entry->feed.row,
		entry->sensor_specific.photo_gate.state_column, col_state);

	/* store the new results */
	entry->sensor_specific.photo_gate.trigger_time = time;
	entry->sensor_specific.photo_gate.blocked = blocked;
	entry->feed.row++;
}
