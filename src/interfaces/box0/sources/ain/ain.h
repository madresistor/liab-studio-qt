/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_AIN_H
#define LIAB_STUDIO_INTERFACES_BOX0_AIN_H

#include <QWidget>
#include <QObject>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QModelIndex>
#include <QObject>
#include <QSize>
#include <QTableView>

#include <libbox0/libbox0.h>

#include "interfaces/box0/sources/source/source.h"
#include "sampler/snapshot.h"
#include "sampler/stream.h"

#include "model.h"

#include "ui_box0_ain.h"

class LsBox0Ain : public LsBox0Source
{
	Q_OBJECT

	public:
		LsBox0Ain(QWidget *parent = Q_NULLPTR);
		void setDevice(b0_device *dev);
		QJsonObject jsonConfig();
		void setJsonConfig(QJsonObject config);

	public Q_SLOTS:
		void errorOccured(QString &details);

	private Q_SLOTS:
		void mode_changed();

	public:
		void onStop();
		int onStart(LsTable *table, LsGraph *graph, bool assign_marker, uint time);

	/* stream */
	private:
		void setDevice_stream();
		void onStop_stream();
		int onStart_stream(LsTable *table, LsGraph *graph, bool assign_marker, uint time);

	private Q_SLOTS:
		void updateSpeed_stream();
		void feed_stream(float *data, uint size);

	/* snapshot */
	private:
		void setDevice_snapshot();
		void onStop_snapshot();
		int onStart_snapshot(LsTable *table, LsGraph *graph, bool assign_marker, uint time);

	private Q_SLOTS:
		void feed_snapshot(float *data, uint size, qreal time);

	private:
		Ui::LsBox0Ain ui;
		b0_ain *module = NULL;

		LsBox0AinModel *model = Q_NULLPTR;

		bool m_running = false;

		LsBox0AinSnapshotSampler *sampler_snapshot;
		LsBox0AinStreamSampler *sampler_stream;
};

#endif
