/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ain.h"

#include "interfaces/box0/extra/result_code.h"

void LsBox0Ain::setDevice_stream()
{
	for (size_t i = 0; i < module->stream.count; i++) {
		unsigned int bitsize = module->stream.values[i].bitsize;
		if (ui.stream_bitsize->findData(bitsize) != -1) {
			continue;
		}
		ui.stream_bitsize->addItem(QString("%1 bits").arg(bitsize), bitsize);
	}

	connect(ui.stream_bitsize, SIGNAL(currentIndexChanged(int)),
		this, SLOT(updateSpeed_stream()));
	ui.stream_bitsize->setCurrentIndex(0);
	updateSpeed_stream();

	/* sampler */
	sampler_stream = new LsBox0AinStreamSampler(this);
	connect(sampler_stream, SIGNAL(output(float *, uint)),
			this, SLOT(feed_stream(float *, uint)),
			Qt::QueuedConnection);
}

void LsBox0Ain::updateSpeed_stream()
{
	ui.stream_speed->clear();

	unsigned int bs = ui.stream_bitsize->currentData().toInt();
	Q_ASSERT(bs > 0);

	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
		*bitsize_speeds = NULL, *item;

	for (size_t i = 0; i < module->stream.count; i++) {
		item = &module->stream.values[i];
		if (item->bitsize == bs) {
			bitsize_speeds = item;
			break;
		}
	}

	Q_ASSERT(bitsize_speeds != NULL);

	for (size_t i = 0; i < bitsize_speeds->speed.count; i++) {
		unsigned long speed = bitsize_speeds->speed.values[i];
		QString value_str = (speed >= 1000) ?
			QString("%1 KSPS").arg(speed / 1000.0) :
			QString("%1 SPS").arg(speed);
		ui.stream_speed->addItem(value_str, (qlonglong) speed);
	}
}

int LsBox0Ain::onStart_stream(LsTable *table, LsGraph *graph,
	bool assign_marker, uint time)
{
	unsigned int bs = ui.stream_bitsize->currentData().toUInt();
	unsigned long speed = ui.stream_speed->currentData().toUInt();

	Q_ASSERT(bs > 0);
	Q_ASSERT(speed > 0);

	b0_result_code rc;

	QVector<unsigned int> _chan_seq =
		model->prepareColumns_stream(table, graph, assign_marker, time, speed);

	unsigned int *chan_seq = _chan_seq.data();
	uint8_t chan_seq_len = _chan_seq.size();

	rc = b0_ain_stream_prepare(module);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN prepare failed")) {
		return -1;
	}

	rc = b0_ain_chan_seq_set(module, chan_seq, chan_seq_len);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN fail to set channel sequence")) {
		return -1;
	}

	rc = b0_ain_bitsize_speed_set(module, bs, speed);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN fail to set bitsize,speed")) {
		return -1;
	}

	rc = b0_ain_stream_start(module);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN start failed")) {
		return -1;
	}

	size_t samples_per_callback = speed / 10;
	samples_per_callback -= samples_per_callback % chan_seq_len;
	sampler_stream->start(module, samples_per_callback);

	return 1;
}

void LsBox0Ain::feed_stream(float *data, uint size)
{
	model->feed_stream(data, size);
	sampler_stream->dispose(data);
}

void LsBox0Ain::onStop_stream()
{
	/* signal and wait for the thread to exit */
	sampler_stream->requestInterruption();
	sampler_stream->wait();

	if (m_running && module != NULL) {
		LsBox0ResultCode::handleInternal(
			b0_ain_stream_stop(module), "unable to stop AIN stream");
	}
}
