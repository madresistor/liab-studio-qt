/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "type.h"
#include <QComboBox>
#include <Qt>
#include <QDebug>
#include <QApplication>
#include "../model.h"
#include "interfaces/box0/extra/sensor.h"

LsBox0AinDelegateType::LsBox0AinDelegateType(QWidget *parent) :
	QItemDelegate(parent)
{
}

QWidget *LsBox0AinDelegateType::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex & /* index */) const
{
	QComboBox *cmb = new QComboBox(parent);
	cmb->setInsertPolicy(QComboBox::InsertAtBottom);

	Q_FOREACH(const LsBox0Sensor::Ain &i, LsBox0Sensor::ain) {
		cmb->addItem(i.title, i.id);
	}

	connect(cmb, SIGNAL(activated(int)), this, SLOT(commitAndCloseEditor()));

	return cmb;
}

void LsBox0AinDelegateType::commitAndCloseEditor()
{
	QWidget *editor = qobject_cast<QWidget *>(sender());
	Q_EMIT commitData(editor);
	Q_EMIT closeEditor(editor);
}

void LsBox0AinDelegateType::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	QString id = index.model()->data(index, Qt::EditRole).toString();
	QComboBox *cmb = static_cast<QComboBox *>(editor);
	int _index = LsBox0Sensor::sensor_index(id, LsBox0Sensor::ain);
	cmb->setCurrentIndex(_index);
}

void LsBox0AinDelegateType::setModelData(QWidget *editor,
	QAbstractItemModel *model, const QModelIndex &index) const
{
	QComboBox *cmb = static_cast<QComboBox *>(editor);
	QString id = cmb->currentData().toString();
	qDebug() << "current ID is " << id;

	model->setData(index, id, Qt::EditRole);

	/* WARN: static cast, assuming model is `LsBox0AinModel' */
	QModelIndex tfnIndex = static_cast<LsBox0AinModel *>(model)
		->transferFunctionIndex(index.row());
	int _index = LsBox0Sensor::sensor_index(id, LsBox0Sensor::ain);
	model->setData(tfnIndex, LsBox0Sensor::ain.at(_index).func, Qt::EditRole);
}

void LsBox0AinDelegateType::updateEditorGeometry(QWidget *editor,
	const QStyleOptionViewItem &option, const QModelIndex & /* index */) const
{
	editor->setGeometry(option.rect);
}

void LsBox0AinDelegateType::paint(QPainter *painter,
	const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(option.rect, option.palette.highlight());
	}

	painter->save();
	painter->setRenderHint(QPainter::Antialiasing, true);
	if (option.state & QStyle::State_Selected) {
		painter->setPen(option.palette.color(QPalette::HighlightedText));
		painter->setBrush(option.palette.highlightedText());
	} else {
		painter->setPen(option.palette.color(QPalette::Text));
		painter->setBrush(option.palette.text());
	}

	QStyle* style = QApplication::style();
	QRect textRect = style->subElementRect(QStyle::SE_ItemViewItemText,
		&option);

	const QString &id = index.data().toString();
	int sensor_index = LsBox0Sensor::sensor_index(id, LsBox0Sensor::ain);
	const QString &title = LsBox0Sensor::ain.at(sensor_index).title;
	painter->drawText(textRect, option.displayAlignment, title);
	painter->restore();
}
