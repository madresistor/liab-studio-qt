/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "averaging.h"
#include <QSpinBox>
#include <QPainter>

LsBox0AinDelegateAveraging::LsBox0AinDelegateAveraging(QWidget *parent) :
	QItemDelegate(parent)
{
}

QWidget *LsBox0AinDelegateAveraging::createEditor(QWidget *parent,
	const QStyleOptionViewItem &/* option */, const QModelIndex & /* index */) const
{
	QSpinBox *cmb = new QSpinBox(parent);
	cmb->setRange(1, 1000000);
	cmb->setSpecialValueText("Disabled");
	cmb->setSuffix(" Times");
	return cmb;
}

void LsBox0AinDelegateAveraging::setEditorData(QWidget *editor,
	const QModelIndex &index) const
{
	int value = index.model()->data(index, Qt::EditRole).toInt();
	QSpinBox *cmb = static_cast<QSpinBox *>(editor);
	cmb->setValue(value);
}

void LsBox0AinDelegateAveraging::setModelData(QWidget *editor,
	QAbstractItemModel *model, const QModelIndex &index) const
{
	QSpinBox *cmb = static_cast<QSpinBox *>(editor);
	model->setData(index, cmb->value(), Qt::EditRole);
}

void LsBox0AinDelegateAveraging::updateEditorGeometry(QWidget *editor,
	const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	editor->setGeometry(option.rect);
}

void LsBox0AinDelegateAveraging::paint(QPainter * painter,
	const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(option.rect, option.palette.highlight());
	}

	painter->save();
	painter->setRenderHint(QPainter::Antialiasing, true);
	painter->setPen(Qt::NoPen);
	if (option.state & QStyle::State_Selected) {
		painter->setBrush(option.palette.highlightedText());
	}

	uint avg = index.model()->data(index, Qt::DisplayRole).toUInt();
	QString text = (avg > 1) ? QString("%1 Times").arg(avg) : "Disabled";

	QStyleOptionViewItem myOption = option;
	myOption.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;

	drawDisplay(painter, myOption, myOption.rect, text);
	drawFocus(painter, myOption, myOption.rect);

	painter->restore();
}
