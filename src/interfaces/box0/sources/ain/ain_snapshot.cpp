/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ain.h"

#include "interfaces/box0/extra/result_code.h"

void LsBox0Ain::setDevice_snapshot()
{
	for (size_t i = 0; i < module->snapshot.count; i++) {
		unsigned int bitsize = module->snapshot.values[i].bitsize;
		if (ui.snapshot_bitsize->findData(bitsize) != -1) {
			continue;
		}
		ui.snapshot_bitsize->addItem(QString("%1 bits").arg(bitsize), bitsize);
	}

	ui.snapshot_bitsize->setCurrentIndex(0);

	/* sampler */
	sampler_snapshot = new LsBox0AinSnapshotSampler(this);
	connect(sampler_snapshot, SIGNAL(output(float *, uint, qreal)),
			this, SLOT(feed_snapshot(float *, uint, qreal)),
			Qt::QueuedConnection);
}

int LsBox0Ain::onStart_snapshot(LsTable *table, LsGraph *graph,
	bool assign_marker, uint time)
{
	/* get the heighest frequency possible */
	unsigned int bs = ui.snapshot_bitsize->currentData().toUInt();
	unsigned long speed = ui.snapshot_speed->value();

	Q_ASSERT(bs > 0);
	Q_ASSERT(speed > 0);

	QVector<unsigned int> _chan_seq =
		model->prepareColumns_snapshot(table, graph, assign_marker, time, speed);

	unsigned int *chan_seq = _chan_seq.data();
	uint8_t chan_seq_len = _chan_seq.size();

	struct b0_ain::b0_ain_mode_bitsize_speeds::b0_ain_bitsize_speeds
		*bitsize_speeds = NULL, *item;

	for (size_t i = 0; i < module->stream.count; i++) {
		item = &module->stream.values[i];
		if (item->bitsize == bs) {
			bitsize_speeds = item;
			break;
		}
	}

	Q_ASSERT(bitsize_speeds != NULL);

	/* find the largest speed that atleast can sample channels once */
	uint min_actual_speed = speed * chan_seq_len;
	unsigned long got_speed = 0;

	for (size_t i = 0; i < bitsize_speeds->speed.count; i++) {
		unsigned long value = bitsize_speeds->speed.values[i];
		if (value > min_actual_speed && value > got_speed) {
			got_speed = value;
		}
	}

	Q_ASSERT(got_speed > 0);

	b0_result_code rc;

	rc = b0_ain_snapshot_prepare(module);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN prepare failed")) {
		return -1;
	}

	rc = b0_ain_chan_seq_set(module, chan_seq, chan_seq_len);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN fail to set channel sequence")) {
		return -1;
	}

	rc = b0_ain_bitsize_speed_set(module, bs, got_speed);
	if (!LsBox0ResultCode::handleInternal(rc, "AIN fail to set bitsize,speed")) {
		return -1;
	}

	size_t byte_per_sample = (bs + 7) / 8;
	size_t max_count = module->buffer_size / byte_per_sample;

	/* calculate the number of samples can be acquired at the "got_speed" */
	size_t count = (got_speed / speed) * chan_seq_len;
	count = qMin(max_count, count);

	sampler_snapshot->start(module, speed, count);

	return 1;
}

void LsBox0Ain::feed_snapshot(float *data, uint size, qreal time)
{
	model->feed_snapshot(data, size, time);
	sampler_snapshot->dispose(data);
}

void LsBox0Ain::onStop_snapshot()
{
	sampler_snapshot->requestInterruption();
	sampler_snapshot->wait();
}
