/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include <QDebug>
#include <muParser.h>
#include "interfaces/box0/extra/sensor.h"
#include "interfaces/box0/extra/ease.h"

#include "extra/multi_proc.h"

#define HORIZONTAL_HEADER_COUNT 5
static const QVariant horizontal_header[HORIZONTAL_HEADER_COUNT] = {
	"Enable",
	"Name",
	"Averaging",
	"Type",
	"Transfer Function"
};

LsBox0AinModel::LsBox0AinModel(b0_ain *module, QWidget *parent) :
	QAbstractTableModel(parent)
{
	for (uint i = 0; i < module->chan_count; i++) {
		LsBox0AinModelEntry *entry = new LsBox0AinModelEntry();
		if (module->label.chan[i] != NULL) {
			entry->channel = utf8_to_qstring(module->label.chan[i]);
		} else {
			entry->channel = QString("CH%1").arg(i);
		}

		entry->enable = false;
		entry->name = entry->channel;
		entry->averaging = 1;
		entry->type = "CUSTOM_EQUATION";
		entry->transferFunc.equation = QString::null;
		entry->transferFunc.parser = Q_NULLPTR;

		entries.append(entry);
	}
}

/* convert name model index to transfer function index */
QModelIndex LsBox0AinModel::transferFunctionIndex(int row)
{
	return index(row, 4);
}

/*
 * Columns:
 * 		enable: boolean, rw, default-false
 * 		channel: ro, string
 * 		name: rw, string, dropdown-with-default-selectable, custom text support
 * 		transfer-equation: string, rw, depends-`name'-if-selected-from-selectable
 *
 * Rows: the number of channels supported by devices
 */

int LsBox0AinModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return entries.size();
}

int LsBox0AinModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return HORIZONTAL_HEADER_COUNT;
}

QVariant LsBox0AinModel::data(const QModelIndex &index, int role) const
{
	int row = index.row();
	if (row >= 0 && row < entries.size()) {
		const LsBox0AinModelEntry *col = entries.at(row);

		int column = index.column();
		if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
			switch(column) {
			case 1:
			return QVariant(col->name);
			case 2:
			return QVariant(col->averaging);
			case 3:
			return QVariant(col->type);
			case 4:
			return QVariant(col->transferFunc.equation);
			}
		} else if (role == Qt::CheckStateRole) {
			switch(column) {
			case 0:
			return col->enable ? Qt::Checked : Qt::Unchecked;
			}
		} /* else if (role == Qt::SizeHintRole) {
			switch(column) {
			case 0:
			return QSize(10, 10);
			case 2:
			return QSize(20, 10);
			}
		} */
	}

	return QVariant();
}

QVariant LsBox0AinModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal) {
			if (section >= 0 && section < HORIZONTAL_HEADER_COUNT) {
				return horizontal_header[section];
			}
		} else if (section < entries.size()) {
			return entries.at(section)->channel;
		}
	}

	return QVariant();
}

bool LsBox0AinModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (!editable) {
		return false;
	}

	int row = index.row();
	LsBox0AinModelEntry *col = entries.at(row);

	if (role == Qt::EditRole) {
		switch(index.column()) {
		case 1:
			col->name = value.toString();
		break;
		case 2:
			col->averaging = value.toInt();
		break;
		case 3: {
			col->type = value.toString();
		} break;
		case 4:
			col->transferFunc.equation = value.toString();

			/* delete old parser */
			if (col->transferFunc.parser != Q_NULLPTR) {
				delete col->transferFunc.parser;
				col->transferFunc.parser = Q_NULLPTR;
			}

			if (!col->transferFunc.equation.isEmpty()) {
				QString details = testEquation(row);
				if (!details.isEmpty()) {
					Q_EMIT errorOccured(details);
				}
			}
		break;
		default:
		return false;
		}

	} else if (role == Qt::CheckStateRole) {
		switch(index.column()) {
		case 0:
			col->enable = (value.toInt() == Qt::Checked);
		break;
		default:
		return false;
		}
	}

	Q_EMIT dataChanged(index, index);
	return true;
}

QString LsBox0AinModel::testEquation(int index)
{
	LsBox0AinModelEntry *entry = entries.at(index);

	if (entry->transferFunc.parser == Q_NULLPTR) {
		entry->transferFunc.parser = new mu::Parser();
	}

	try {
		entry->transferFunc.parser->DefineVar("value", &entry->transferFunc.output);
		QString tfn = entry->transferFunc.equation;
		entry->transferFunc.parser->SetExpr(tfn.toStdString());

		//debug info
		std::cout <<
			"function " << tfn.toStdString()
			<< " resulted " << entry->transferFunc.parser->Eval()
			<< " for value = 0" << std::endl;

		return QString();
	} catch (mu::Parser::exception_type &e) {
		std::string msg = e.GetMsg();
		std::string expr = e.GetExpr();
		std::string token = e.GetToken();
		int pos = e.GetPos();
		int code = e.GetCode();
		QString details = QString(
			"MuParser (the equation solving library) has returned error while evaluating the transfter function.\n"
			"Message: %1\n"
			"Formula: %2\n"
			"Token: %3\n"
			"Position: %4\n"
			"Errc: %5")
		.arg(QString::fromStdString(msg))
		.arg(QString::fromStdString(expr))
		.arg(QString::fromStdString(token))
		.arg(pos)
		.arg(code);

		delete entry->transferFunc.parser;
		entry->transferFunc.parser = Q_NULLPTR;

		return details;
	}
}

Qt::ItemFlags LsBox0AinModel::flags(const QModelIndex & index) const
{
	switch(index.column()) {
	case 0:
	return  Qt::ItemIsSelectable |
			Qt::ItemIsEnabled |
			(editable ? Qt::ItemIsUserCheckable : Qt::NoItemFlags);

	case 2:
		/* if averaging is not accepted, do not let user do anything with it */
		if (!accept_averaging) {
			return Qt::ItemIsSelectable;
		}

	case 1: case 3: case 4:
	return  Qt::ItemIsSelectable |
			Qt::ItemIsEnabled |
			(editable ? Qt::ItemIsEditable : Qt::NoItemFlags);

	default:
	return 0;
	}
}

void LsBox0AinModel::prepareColumns(LsTable *table, LsGraph *graph, bool assign_marker)
{
	m_table = table;
	m_chan_seq.clear();

	for (int i = 0; i < entries.size(); i++) {
		LsBox0AinModelEntry *entry = entries.at(i);
		if (entry->enable) {

			/* prepare the entries */
			entry->feed.row = 0;

			entry->feed.x.column = table->appendColumn(entry->name + " time");
			table->tableData->columns.at(entry->feed.x.column)->sig_digits = 6;

			entry->feed.y.column = table->appendColumn(entry->name + " value");

			if (entry->type == "PHOTO_GATE") {
				int index = LsBox0Sensor::sensor_index(entry->type, LsBox0Sensor::ain);
				Q_ASSERT(index != -1);
				LsBox0Sensor::Ain::Parameter outp = LsBox0Sensor::ain.at(index).output;
				qreal threshold = (outp.max - outp.min) / 2;
				entry->sensor_specific.photo_gate.threshold = threshold;
				entry->sensor_specific.photo_gate.trigger_time = NAN;
				entry->sensor_specific.photo_gate.blocked = false;
				entry->sensor_specific.photo_gate.state =
							LsBox0AinModelEntry::UNKNOWN;
				entry->sensor_specific.photo_gate.state_column =
					table->appendColumn(entry->name + " state");
			}

			if (graph != Q_NULLPTR) {
				LsMarker::Type marker = static_cast<LsMarker::Type>(LsMarker::PLUS + i);
				graph->addCurve(entry->name,
					table, entry->feed.x.column,
					table, entry->feed.y.column,
					table->tableData->columns.at(entry->feed.y.column)->color,
					true, LsGraphCurve::DEF_LINE_WIDTH,
					assign_marker, marker);
			}

			m_chan_seq.append(i);
		}
	}
}



bool LsBox0AinModel::anyColumnEnabled()
{
	for (int i = 0; i < entries.size(); i++) {
		if (entries.at(i)->enable) {
			return true;
		}
	}

	return false;
}

void LsBox0AinModel::userEditable(bool e)
{
	beginResetModel();
	editable = e;
	endResetModel();
}

/**
 * Build and return Channel configuration as JSON array
 * @return JSON object containing channels configuration
 */
QJsonArray LsBox0AinModel::toJsonArray()
{
	QJsonArray ch_list;

	for (int i = 0; i < entries.count(); i++) {
		LsBox0AinModelEntry *entry = entries.at(i);

		QJsonObject ch {
			{"enable", entry->enable},
			{"name", entry->name}
		};

		if (entry->averaging > 1) {
			ch["averaging"] = entry->averaging;
		}

		if (entry->type == "CUSTOM_EQUATION") {
			if (!entry->transferFunc.equation.isEmpty()) {
				ch["type"] = "CUSTOM_EQUATION";
				ch["transfer-func"] = entry->transferFunc.equation;
			}
		} else if (entry->type == "PHOTO_GATE") {
			ch["type"] = "PHOTO_GATE";
		} else {
			ch["type"] = entry->type;
			ch["transfer-func"] = entry->transferFunc.equation;
		}

		ch_list.push_back(ch);
	}

	return ch_list;
}

/**
 * Read mode data (channels) from @a ch_list Array
 * @param ch_list JSON Array of channel configuration
 */
void LsBox0AinModel::fromJsonArray(QJsonArray ch_list)
{
	int count = qMin(ch_list.count(), entries.count());

	beginResetModel();

	for (int i = 0; i < count; i++) {
		QJsonObject ch = ch_list.at(i).toObject();
		LsBox0AinModelEntry *entry = entries.at(i);

		entry->enable = ch.value("enable").toBool(entry->enable);
		entry->name = ch.value("name").toString(entry->name);
		entry->averaging = ch.value("averaging").toInt(entry->averaging);

		QJsonValue type = ch.value("type");
		if (type.isString()) {
			QString id = type.toString();
			bool found = LsBox0Sensor::sensor_exists(id, LsBox0Sensor::ain);
			if (found) {
				entry->type = id;
				entry->transferFunc.equation = ch.value("transfer-func").toString();
			} else {
				/* we have problem!
				 *  We have equation in json but no known sensor equivalent.
				 * What best we can do is,
				 *  assume that the transfer-func as CUSTOM_EQUATION.
				 */
				qWarning() << "AIN:" << id << "not found, make it a custom equation";
				entry->type = "CUSTOM_EQUATION";
				entry->transferFunc.equation = ch.value("transfer-func").toString();
			}
		} else {
			/* just try to make it a Custom equation */
			entry->type = "CUSTOM_EQUATION";
			entry->transferFunc.equation = ch.value("transfer-func").toString();
		}

		/* test the equation */
		if (!entry->transferFunc.equation.isEmpty()) {
			QString res = testEquation(i);
			if (!res.isEmpty()) {
				qDebug() << "AIN Equation ("  << i <<") failed test: " << res;
			}
		}
	}

	endResetModel();
}

bool LsBox0AinModel::anyEnabledColumnRequireStreaming()
{
	for (int i = 0; i < entries.size(); i++) {
		LsBox0AinModelEntry *entry = entries.at(i);
		if (entry->enable && entry->type == "PHOTO_GATE") {
			return true;
		}
	}

	return false;
}

void LsBox0AinModel::acceptAveraging(bool accept)
{
	beginResetModel();
	accept_averaging = accept;
	endResetModel();
}
