/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_I2C_ADD_TREE_H
#define LIAB_STUDIO_INTERFACES_BOX0_I2C_ADD_TREE_H

#include <QVariant>
#include <Qt>
#include "interfaces/box0/extra/sensor.h"
#include <QVector>

/* the base class */
class LsBox0I2cAddTreeItem
{
	public:
		LsBox0I2cAddTreeItem() = default;
		virtual ~LsBox0I2cAddTreeItem() = default;

		virtual Qt::ItemFlags flags() { return Qt::NoItemFlags; }
		virtual QVariant data(int column, int role) = 0;
		virtual int columnCount() = 0;

		virtual LsBox0I2cAddTreeItem* child(int num) { Q_UNUSED(num); return Q_NULLPTR; }
		virtual int childCount() { return 0; }
		virtual int childIndexOf(LsBox0I2cAddTreeItem *child) { Q_UNUSED(child); return -1; }
		int row() { return parent()->childIndexOf(this); }

		virtual LsBox0I2cAddTreeItem* parent() { return Q_NULLPTR; }
};

class LsBox0I2cAddTreeSlave;
class LsBox0I2cAddTreeAddress : public LsBox0I2cAddTreeItem
{
	public:
		LsBox0I2cAddTreeAddress(LsBox0I2cAddTreeSlave *parent,
			const LsBox0Sensor::I2c::Address &addr, bool disabled);
		int columnCount() { return 3; }
		QVariant data(int column, int role);
		/* LsBox0I2cAddTreeSlave* */ LsBox0I2cAddTreeItem* parent() { return (LsBox0I2cAddTreeItem *)(m_parent); }
		Qt::ItemFlags flags() {
			return m_disabled ? Qt::ItemNeverHasChildren :
			Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsSelectable;
		}

		uint getAddress() { return m_addr.value; }

		QString title();

	private:
		LsBox0I2cAddTreeSlave *m_parent;
		const LsBox0Sensor::I2c::Address &m_addr;
		bool m_disabled;
};

class LsBox0I2cAddTreeRoot;
class LsBox0I2cAddTreeSlave : public LsBox0I2cAddTreeItem
{
	public:
		LsBox0I2cAddTreeSlave(LsBox0I2cAddTreeRoot *parent, QString type, QVector<uint> &usedAddresses);
		~LsBox0I2cAddTreeSlave() { qDeleteAll(m_addresses); }

		int columnCount() { return 3; }
		int childCount() { return m_addresses.size(); }
		/* LsBox0I2cAddTreeRoot* */ LsBox0I2cAddTreeItem* parent() { return (LsBox0I2cAddTreeItem *)(m_parent); }
		/* LsBox0I2cAddTreeAddress* */ LsBox0I2cAddTreeItem* child(int index);
		int childIndexOf(LsBox0I2cAddTreeItem *child) { return m_addresses.indexOf(static_cast<LsBox0I2cAddTreeAddress *>(child)); }
		QVariant data(int column, int role);

		QString getType() { return m_type; }

	private:
		LsBox0I2cAddTreeRoot *m_parent;
		QList<LsBox0I2cAddTreeAddress *> m_addresses;
		QString m_type;
};

class LsBox0I2cAddTreeRoot : public LsBox0I2cAddTreeItem
{
	public:
		LsBox0I2cAddTreeRoot(QVector<uint> &usedAddresses);
		~LsBox0I2cAddTreeRoot() { qDeleteAll(m_slaves); }

		int childCount() { return m_slaves.size(); }
		int columnCount() { return 3; }
		/* LsBox0I2cAddTreeSlave* */ LsBox0I2cAddTreeItem* child(int index);
		QVariant data(int column, int role);
		int childIndexOf(LsBox0I2cAddTreeItem *child) { return m_slaves.indexOf(static_cast<LsBox0I2cAddTreeSlave *>(child)); }

	private:
		QList<LsBox0I2cAddTreeSlave *> m_slaves;
};

#endif
