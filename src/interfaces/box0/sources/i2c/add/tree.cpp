/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tree.h"

/*
 *  this is used to simulate a tree of design:
 *
 * LsBox0I2cAddTreeRoot (-> LsBox0I2cAddTreeItem)
 * 		|
 * 		+--- LsBox0I2cAddTreeSlave (-> LsBox0I2cAddTreeItem)
 * 				|
 * 				+----- LsBox0I2cAddTreeAddress (-> LsBox0I2cAddTreeItem)
 */

/* --- address --- */

LsBox0I2cAddTreeAddress::LsBox0I2cAddTreeAddress(LsBox0I2cAddTreeSlave *parent,
	const LsBox0Sensor::I2c::Address &addr, bool disabled) :
	LsBox0I2cAddTreeItem(),
	m_parent(parent),
	m_addr(addr),
	m_disabled(disabled)
{
}

QVariant LsBox0I2cAddTreeAddress::data(int column, int role)
{
	if (role == Qt::DisplayRole) {
		switch(column) {
		case 1:
		return m_addr.title;

		case 2:
		return "0x" + QString::number(m_addr.value, 16);
		}
	}

	return QVariant();
}

QString LsBox0I2cAddTreeAddress::title()
{
	QString id = m_parent->getType();
	int sensor_index = LsBox0Sensor::sensor_index(id, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);
	return sensor.title + " - 0x" + QString::number(m_addr.value, 16);
}

/* --- slave --- */

LsBox0I2cAddTreeSlave::LsBox0I2cAddTreeSlave(
	LsBox0I2cAddTreeRoot *parent, QString type, QVector<uint> &usedAddresses) :
	LsBox0I2cAddTreeItem (),
	m_parent(parent),
	m_type(type)
{
	int sensor_index = LsBox0Sensor::sensor_index(type, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

	Q_FOREACH(const LsBox0Sensor::I2c::Address &i, sensor.addresses) {
		bool disable = usedAddresses.contains(i.value);
		m_addresses.append(new LsBox0I2cAddTreeAddress(this, i, disable));
	}
}

/* LsBox0I2cAddTreeAddress* */ LsBox0I2cAddTreeItem* LsBox0I2cAddTreeSlave::child(int index)
{
	Q_ASSERT(index >= 0 && index < m_addresses.size());
	return m_addresses.at(index);
}

QVariant LsBox0I2cAddTreeSlave::data(int column, int role)
{
	int sensor_index = LsBox0Sensor::sensor_index(m_type, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);
	return (column == 0 && role == Qt::DisplayRole) ? sensor.title : QVariant();
}

/* --- root --- */

LsBox0I2cAddTreeRoot::LsBox0I2cAddTreeRoot(QVector<uint> &usedAddresses) :
	LsBox0I2cAddTreeItem()
{
	Q_FOREACH(const LsBox0Sensor::I2c &i, LsBox0Sensor::i2c) {
		m_slaves.append(new LsBox0I2cAddTreeSlave(this, i.id, usedAddresses));
	}
}

/* LsBox0I2cAddTreeSlave* */ LsBox0I2cAddTreeItem* LsBox0I2cAddTreeRoot::child(int index)
{
	Q_ASSERT(index >= 0 && index < m_slaves.size());
	return m_slaves.at(index);
}

const static QVariant headers[] = {"Name", "Detail", "Address"};

/* return header data
 * header moved here because it was convient to manage */
QVariant LsBox0I2cAddTreeRoot::data(int column, int role)
{
	if (role == Qt::DisplayRole && column >= 0 && column < 3) {
		return headers[column];
	}

	return QVariant();
}
