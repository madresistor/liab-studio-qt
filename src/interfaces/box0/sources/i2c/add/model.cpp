/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include "tree.h"

LsBox0I2cAddModel::LsBox0I2cAddModel(QVector<uint> &usedAddresses, QObject *parent) :
	QAbstractItemModel(parent),
	root(new LsBox0I2cAddTreeRoot(usedAddresses))
{
}

LsBox0I2cAddModel::~LsBox0I2cAddModel()
{
	delete root;
}

int LsBox0I2cAddModel::columnCount(const QModelIndex &parent) const
{
	LsBox0I2cAddTreeItem *parentItem = root;
	if (parent.isValid()) {
		parentItem = static_cast<LsBox0I2cAddTreeItem*>(parent.internalPointer());
	}

	return parentItem->columnCount();
}

int LsBox0I2cAddModel::rowCount(const QModelIndex &parent) const
{
	LsBox0I2cAddTreeItem *parentItem = root;
	if (parent.isValid()) {
		parentItem = static_cast<LsBox0I2cAddTreeItem*>(parent.internalPointer());
	}

	return parentItem->childCount();
}

QVariant LsBox0I2cAddModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}

	LsBox0I2cAddTreeItem *item = static_cast<LsBox0I2cAddTreeItem*>(index.internalPointer());
	return item->data(index.column(), role);
}

Qt::ItemFlags LsBox0I2cAddModel::flags(const QModelIndex &index) const
{
	LsBox0I2cAddTreeItem *item = root;
	if (index.isValid()) {
		item = static_cast<LsBox0I2cAddTreeItem*>(index.internalPointer());
	}

	return item->flags();
}

QVariant LsBox0I2cAddModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	return (orientation == Qt::Horizontal) ?
		root->data(section, role) :
		QVariant();
}

QModelIndex LsBox0I2cAddModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	LsBox0I2cAddTreeItem *parentItem = root;

	if (parent.isValid()) {
		parentItem = static_cast<LsBox0I2cAddTreeItem*>(parent.internalPointer());
	}

	LsBox0I2cAddTreeItem *childItem = parentItem->child(row);
	if (childItem) {
		return createIndex(row, column, childItem);
	} else {
		return QModelIndex();
	}
}

QModelIndex LsBox0I2cAddModel::parent(const QModelIndex & index) const
{
	if (!index.isValid()) {
		return QModelIndex();
	}

	LsBox0I2cAddTreeItem *childItem = static_cast<LsBox0I2cAddTreeItem*>(index.internalPointer());
	LsBox0I2cAddTreeItem *parentItem = childItem->parent();

	if (parentItem == root) {
		return QModelIndex();
	}

	return createIndex(parentItem->row(), 0, parentItem);
}
