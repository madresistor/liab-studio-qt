/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_I2C_ADD_H
#define LIAB_STUDIO_INTERFACES_BOX0_I2C_ADD_H

#include <QDialog>
#include <QWidget>

#include "ui_box0_i2c_add.h"

class LsBox0I2cAdd : public QDialog
{
	Q_OBJECT

	public:
		LsBox0I2cAdd(QWidget *parent);
		QString type();
		QString title();
		uint address();
		void setUsedAddresses(QVector<uint> addresses);

	private Q_SLOTS:
		void autoFillTitle();

	private:
		Ui::LsBox0I2cAdd ui;
};

#endif
