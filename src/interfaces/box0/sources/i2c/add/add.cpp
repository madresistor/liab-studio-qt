/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "add.h"
#include "model.h"
#include <QPushButton>
#include <QDebug>

LsBox0I2cAdd::LsBox0I2cAdd(QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);
	ui.buttonBox->button(QDialogButtonBox::Ok)->setText("Add");
}

QString LsBox0I2cAdd::type()
{
	QModelIndex index = ui.slaveTable->currentIndex();

	if (index.isValid()) {
		LsBox0I2cAddTreeAddress *_addr = static_cast<LsBox0I2cAddTreeAddress *>(index.internalPointer());
		LsBox0I2cAddTreeSlave *_slave = static_cast<LsBox0I2cAddTreeSlave *>(_addr->parent());
		return _slave->getType();
	}

	qDebug() << "BUG: [LsBox0I2cAdd::type] index should have been valid";
	return 0;
}

uint LsBox0I2cAdd::address()
{
	QModelIndex index = ui.slaveTable->currentIndex();

	if (index.isValid()) {
		LsBox0I2cAddTreeAddress *_addr = static_cast<LsBox0I2cAddTreeAddress *>(index.internalPointer());
		return _addr->getAddress();
	}

	qDebug() << "BUG: [LsBox0I2cAdd::address] index should have been valid";
	return 0;
}

QString LsBox0I2cAdd::title()
{
	return ui.title->text();
}

void LsBox0I2cAdd::setUsedAddresses(QVector<uint> usedAddresses)
{
	QAbstractItemModel *model = new LsBox0I2cAddModel(usedAddresses, this);
	ui.slaveTable->setModel(model);

	/* TODO: select first item that is selectable */
	ui.slaveTable->setCurrentIndex(model->index(0, 0, model->index(0, 0)));

	connect(ui.slaveTable->selectionModel(),
			SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
			this, SLOT(autoFillTitle()));

	autoFillTitle();

	ui.slaveTable->expandAll();

	for (int i = 0; i < 3; i++) {
		ui.slaveTable->resizeColumnToContents(i);
	}
}

void LsBox0I2cAdd::autoFillTitle()
{
	QModelIndex index = ui.slaveTable->currentIndex();

	if (index.isValid()) {
		LsBox0I2cAddTreeAddress *_addr = static_cast<LsBox0I2cAddTreeAddress *>(index.internalPointer());
		ui.title->setText(_addr->title());
	}
}
