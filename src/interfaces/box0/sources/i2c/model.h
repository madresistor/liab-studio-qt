/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_I2C_MODEL_H
#define LIAB_STUDIO_INTERFACES_BOX0_I2C_MODEL_H

#include <QWidget>
#include <QAbstractTableModel>
#include <QJsonArray>
#include <libbox0/libbox0.h>
#include <libbox0/driver/adxl345/adxl345.h>
#include <libbox0/driver/bmp180/bmp180.h>
#include <libbox0/driver/tmp006/tmp006.h>
#include <libbox0/driver/tsl2591/tsl2591.h>
#include <libbox0/driver/lm75/lm75.h>
#include <libbox0/driver/l3gd20/l3gd20.h>
#include <libbox0/driver/si114x/si114x.h>
#include <libbox0/driver/tcs34725/tcs34725.h>
#include "widgets/table/table.h"
#include "widgets/graph/graph.h"
#include "widgets/graph/curve.h"

struct LsBox0I2cModelEntry
{
	bool enable;
	QString name;
	QString type;
	uint address;
	QHash<QString, QVariant> config;

	union {
		b0_adxl345 *adxl345;
		b0_bmp180 *bmp180;
		b0_tmp006 *tmp006;
		b0_tsl2591 *tsl2591;
		b0_lm75 *lm75;
		b0_l3gd20 *l3gd20;
		b0_si114x *si114x;
		b0_tcs34725 *tcs34725;
	} driver;

	struct {
		size_t total_row;
		size_t row;
		int x;
		QList<int> y  /* usecase: accelerometer */;
	} feed;
};

class LsBox0I2cModel : public QAbstractTableModel
{
	Q_OBJECT

	public:
		LsBox0I2cModel(b0_i2c *i2c, QWidget *parent = Q_NULLPTR);
		QVariant headerData(int section, Qt::Orientation orientation, int role) const;
		QVariant data(const QModelIndex &index, int role) const;
		int columnCount(const QModelIndex &parent) const;
		int rowCount(const QModelIndex &parent) const;
		Qt::ItemFlags flags(const QModelIndex & index) const;
		bool setData(const QModelIndex & index, const QVariant & value, int role);

		void appendRow(QString type, QString title, uint address);

		bool removeRow(int row, const QModelIndex & parent = QModelIndex());

		QString type(int index);
		uint address(int index);

		QHash<QString, QVariant> config(int index);
		void setConfig(int index, const QHash<QString, QVariant> &config);

		bool anyColumnEnabled();
		bool prepareColumns(LsTable *table, LsGraph *graph,
			bool assign_marker, size_t total_samples);
		void userEditable(bool e);

		void gather(LsTable *table, qint64 start_time);

		QVector<uint> usedAddresses();

		void onStop();

		QJsonArray toJsonArray();
		void fromJsonArray(QJsonArray arr);

	private:
		QList<LsBox0I2cModelEntry> entries;
		bool editable = true;
		b0_i2c *module;
};

#endif
