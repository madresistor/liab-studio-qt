/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "i2c.h"
#include "add/add.h"
#include "model.h"
#include <QMenu>
#include "interfaces/box0/extra/result_code.h"
#include "interfaces/box0/extra/config/config.h"
#include "interfaces/box0/extra/sensor.h"
#include <QMessageBox>

LsBox0I2c::LsBox0I2c(QWidget *parent) :
	LsBox0Source(parent)
{
	ui.setupUi(this);

	connect(ui.btnAdd, SIGNAL(clicked()), SLOT(addRow()));
	connect(ui.btnConfigure, SIGNAL(clicked()), SLOT(configureRow()));
	connect(ui.btnRemove, SIGNAL(clicked()), SLOT(removeRow()));

	m_sampler = new LsBox0ThreadedTimer(this);
	connect(m_sampler, SIGNAL(timeout()), SLOT(timeout()), Qt::DirectConnection);
}

void LsBox0I2c::timeout()
{
	model->gather(table, start_time);
	table->update();
}

void LsBox0I2c::addRow()
{
	/* model is NULL, nothing can be done */
	if (model == Q_NULLPTR) {
		return;
	}

	LsBox0I2cAdd add(this);

	add.setUsedAddresses(model->usedAddresses());

	if (add.exec() == QDialog::Accepted) {
		model->appendRow(add.type(), add.title(), add.address());
	}
}

void LsBox0I2c::setDevice(b0_device *dev)
{
	if (dev == NULL) {
		/* remove device */

		if (module != NULL) {
			onStop();

			LsBox0ResultCode::handleInternal(
				b0_i2c_close(module), "unable to unload I2C");
		}

		module = NULL;
	} else if (B0_ERR_RC(b0_i2c_open(dev, &module, 0))) {
		qDebug() << "no i2c in device";
		module = NULL;
	} else if (B0_ERR_RC(b0_i2c_master_prepare(module))) {
		qDebug() << "unable to prepare I2C module for master";
		b0_i2c_close(module);
		module = NULL;
	}

	setEnabled(module != NULL);

	if (module == NULL) {
		qDebug() << "no I2C module";
		model = Q_NULLPTR;
		ui.table->setModel(Q_NULLPTR);
		return;
	}

	model = new LsBox0I2cModel(module, this);
	ui.table->setModel(model);

	/* show Delete popup on right click on column header */
	QHeaderView *headerView = ui.table->verticalHeader();
	headerView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(headerView, SIGNAL(customContextMenuRequested(const QPoint &)),
		SLOT(showMenuColumn(const QPoint &)));
}

void LsBox0I2c::configureRow(int index)
{
	/* model is NULL, nothing can be done */
	if (model == Q_NULLPTR) {
		return;
	}

	QString t = model->type(index);
	uint address = model->address(index);
	QHash<QString, QVariant> values = model->config(index);

	/* any configuration? */
	if (values.isEmpty()) {
		QMessageBox::information(this, "No configuration", "There is nothing to configure");
		return;
	}

	int sensor_index = LsBox0Sensor::sensor_index(t, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

	QStringList readonly_hidden;

	Q_FOREACH(const LsBox0Sensor::I2c::Address &a, sensor.addresses) {
		if (a.value == address) {
			Q_FOREACH(const LsBox0Sensor::I2c::Address::Equal &e, a.equal) {
				readonly_hidden.append(e.id);
			}

			break;
		}
	}

	/* TODO: consider config with hidden/readonly as nothing to configure */

	/* we have something to configure (for user)? */
	if (readonly_hidden == values.keys()) {
		QMessageBox::information(this, "No configuration", "There is nothing to configure");
		return;
	}

	LsBox0Config d;
	d.setConfig(sensor.config, values, readonly_hidden);

	if (d.exec() == QDialog::Accepted) {
		model->setConfig(index, d.config());
	}
}

void LsBox0I2c::configureRow()
{
	int row = ui.table->selectedRow();
	if (row == -1) {
		return;
	}

	configureRow(row);
}

void LsBox0I2c::removeRow(int index)
{
	/* model is NULL, nothing can be done */
	if (model == Q_NULLPTR) {
		return;
	}

	model->removeRow(index);
}

void LsBox0I2c::removeRow()
{
	int row = ui.table->selectedRow();
	if (row == -1) {
		return;
	}

	removeRow(row);
	ui.table->selectRow(row - (row > 0 ? 1 : 0));
}

void LsBox0I2c::showMenuColumn(const QPoint & pos)
{
	int logical_index = ui.table->verticalHeader()->logicalIndexAt(pos);

	if (logical_index < 0) {
		qDebug() << "really occured?";
		return;
	}

	QMenu menu;
	QAction actionConfigure("Configure", &menu);
	QAction actionRemove("Remove", &menu);

	menu.addAction(&actionConfigure);
	menu.addAction(&actionRemove);

	QAction *sel = menu.exec(QCursor::pos());
	if (sel == &actionRemove) {
		removeRow(logical_index);
	} else if (sel == &actionConfigure) {
		configureRow(logical_index);
	}
}

int LsBox0I2c::onStart(LsTable *table, LsGraph *graph, bool assign_marker, uint time)
{
	if (module == NULL) {
		qDebug() << "no I2C module";
		return 0;
	}

	if (model == Q_NULLPTR) {
		qDebug() << "no I2C model";
		return -1;
	}

	if (!model->anyColumnEnabled()) {
		qDebug() << "i2c no column enabled";
		return 0;
	}

	uint sampleRate = ui.sampleRate->value();
	if (sampleRate == 0) {
		qDebug() << "i2c disabled";
		return 0;
	}

	if (!model->prepareColumns(table, graph, assign_marker, sampleRate * time)) {
		qDebug() << "i2c prepare column failed";
		return -1;
	}

	this->table = table;

	start_time = QDateTime::currentMSecsSinceEpoch();
	m_sampler->start(1000 / sampleRate);

	ui.btnAdd->setEnabled(false);
	ui.btnConfigure->setEnabled(false);
	ui.btnRemove->setEnabled(false);
	ui.table->verticalHeader()->setContextMenuPolicy(Qt::PreventContextMenu);
	model->userEditable(false);
	m_running = true;

	return 1;
}

void LsBox0I2c::onStop()
{
	if (!m_running) {
		return;
	}

	m_sampler->quit();
	m_sampler->wait();
	model->onStop();

	ui.btnAdd->setEnabled(true);
	ui.btnConfigure->setEnabled(true);
	ui.btnRemove->setEnabled(true);
	ui.table->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
	model->userEditable(true);
	m_running = false;
}

QJsonObject LsBox0I2c::jsonConfig()
{
	return {
		{"channels", model->toJsonArray()},
		{"speed", QJsonValue(ui.sampleRate->value())}
	};
}

void LsBox0I2c::setJsonConfig(QJsonObject config)
{
	ui.sampleRate->setValue(config.value("speed").toInt(0));
	model->fromJsonArray(config.value("channels").toArray());
}
