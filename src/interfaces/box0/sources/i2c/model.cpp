/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include "interfaces/box0/extra/sensor.h"
#include "interfaces/box0/extra/result_code.h"
#include <libbox0/misc/conv.h>

#define ACCELERATION_DUE_TO_GRAVITY (9.8067)

#define HORIZONTAL_HEADER_COUNT 3
static const QVariant horizontal_header[HORIZONTAL_HEADER_COUNT] = {
	"Enable",
	"Name",
	"Type"
};

LsBox0I2cModel::LsBox0I2cModel(b0_i2c *mod, QWidget *parent) :
	QAbstractTableModel(parent),
	module(mod)
{
}

int LsBox0I2cModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return entries.size();
}

int LsBox0I2cModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return HORIZONTAL_HEADER_COUNT;
}

QVariant LsBox0I2cModel::data(const QModelIndex &index, int role) const
{
	int row = index.row();
	if (row >= 0 && row < entries.size()) {
		const LsBox0I2cModelEntry &col = entries.at(row);

		int column = index.column();
		if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
			switch(column) {
			case 1:
			return QVariant(col.name);
			case 2:
			return QVariant(col.type);
			}
		} else if (role == Qt::CheckStateRole) {
			switch(column) {
			case 0:
			return col.enable ? Qt::Checked : Qt::Unchecked;
			}
		} /* else if (role == Qt::SizeHintRole) {
			switch(column) {
			case 0:
			return QSize(10, 10);
			case 2:
			return QSize(20, 10);
			}
		} */
	}

	return QVariant();
}

QVariant LsBox0I2cModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal) {
			if (section >= 0 && section < HORIZONTAL_HEADER_COUNT) {
				return horizontal_header[section];
			}
		} else if (section < entries.size()){
			const LsBox0I2cModelEntry &col = entries.at(section);
			return "0x" + QString::number(col.address, 16);
		}
	}

	return QVariant();
}

bool LsBox0I2cModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (!editable) {
		return false;
	}

	int row = index.row();
	LsBox0I2cModelEntry &col = entries[row];

	if (role == Qt::EditRole) {
		switch(index.column()) {
		case 1:
			col.name = value.toString();
		break;
		default:
		return false;
		}
	} else if (role == Qt::CheckStateRole) {
		switch(index.column()) {
		case 0:
			col.enable = (value.toInt() == Qt::Checked);
		break;
		default:
		return false;
		}
	}

	Q_EMIT dataChanged(index, index);
	return true;
}

Qt::ItemFlags LsBox0I2cModel::flags(const QModelIndex & index) const
{
	switch(index.column()) {
	case 0:
	return  Qt::ItemIsSelectable |
			Qt::ItemIsEnabled |
			(editable ? Qt::ItemIsUserCheckable : Qt::NoItemFlags);

	case 1:
	return  Qt::ItemIsSelectable |
			Qt::ItemIsEnabled |
			(editable ? Qt::ItemIsEditable : Qt::NoItemFlags);

	case 2:
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;

	default:
	return 0;
	}
}

bool LsBox0I2cModel::prepareColumns(LsTable *table, LsGraph *graph,
	bool assign_marker, size_t total_samples)
{
	for (int i = 0; i < entries.size(); i++) {
		LsBox0I2cModelEntry &entry = entries[i];
		if (entry.enable) {
			/* prepare the entries */
			entry.feed.row = 0;
			entry.feed.total_row = total_samples;

			entry.feed.y.clear();

			entry.feed.x = table->appendColumn(entry.name + " time");
			table->tableData->columns.at(entry.feed.x)->sig_digits = 6;

			QList<QString> y_axis_xyz, y_axis_rgb;
			y_axis_xyz << "X" << "Y" << "Z";
			y_axis_rgb << "Red" << "Green" << "Blue";

			QList<QString> y_axis_entries;

			QString sensor_driver =
				LsBox0Sensor::sensor_driver(entry.type, LsBox0Sensor::i2c);

			if (sensor_driver == "ADXL345") {
				y_axis_entries = y_axis_xyz;

				bool ALT_ADDRESS = entry.config.value("ALT_ADDRESS") == "HIGH";
				if (B0_ERR_RC(b0_adxl345_open_i2c(module,
									&entry.driver.adxl345, ALT_ADDRESS))) {
					qDebug() << "failed to open ADXL345";
					entry.driver.adxl345 = NULL;
					return false;
				}

				if (B0_ERR_RC(b0_adxl345_power_up(entry.driver.adxl345))) {
					qDebug() << "Unable to power up ADXL345";
					/* TODO: free driver */
					return false;
				}
			} else if (sensor_driver == "BMP180") {
				if (B0_ERR_RC(b0_bmp180_open(module, &entry.driver.bmp180))) {
					qDebug() << "failed to open BMP180";
					entry.driver.bmp180 = NULL;
					return false;
				}

				QHash<QString, b0_bmp180_over_samp> OVER_SAMP_MAP {
					{"1", B0_BMP180_OVER_SAMP_1},
					{"2", B0_BMP180_OVER_SAMP_2},
					{"4", B0_BMP180_OVER_SAMP_4},
					{"8", B0_BMP180_OVER_SAMP_8}
				};
				QString _over_samp = entry.config.value("OVER_SAMP").toString();
				b0_bmp180_over_samp over_samp = OVER_SAMP_MAP.value(_over_samp);
				if (B0_ERR_RC(b0_bmp180_over_samp_set(entry.driver.bmp180,
						over_samp))) {
						qDebug() << "over sampling set to BMP180 failed";
				}
			} else if (sensor_driver == "TMP006") {
				bool ADR1 = entry.config.value("ADR1") == "HIGH";
				QHash<QString, b0_tmp006_adr0> ADR0_MAP {
					{"GND", B0_TMP006_GND},
					{"VCC", B0_TMP006_VCC},
					{"SDA", B0_TMP006_SDA},
					{"SCL", B0_TMP006_SCL}
				};
				b0_tmp006_adr0 ADR0 = ADR0_MAP.value(entry.config.value("ADR0").toString());
				double calib_factor = entry.config.value("CALIB_FACTOR").toDouble();
				if (B0_ERR_RC(b0_tmp006_open(module, &entry.driver.tmp006,
								calib_factor, ADR1, ADR0))) {
					qDebug() << "failed to open TMP006";
					entry.driver.tmp006 = NULL;
					return false;
				}
			} else if (entry.type == "TSL2591") {
				if (B0_ERR_RC(b0_tsl2591_open(module,
											&entry.driver.tsl2591, NULL))) {
					qDebug() << "failed to open TSL2591";
					entry.driver.tsl2591 = NULL;
					return false;
				}

				QHash<QString, b0_tsl2591_integ> INTEG_MAP {
					{"100MS", B0_TSL2591_INTEG_100MS},
					{"200MS", B0_TSL2591_INTEG_200MS},
					{"300MS", B0_TSL2591_INTEG_300MS},
					{"400MS", B0_TSL2591_INTEG_400MS},
					{"500MS", B0_TSL2591_INTEG_500MS},
					{"600MS", B0_TSL2591_INTEG_600MS}
				};
				QString _integ = entry.config.value("INTEG").toString();
				b0_tsl2591_integ integ = INTEG_MAP.value(_integ);
				if (B0_ERR_RC(b0_tsl2591_integ_set(entry.driver.tsl2591, integ))) {
					qDebug() << "failed to set TSL2591 Integeration time";
				}

				QHash <QString, b0_tsl2591_gain> GAIN_MAP {
					{"LOW", B0_TSL2591_GAIN_LOW},
					{"MEDIUM", B0_TSL2591_GAIN_MEDIUM},
					{"HIGH", B0_TSL2591_GAIN_HIGH},
					{"MAX", B0_TSL2591_GAIN_MAX}
				};
				QString _gain = entry.config.value("GAIN").toString();
				b0_tsl2591_gain gain = GAIN_MAP.value(_gain);
				if (B0_ERR_RC(b0_tsl2591_gain_set(entry.driver.tsl2591, gain))) {
					qDebug() << "failed to set TSL2591 Gain";
				}
			} else if (entry.type == "LM75") {
				bool A2 = entry.config.value("A2") == "HIGH";
				bool A1 = entry.config.value("A1") == "HIGH";
				bool A0 = entry.config.value("A0") == "HIGH";
				if (B0_ERR_RC(b0_lm75_open(module,
								&entry.driver.lm75, A2, A1, A0))) {
					qDebug() << "failed to open LM75";
					entry.driver.lm75 = NULL;
					return false;
				}
			} else if (sensor_driver == "L3GD20") {
				y_axis_entries = y_axis_xyz;

				bool SAD0 = entry.config.value("SAD0") == "HIGH";
				if (B0_ERR_RC(b0_l3gd20_open_i2c(module,
								&entry.driver.l3gd20, SAD0))) {
					qDebug() << "failed to open L3GD20";
					entry.driver.l3gd20 = NULL;
					return false;
				}

				if (B0_ERR_RC(b0_l3gd20_power_up(entry.driver.l3gd20))) {
					qDebug() << "Unable to power up L3GD20";
					/* TODO: free driver */
					return false;
				}
			} else if (sensor_driver == "SI114X") {
				if (B0_ERR_RC(b0_si114x_open(module, &entry.driver.si114x))) {
					qDebug() << "failed to open Si114x";
					return false;
				}
			} else if (sensor_driver == "TCS3472") {
				y_axis_entries = y_axis_rgb;

				if (B0_ERR_RC(b0_tcs34725_open(module,
								&entry.driver.tcs34725))) {
					qDebug() << "failed to open TCS3472";
					return false;
				}

				QHash<QString, uint8_t> AGAIN_MAP {
					{"1", B0_TCS34725_AGAIN_GAIN_1X},
					{"4", B0_TCS34725_AGAIN_GAIN_4X},
					{"16", B0_TCS34725_AGAIN_GAIN_16X},
					{"60", B0_TCS34725_AGAIN_GAIN_60X}
				};
				QString _again = entry.config.value("GAIN").toString();
				uint8_t again = AGAIN_MAP.value(_again);
				if (B0_ERR_RC(b0_tcs34725_again_set(entry.driver.tcs34725, again))) {
					qDebug() << "failed to set again to TCS3472";
				}

				QString _wait_long = entry.config.value("WAIT_LONG").toString();
				uint8_t wait_long = _wait_long == "12" ?
						B0_TCS34725_CONFIG_WLONG : 0x00;
				if (B0_ERR_RC(b0_tcs34725_reg_write(entry.driver.tcs34725,
										B0_TCS34725_CONFIG, wait_long))) {
					qDebug() << "failed to set wait long to TCS3472";
				}

				uint _wait_time = entry.config.value("WAIT_TIME").toUInt();
				uint8_t wait_time = B0_TCS34725_WTIME_FROM_WAIT_CYCLES(_wait_time);
				if (B0_ERR_RC(b0_tcs34725_reg_write(entry.driver.tcs34725,
										B0_TCS34725_WTIME, wait_time))) {
					qDebug() << "failed to set wait time to TCS3472";
				}

				/* for user, it is seens as integeration time,
				 *  but for developer it has integeration cycle value */
				uint _integ_cycle = entry.config.value("INTEG_CYCLE").toUInt();
				uint8_t atime = B0_TCS34725_ATIME_FROM_INTEG_CYCLES(_integ_cycle);
				if (B0_ERR_RC(b0_tcs34725_atime_set(entry.driver.tcs34725, atime))) {
					qDebug() << "failed to set atime to TCS3472";
				}

			} else {
				qDebug() << "BUG: LsBox0I2cModel::prepareColumns ???";
				return false;
			}

			entry.feed.y.clear();
			if (y_axis_entries.size() < 1) {
				int col_y = table->appendColumn(entry.name + " value");
				entry.feed.y.append(col_y);

				if (graph != Q_NULLPTR) {
					LsMarker::Type marker = static_cast<LsMarker::Type>(LsMarker::PLUS + i);
					graph->addCurve(entry.name,
						table, entry.feed.x,
						table, col_y,
						table->tableData->columns.at(col_y)->color,
						true, LsGraphCurve::DEF_LINE_WIDTH,
						assign_marker, marker);
				}
			} else {
				for (int j = 0; j < y_axis_entries.size(); j++) {
					QString name = entry.name + " " + y_axis_entries.at(j);
					int col_y = table->appendColumn(name);
					entry.feed.y.append(col_y);

					if (graph != Q_NULLPTR) {
						LsMarker::Type marker = static_cast<LsMarker::Type>(LsMarker::PLUS + i);
						graph->addCurve(name,
							table, entry.feed.x,
							table, col_y,
							table->tableData->columns.at(col_y)->color,
							true, LsGraphCurve::DEF_LINE_WIDTH,
							assign_marker, marker);
					}
				}
			}
		}
	}

	return true;
}

bool LsBox0I2cModel::anyColumnEnabled()
{
	for (int i = 0; i < entries.size(); i++) {
		if (entries.at(i).enable) {
			return true;
		}
	}

	return false;
}

void LsBox0I2cModel::userEditable(bool e)
{
	beginResetModel();
	editable = e;
	endResetModel();
}

QVector<uint> LsBox0I2cModel::usedAddresses()
{
	QVector<uint> list;

	for (int i = 0; i < entries.size(); i++) {
		const LsBox0I2cModelEntry &entry = entries.at(i);
		if (entry.enable) {
			list.append(entry.address);
		}
	}

	return list;
}

/**
 * Build configuration based on @a type and @a address and store the result in @a config
 * @param type Sensor ID
 * @param address Address
 * @param[out] config Configuration
 */
static void build_entry_config(const QString &type,
		uint address, QHash<QString, QVariant> &config)
{
	config.clear();

	int sensor_index = LsBox0Sensor::sensor_index(type, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

	/* copy the address specific configuration */
	Q_FOREACH(const LsBox0Sensor::I2c::Address &a, sensor.addresses) {
		if (a.value == address) {
			Q_FOREACH(const LsBox0Sensor::I2c::Address::Equal &e, a.equal) {
				config[e.id] = e.value;
			}

			break;
		}
	}

	/* copy the remaining configuration */
	Q_FOREACH(const LsBox0Sensor::Config *c, sensor.config) {
		/* no overwrite if key exists */
		if (!config.contains(c->id)) {
			config[c->id] = c->currentValue();
		}
	}
}

void LsBox0I2cModel::appendRow(QString type, QString title, uint address)
{
	beginInsertRows(QModelIndex(), entries.size(), entries.size());

	LsBox0I2cModelEntry entry;
	entry.enable = true;
	entry.name = title;
	entry.type = type;
	entry.address = address;
	entry.driver.adxl345 = NULL; /* null the driver pointer */

	build_entry_config(type, address, entry.config);

	entries.append(entry);

	endInsertRows();
}

QString LsBox0I2cModel::type(int idx)
{
	if (idx >= 0 && idx < entries.count()) {
		return entries.at(idx).type;
	}

	return QString::null;
}

uint LsBox0I2cModel::address(int idx)
{
	if (idx >= 0 && idx < entries.count()) {
		return entries.at(idx).address;
	}

	return 0;
}

QHash<QString, QVariant> LsBox0I2cModel::config(int idx)
{
	if (idx >= 0 && idx < entries.count()) {
		return entries.at(idx).config;
	}

	qDebug() << Q_FUNC_INFO << "index out of range";
	return QHash<QString, QVariant>();
}

void LsBox0I2cModel::setConfig(int idx, const QHash<QString, QVariant> &config)
{
	if (idx >= 0 && idx < entries.count()) {
		entries[idx].config = config;
		return;
	}

	qDebug() << Q_FUNC_INFO << "index out of range";
}

bool LsBox0I2cModel::removeRow(int row, const QModelIndex & parent)
{
	if (row >= 0 && row < entries.size()) {
		beginRemoveRows(parent, row, row);
		entries.removeAt(row);
		endRemoveRows();

		return true;
	}

	return false;
}

void LsBox0I2cModel::gather(LsTable *table, qint64 start_time)
{
	for (int i = 0; i < entries.size(); i++) {
		LsBox0I2cModelEntry &entry = entries[i];
		if (entry.enable) {

			/* keep a bar on total samples to recevied */
			if (entry.feed.total_row > 0) {
				if (entry.feed.row >= entry.feed.total_row) {
					continue;
				}
			}

			qint64 current_time = QDateTime::currentMSecsSinceEpoch();

			if (entry.type == "ADXL345") {
				double x, y, z;
				if (B0_ERR_RC(b0_adxl345_read(
								entry.driver.adxl345, &x, &y, &z))) {
					qDebug() << "b0_adxl345_read() failed";
					goto failed;
				}

				// these configuration will only work for Earth :)
				x *= ACCELERATION_DUE_TO_GRAVITY;
				y *= ACCELERATION_DUE_TO_GRAVITY;
				z *= ACCELERATION_DUE_TO_GRAVITY;

				table->setDataDirect(entry.feed.row, entry.feed.y[0], x);
				table->setDataDirect(entry.feed.row, entry.feed.y[1], z);
				table->setDataDirect(entry.feed.row, entry.feed.y[2], y);
			} else if (entry.type == "BMP180") {
				double pressure;
				if (B0_ERR_RC(b0_bmp180_read(entry.driver.bmp180,
											&pressure, NULL))) {
					qDebug() << "b0_bmp180_read() failed";
					goto failed;
				}

				table->setDataDirect(entry.feed.row, entry.feed.y[0], pressure);
			} else if (entry.type == "TMP006") {
				double temp;
				if (B0_ERR_RC(b0_tmp006_read(
											entry.driver.tmp006, &temp))) {
					qDebug() << "b0_tmp006_read() failed";
					goto failed;
				}
				temp = B0_KELVIN_TO_CELSIUS(temp);

				table->setDataDirect(entry.feed.row, entry.feed.y[0], temp);
			} else if (entry.type == "TSL2591") {
				double illuminance;
				if (B0_ERR_RC(b0_tsl2591_read(entry.driver.tsl2591,
																&illuminance))) {
					qDebug() << "b0_tsl2591_read() failed";
					goto failed;
				}

				table->setDataDirect(entry.feed.row, entry.feed.y[0], illuminance);
			} else if (entry.type == "LM75") {
				b0_result_code (*read_fn)(b0_lm75 *, double *);
				read_fn = entry.config.value("READ_WORD")  == "16BIT" ?
					b0_lm75_read16 : b0_lm75_read;
				double temp;
				if (B0_ERR_RC(read_fn(entry.driver.lm75, &temp))) {
					qDebug() << "reading from lm75 failed";
					goto failed;
				}
				temp = B0_KELVIN_TO_CELSIUS(temp);

				table->setDataDirect(entry.feed.row, entry.feed.y[0], temp);
			} else if (entry.type == "L3GD20") {
				double /* radian per second */ x, y, z;
				if (B0_ERR_RC(b0_l3gd20_read(
									entry.driver.l3gd20, &x, &y, &z))) {
					qDebug() << "b0_l3gd20_read() failed";
					goto failed;
				}

				x = B0_RADIAN_TO_DEGREE(x);
				y = B0_RADIAN_TO_DEGREE(y);
				z = B0_RADIAN_TO_DEGREE(x);

				table->setDataDirect(entry.feed.row, entry.feed.y[0], x);
				table->setDataDirect(entry.feed.row, entry.feed.y[1], z);
				table->setDataDirect(entry.feed.row, entry.feed.y[2], y);
			} else if (entry.type == "SI114X") {
				double uv_index;
				if (B0_ERR_RC(b0_si114x_uv_index_read(
											entry.driver.si114x, &uv_index))) {
					qDebug() << "b0_si114x_read() failed";
					goto failed;
				}

				table->setDataDirect(entry.feed.row, entry.feed.y[0], uv_index);
			} else if (entry.type == "TCS3472") {
				double r, g, b;
				if (B0_ERR_RC(b0_tcs34725_read(entry.driver.tcs34725,
								&r, &g, &b, NULL))) {
					qDebug() << "b0_tcs34725_read() failed";
					goto failed;
				}

				table->setDataDirect(entry.feed.row, entry.feed.y[0], r);
				table->setDataDirect(entry.feed.row, entry.feed.y[1], g);
				table->setDataDirect(entry.feed.row, entry.feed.y[2], b);
			} else {
				qDebug() << "??? i2c type value";
			}

			/* only executed on success */
			{
				/* add a x entry */
				qreal value = (current_time - start_time) / 1000.0; /* now in sec */
				table->setDataDirect(entry.feed.row, entry.feed.x, value);
				entry.feed.row++;
			}
			continue;

			failed:
			/* now their are less rows to fetch */
			if (entry.feed.total_row > 0) {
				entry.feed.total_row--;
			}
		}
	}
}


void LsBox0I2cModel::onStop()
{
	for (int i = 0; i < entries.size(); i++) {
		LsBox0I2cModelEntry &entry = entries[i];
		if (entry.enable) {
			if (entry.type == "ADXL345") {
				if (entry.driver.adxl345 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_adxl345_close(entry.driver.adxl345),
						"failed to close ADXL345"
					);
					entry.driver.adxl345 = NULL;
				}
			} else if (entry.type == "BMP180") {
				if (entry.driver.bmp180 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_bmp180_close(entry.driver.bmp180),
						"failed to close BMP180"
					);
					entry.driver.bmp180 = NULL;
				}
			} else if (entry.type == "TMP006") {
				if (entry.driver.tmp006 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_tmp006_close(entry.driver.tmp006),
						"failed to close TMP006"
					);
					entry.driver.tmp006 = NULL;
				}
			} else if (entry.type == "TSL2591") {
				if (entry.driver.tsl2591 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_tsl2591_close(entry.driver.tsl2591),
						"failed to close TSL2591"
					);
					entry.driver.tsl2591 = NULL;
				}
			} else if (entry.type == "LM75") {
				if (entry.driver.lm75) {
					LsBox0ResultCode::handleInternal(
						b0_lm75_close(entry.driver.lm75),
						"failed to close LM75"
					);
					entry.driver.lm75 = NULL;
				}
			} else if (entry.type == "L3GD20") {
				if (entry.driver.l3gd20 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_l3gd20_close(entry.driver.l3gd20),
						"failed to close L3GD20"
					);
					entry.driver.l3gd20 = NULL;
				}
			} else if (entry.type == "SI114X") {
				if (entry.driver.si114x != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_si114x_close(entry.driver.si114x),
						"failed to close Si114x"
					);
					entry.driver.si114x = NULL;
				}
			} else if (entry.type == "TCS3472") {
				if (entry.driver.tcs34725 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_tcs34725_close(entry.driver.tcs34725),
						"failed to close TCS3472"
					);
					entry.driver.si114x = NULL;
				}
			} else {
				qDebug() << "BUG: LsBox0I2cModel::onStop ???";
			}
		}
	}
}

/**
 * Build and return Channel configuration as JSON array
 * @return JSON object containing channels configuration
 */
QJsonArray LsBox0I2cModel::toJsonArray()
{
	QJsonArray ch_list;

	for (int i = 0; i < entries.count(); i++) {
		const LsBox0I2cModelEntry &entry = entries.at(i);
		int sensor_index = LsBox0Sensor::sensor_index(entry.type, LsBox0Sensor::i2c);
		const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

		QJsonObject ch {
			{"enable", entry.enable},
			{"name", entry.name},
			{"type", sensor.id},
			{"address", (int) entry.address},
			{"config", QJsonObject::fromVariantHash(entry.config)}
		};

		ch_list.push_back(ch);
	}

	return ch_list;
}

/**
 * Validate if the sensor is valid for the given sensor
 * @param type Sensor type
 * @param address Sensor Address
 * @return true on valid else false
 */
static bool address_valid(const QString &type, uint address)
{
	int sensor_index = LsBox0Sensor::sensor_index(type, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

	Q_FOREACH(const LsBox0Sensor::I2c::Address &addr, sensor.addresses) {
		if (addr.value == address) {
			return true;
		}
	}

	return false;
}

/**
 * Validate if the config is valid for the given sensor
 * @param type Sensor type
 * @param address Sensor Address
 * @param config configuration to verify
 * @return true on valid else false
 */
static bool config_valid(const QString &type, uint address,
			const QHash<QString, QVariant> &config)
{
	int sensor_index = LsBox0Sensor::sensor_index(type, LsBox0Sensor::i2c);
	const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

	/* check that any config is not missing */
	Q_FOREACH(LsBox0Sensor::Config *c, sensor.config) {
		if (!config.contains(c->id) || !c->valueValid(config.value(c->id))) {
			return false;
		}
	}

	Q_FOREACH(const LsBox0Sensor::I2c::Address &addr, sensor.addresses) {
		if (addr.value != address) {
			continue;
		}

		/* check that all @a config values matches with the address config values */
		Q_FOREACH(const LsBox0Sensor::I2c::Address::Equal &e, addr.equal) {
			if (!config.contains(e.id) || config.value(e.id) != e.value) {
				return false;
			}
		}

		return true;
	}

	return false;
}

/**
 * Read mode data (channels) from @a ch_list Array
 * @param ch_list JSON Array of channel configuration
 */
void LsBox0I2cModel::fromJsonArray(QJsonArray ch_list)
{
	entries.clear();

	beginResetModel();

	for (int i = 0; i < ch_list.count(); i++) {
		QJsonObject ch = ch_list.at(i).toObject();

		QString id = ch.value("type").toString();
		bool valid = LsBox0Sensor::sensor_exists(id, LsBox0Sensor::i2c);

		if (!valid) {
			/* the sensor is not found, we cannot do anything :( */
			qWarning() << "I2C:" << id << "not found in sensor list";
			continue;
		}

		/* add the entry */
		LsBox0I2cModelEntry entry;
		entry.enable = ch.value("enable").toBool();
		entry.name = ch.value("name").toString();
		entry.type = id;
		entry.address = ch.value("address").toInt();
		entry.config = ch.value("config").toObject().toVariantHash();
		entry.driver.adxl345 = NULL;

		if (!address_valid(entry.type, entry.address)) {
			/* the sensor address is not found,
			 *  best we can do is auto-select the first address */
			qWarning() << Q_FUNC_INFO << entry.address << "not found in"
				<< entry.type << ", fallback: selecting first address"
				<< " and reject all provided configuration";

			int sensor_index = LsBox0Sensor::sensor_index(entry.type, LsBox0Sensor::i2c);
			const LsBox0Sensor::I2c &sensor = LsBox0Sensor::i2c.at(sensor_index);

			entry.address = sensor.addresses.first().value;
			build_entry_config(entry.type, entry.address, entry.config);
		} else if (!config_valid(entry.type, entry.address, entry.config)) {
			/* we have a valid sensor address,
			 *  but the configuration provided is not valid.
			 * In future, we could try to fix the configuration
			 *  (for missing or invalid) values
			 * but for now, reject it altogether and re-build a fresh config. */
			qWarning() << Q_FUNC_INFO << entry.address << "found in"
				<< entry.type << "but configuration has problem."
				<< "fallback: reject and generating from defaults based on address";

			build_entry_config(entry.type, entry.address, entry.config);
		}

		entries.append(entry);
	}

	endResetModel();
}
