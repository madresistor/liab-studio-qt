/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_I2C_H
#define LIAB_STUDIO_INTERFACES_BOX0_I2C_H

#include <QWidget>
#include <QJsonObject>
#include "interfaces/box0/sources/source/source.h"
#include <libbox0/libbox0.h>

#include "model.h"
#include "interfaces/box0/extra/threaded_timer.h"

#include "ui_box0_i2c.h"

class LsBox0I2c : public LsBox0Source
{
	Q_OBJECT

	public:
		LsBox0I2c(QWidget *parent = Q_NULLPTR);
		QJsonObject jsonConfig();
		void setJsonConfig(QJsonObject config);
		void setDevice(b0_device *dev);

	public:
		void onStop();
		int onStart(LsTable *table, LsGraph *graph, bool assign_marker, uint time);

	public Q_SLOTS:
		void addRow();
		void configureRow();
		void removeRow();
		void showMenuColumn(const QPoint & pos);

	private Q_SLOTS:
		void timeout();

	private:
		void removeRow(int index);
		void configureRow(int index);

		Ui::LsBox0I2c ui;
		LsBox0I2cModel *model = Q_NULLPTR;
		b0_i2c *module = NULL;
		qint64 start_time;
		LsTable *table;
		LsBox0ThreadedTimer *m_sampler;
		bool m_running = false;
};

#endif
