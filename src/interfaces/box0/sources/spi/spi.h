/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_SPI_H
#define LIAB_STUDIO_INTERFACES_BOX0_SPI_H

#include <QWidget>
#include <QObject>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QModelIndex>
#include <QObject>
#include <QSize>
#include <QTableView>
#include <QJsonObject>

#include <libbox0/libbox0.h>

#include "interfaces/box0/extra/threaded_timer.h"
#include "interfaces/box0/sources/source/source.h"
#include "model.h"

#include "ui_box0_spi.h"

class LsBox0Spi : public LsBox0Source
{
	Q_OBJECT

	public:
		LsBox0Spi(QWidget *parent = Q_NULLPTR);

	public:
		void onStop();
		int onStart(LsTable *table, LsGraph *graph, bool assign_marker, uint time);
		QJsonObject jsonConfig();
		void setJsonConfig(QJsonObject config);

	public:
		void setDevice(b0_device *dev);

	private Q_SLOTS:
		void timeout();
		void showMenuColumn(const QPoint &);
		void configureRow();

	private:
		void configureRow(int index);

		Ui::LsBox0Spi ui;
		LsBox0SpiModel *model = Q_NULLPTR;
		b0_spi *module = NULL;
		qint64 start_time;
		LsTable *table;
		LsBox0ThreadedTimer *m_sampler;
		bool m_running = false;
};

#endif
