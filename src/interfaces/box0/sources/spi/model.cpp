/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include <QDebug>
#include <muParser.h>
#include "delegates/type.h"
#include <cmath>
#include "interfaces/box0/extra/sensor.h"
#include "interfaces/box0/extra/result_code.h"
#include "interfaces/box0/extra/ease.h"
#include <libbox0/misc/conv.h>

using namespace std;

#define HORIZONTAL_HEADER_COUNT 3
static const QVariant horizontal_header[HORIZONTAL_HEADER_COUNT] = {
	"Enable",
	"Name",
	"Type",
};

/**
 * Build configuration based on @a type in @a config
 * @param type Sensor type
 * @param[out] config Configuration
 */
static void build_entry_config(const QString &type, QHash<QString, QVariant> &config)
{
	config.clear();

	int sensor_index = LsBox0Sensor::sensor_index(type, LsBox0Sensor::spi);
	const LsBox0Sensor::Spi &sensor = LsBox0Sensor::spi.at(sensor_index);

	Q_FOREACH(const LsBox0Sensor::Config *c, sensor.config) {
		config[c->id] = c->currentValue();
	}
}

LsBox0SpiModel::LsBox0SpiModel(b0_spi *mod, QWidget *parent) :
	QAbstractTableModel(parent),
	module(mod)
{
	for (uint i = 0; i < module->ss_count; i++) {
		LsBox0SpiModelEntry entry;
		if (module->label.ss[i] != NULL) {
			entry.channel = utf8_to_qstring(module->label.ss[i]);
		} else {
			entry.channel = QString("SS%1").arg(i);
		}

		entry.name = entry.channel;
		entry.enable = false;
		entry.type = "MAX31855";
		entry.driver.max31855 = NULL; /* null the driver entry */
		build_entry_config(entry.type, entry.config);

		entries.append(entry);
	}
}

QHash<QString, QVariant> LsBox0SpiModel::config(int idx)
{
	if (idx >= 0 && idx < entries.count()) {
		return entries.at(idx).config;
	}

	qDebug() << Q_FUNC_INFO << "index out of range";
	return QHash<QString, QVariant>();
}

void LsBox0SpiModel::setConfig(int idx, const QHash<QString, QVariant> &config)
{
	if (idx >= 0 && idx < entries.count()) {
		entries[idx].config = config;
		return;
	}

	qDebug() << Q_FUNC_INFO << "index out of range";
}

QString LsBox0SpiModel::type(int idx)
{
	if (idx >= 0 && idx < entries.count()) {
		return entries.at(idx).type;
	}

	qDebug() << Q_FUNC_INFO << "index out of range";
	return QString::null;
}

int LsBox0SpiModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return entries.size();
}

int LsBox0SpiModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return HORIZONTAL_HEADER_COUNT;
}

QVariant LsBox0SpiModel::data(const QModelIndex &index, int role) const
{
	int row = index.row();
	if (row >= 0 && row < entries.size()) {
		const LsBox0SpiModelEntry &col = entries.at(row);

		int column = index.column();
		if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
			switch(column) {
			case 1:
			return QVariant(col.name);
			case 2:
			return QVariant(col.type);
			}
		} else if (role == Qt::CheckStateRole) {
			switch(column) {
			case 0:
			return col.enable ? Qt::Checked : Qt::Unchecked;
			}
		} /* else if (role == Qt::SizeHintRole) {
			switch(column) {
			case 0:
			return QSize(10, 10);
			case 2:
			return QSize(20, 10);
			}
		} */
	}

	return QVariant();
}

QVariant LsBox0SpiModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal) {
			if (section >= 0 && section < HORIZONTAL_HEADER_COUNT) {
				return horizontal_header[section];
			}
		} else if (section < entries.size()){
			return entries.at(section).channel;
		}
	}

	return QVariant();
}

bool LsBox0SpiModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (!editable) {
		return false;
	}

	int row = index.row();
	LsBox0SpiModelEntry &col = entries[row];

	if (role == Qt::EditRole) {
		switch(index.column()) {
		case 1:
			col.name = value.toString();
		break;
		case 2: {
			QString type = value.toString();
			if (type != col.type) {
				col.type = type;
				build_entry_config(col.type, col.config);
			}
		} break;
		default:
		return false;
		}

	} else if (role == Qt::CheckStateRole) {
		switch(index.column()) {
		case 0:
			col.enable = (value.toInt() == Qt::Checked);
		break;
		default:
		return false;
		}
	}

	Q_EMIT dataChanged(index, index);
	return true;
}

Qt::ItemFlags LsBox0SpiModel::flags(const QModelIndex & index) const
{
	switch(index.column()) {
	case 0:
	return  Qt::ItemIsSelectable |
			Qt::ItemIsEnabled |
			(editable ? Qt::ItemIsUserCheckable : Qt::NoItemFlags);

	case 1: case 2: case 3: case 4:
	return  Qt::ItemIsSelectable |
			Qt::ItemIsEnabled |
			(editable ? Qt::ItemIsEditable : Qt::NoItemFlags);

	default:
	return 0;
	}
}

bool LsBox0SpiModel::prepareColumns(LsTable *table, LsGraph *graph,
	bool assign_marker, size_t sample_rate, size_t total_time)
{
	size_t total_samples = sample_rate * total_time;

	for (int i = 0; i < entries.size(); i++) {
		LsBox0SpiModelEntry &entry = entries[i];
		if (entry.enable) {

			/* prepare the entries */
			entry.feed.row = 0;
			entry.feed.total_row = total_samples;

			entry.feed.x = table->appendColumn(entry.name + " time");
			table->tableData->columns.at(entry.feed.x)->sig_digits = 6;

			QList<QString> y_axis_xyz;
			y_axis_xyz << "X" << "Y" << "Z";

			QList<QString> y_axis_entries;

			QString sensor_driver =
				LsBox0Sensor::sensor_driver(entry.type, LsBox0Sensor::spi);

			if (sensor_driver == "MAX31855") {
				if (B0_ERR_RC(b0_max31855_open(module,
											&entry.driver.max31855, i))) {
					qDebug() << "failed to open max31855";
					entry.driver.max31855 = NULL;
					return false;
				}
			} else {
				qDebug() << "??? spi type value";
			}

			entry.feed.y.clear();
			if (y_axis_entries.size() < 1) {
				int col_y = table->appendColumn(entry.name + " value");
				entry.feed.y.append(col_y);

				if (graph != Q_NULLPTR) {
					LsMarker::Type marker = static_cast<LsMarker::Type>(LsMarker::PLUS + i);
					graph->addCurve(entry.name,
						table, entry.feed.x,
						table, col_y,
						table->tableData->columns.at(col_y)->color,
						true, LsGraphCurve::DEF_LINE_WIDTH,
						assign_marker, marker);
				}
			} else {
				for (int j = 0; j < y_axis_entries.size(); j++) {
					QString name = entry.name + " " + y_axis_entries.at(j);
					int col_y = table->appendColumn(name);
					entry.feed.y.append(col_y);

					if (graph != Q_NULLPTR) {
						LsMarker::Type marker = static_cast<LsMarker::Type>(LsMarker::PLUS + j);
						graph->addCurve(name,
							table, entry.feed.x,
							table, col_y,
							table->tableData->columns.at(col_y)->color,
							true, LsGraphCurve::DEF_LINE_WIDTH,
							assign_marker, marker);
					}
				}
			}
		}
	}

	/* everything was succesful */
	return true;
}

bool LsBox0SpiModel::anyColumnEnabled()
{
	for (int i = 0; i < entries.size(); i++) {
		if (entries.at(i).enable) {
			return true;
		}
	}

	return false;
}

void LsBox0SpiModel::userEditable(bool e)
{
	beginResetModel();
	editable = e;
	endResetModel();
}

//gather data from device
void LsBox0SpiModel::gather(LsTable *table, qint64 start_time)
{
	for (int i = 0; i < entries.size(); i++) {
		LsBox0SpiModelEntry &entry = entries[i];
		if (entry.enable) {

			/* keep a bar on total samples to recevied */
			if (entry.feed.total_row > 0) {
				if (entry.feed.row >= entry.feed.total_row) {
					continue;
				}
			}

			qint64 current_time = QDateTime::currentMSecsSinceEpoch();

			double value = NAN;
			// since thermocouple is currently their in list
			// upgrade code for others
			// also note when multiple sensors are connected switch their
			// restore/save bitsize, mode, speed (etc...)
			if (entry.type == "MAX31855") {
				if (B0_ERR_RC(b0_max31855_read(
										entry.driver.max31855, &value))) {
					qDebug() << "b0_max31855_read failed";
					//~ entry.enable = false;
				}

				if (!isnan(value)) {
					value = B0_KELVIN_TO_CELSIUS(value);
				}
			} else {
				qDebug() << "??? spi type value";
			}

			/* nothing was readed, skip */
			if (isnan(value)) {
				/* we have to miss one value */
				if (entry.feed.total_row > 0) {
					entry.feed.total_row--;
				}

				continue;
			}

			table->setDataDirect(entry.feed.row, entry.feed.y[0], value);

			value = (current_time - start_time) / 1000.0; /* now in sec */
			table->setDataDirect(entry.feed.row, entry.feed.x, value);

			entry.feed.row++;
		}
	}
}


void LsBox0SpiModel::onStop()
{
	for (int i = 0; i < entries.size(); i++) {
		LsBox0SpiModelEntry &entry = entries[i];
		if (entry.enable) {
			if (entry.type == "MAX31855") {
				if (entry.driver.max31855 != NULL) {
					LsBox0ResultCode::handleInternal(
						b0_max31855_close(entry.driver.max31855),
						"failed to open MAX31855"
					);
					entry.driver.max31855 = NULL;
				}
			} else {
				qDebug() << "??? spi type value";
			}
		}
	}
}

/**
 * Build and return Channel configuration as JSON array
 * @return JSON object containing channels configuration
 */
QJsonArray LsBox0SpiModel::toJsonArray()
{
	QJsonArray ch_list;

	for (int i = 0; i < entries.count(); i++) {
		const LsBox0SpiModelEntry &entry = entries.at(i);
		int sensor_index = LsBox0Sensor::sensor_index(entry.type, LsBox0Sensor::spi);
		const LsBox0Sensor::Spi &sensor = LsBox0Sensor::spi.at(sensor_index);

		QJsonObject ch {
			{"enable", entry.enable},
			{"name", entry.name},
			{"type", sensor.id},
			{"config", QJsonObject::fromVariantHash(entry.config)}
		};

		ch_list.push_back(ch);
	}

	return ch_list;
}

/**
 * Validate if the config is valid for the given sensor
 * @param type Sensor type
 * @param config configuration to verify
 * @return true on valid else false
 */
static bool config_valid(const QString &type, const QHash<QString, QVariant> &config)
{
	int sensor_index = LsBox0Sensor::sensor_index(type, LsBox0Sensor::spi);
	const LsBox0Sensor::Spi &sensor = LsBox0Sensor::spi.at(sensor_index);

	/* check that any config is not missing */
	Q_FOREACH(LsBox0Sensor::Config *c, sensor.config) {
		if (!config.contains(c->id) || !c->valueValid(config.value(c->id))) {
			return false;
		}
	}

	return true;
}

/**
 * Read mode data (channels) from @a ch_list Array
 * @param ch_list JSON Array of channel configuration
 */
void LsBox0SpiModel::fromJsonArray(QJsonArray ch_list)
{
	int count = qMin(ch_list.count(), entries.count());

	beginResetModel();

	for (int i = 0; i < count; i++) {
		QJsonObject ch = ch_list.at(i).toObject();
		LsBox0SpiModelEntry &entry = entries[i];

		QString type = ch.value("type").toString();
		bool found = LsBox0Sensor::sensor_exists(type, LsBox0Sensor::spi);

		if (!found) {
			/* we have problem!
			 *  We do not have the driver for the sensor
			 * We cannot do anything. :(
			 */
			entry.enable = false;
			continue;
		}

		entry.enable = ch.value("enable").toBool(entry.enable);
		entry.name = ch.value("name").toString(entry.name);
		entry.type = type;
		entry.config = ch.value("config").toObject().toVariantHash();

		if (!config_valid(entry.type, entry.config)) {
			/* the configuration provided is not valid.
			 * In future, we could try to fix the configuration
			 *  (for missing or invalid) values
			 * but for now, reject it altogether and re-build a fresh config. */
			qWarning() << Q_FUNC_INFO << "configuration has problem."
				<< "fallback: reject and generating from defaults";

			build_entry_config(entry.type, entry.config);
		}
	}

	endResetModel();
}
