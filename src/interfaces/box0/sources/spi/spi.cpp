/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spi.h"
#include "model.h"
#include "delegates/type.h"
#include "interfaces/box0/extra/result_code.h"
#include <QMenu>
#include "interfaces/box0/extra/config/config.h"
#include "interfaces/box0/extra/sensor.h"

LsBox0Spi::LsBox0Spi(QWidget *parent) :
	LsBox0Source(parent)
{
	ui.setupUi(this);

	m_sampler = new LsBox0ThreadedTimer(this);
	connect(m_sampler, SIGNAL(timeout()), this, SLOT(timeout()), Qt::DirectConnection);

	connect(ui.btnConfigure, SIGNAL(clicked()), SLOT(configureRow()));

	/* show Delete popup on right click on column header */
	QHeaderView *headerView = ui.table->verticalHeader();
	headerView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(headerView, SIGNAL(customContextMenuRequested(const QPoint &)),
		SLOT(showMenuColumn(const QPoint &)));
}

void LsBox0Spi::configureRow(int index)
{
	/* model is NULL, nothing can be done */
	if (model == Q_NULLPTR) {
		return;
	}

	QString t = model->type(index);
	QHash<QString, QVariant> values = model->config(index);

	/* any configuration? */
	if (values.isEmpty()) {
		QMessageBox::information(this, "No configuration", "There is nothing to configure");
		return;
	}

	int sensor_index = LsBox0Sensor::sensor_index(t, LsBox0Sensor::spi);
	const LsBox0Sensor::Spi &sensor = LsBox0Sensor::spi.at(sensor_index);

	/* TODO: consider config hidden for nothing to configure dialog */

	LsBox0Config d;
	d.setConfig(sensor.config, values);

	if (d.exec() == QDialog::Accepted) {
		model->setConfig(index, d.config());
	}
}

void LsBox0Spi::configureRow()
{
	int row = ui.table->selectedRow();
	if (row == -1) {
		return;
	}

	configureRow(row);
}

void LsBox0Spi::showMenuColumn(const QPoint & pos)
{
	int logical_index = ui.table->verticalHeader()->logicalIndexAt(pos);

	if (logical_index < 0) {
		qDebug() << "really occured?";
		return;
	}

	QMenu menu;
	QAction actionConfigure("Configure", &menu);

	menu.addAction(&actionConfigure);

	QAction *sel = menu.exec(QCursor::pos());
	if (sel == &actionConfigure) {
		configureRow(logical_index);
	}
}

void LsBox0Spi::timeout()
{
	model->gather(table, start_time);
	table->update();
}

void LsBox0Spi::onStop()
{
	if (!m_running) {
		return;
	}

	m_sampler->quit();
	m_sampler->wait();
	model->onStop();

	ui.btnConfigure->setEnabled(true);
	ui.table->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
	model->userEditable(true);
	m_running = false;
}

int LsBox0Spi::onStart(LsTable *table, LsGraph *graph, bool assign_marker, uint time)
{
	if (module == NULL) {
		qDebug() << "no SPI module";
		return 0;
	}

	if (model == Q_NULLPTR) {
		qDebug() << "no SPI model";
		return -1;
	}

	if (!model->anyColumnEnabled()) {
		qDebug() << "currently spi no column enabled";
		return 0;
	}

	int sampleRate = ui.sampleRate->value();
	if (sampleRate <= 0) {
		return 0;
	}

	if (!model->prepareColumns(table, graph, assign_marker, sampleRate, time)) {
		qDebug() << "something failed in spi";
		return -1;
	}

	this->table = table;

	start_time = QDateTime::currentMSecsSinceEpoch();
	m_sampler->start(1000 / sampleRate);

	ui.btnConfigure->setEnabled(false);
	ui.table->verticalHeader()->setContextMenuPolicy(Qt::PreventContextMenu);

	model->userEditable(false);
	m_running = true;

	return 1;
}

void LsBox0Spi::setDevice(b0_device *dev)
{
	if (dev == NULL) {
		/* remove device */

		if (module != NULL) {
			onStop();

			LsBox0ResultCode::handleInternal(
				b0_spi_close(module), "unable to unload SPI");
		}

		module = NULL;
	} else if (B0_ERR_RC(b0_spi_open(dev, &module, 0))) {
		qDebug() << "no spi in device";
		module = NULL;
	} else if (B0_ERR_RC(b0_spi_master_prepare(module))) {
		qDebug() << "unable to prepare SPI module for master";
		b0_spi_close(module);
		module = NULL;
	}

	setEnabled(module != NULL);

	if (module == NULL) {
		qDebug() << "no SPI module";
		model = Q_NULLPTR;
		ui.table->setModel(Q_NULLPTR);
		return;
	}

	model = new LsBox0SpiModel(module, this);
	ui.table->setModel(model);
	ui.table->setItemDelegateForColumn(2, new LsBox0SpiDelegateType(this));
}

QJsonObject LsBox0Spi::jsonConfig()
{
	return {
		{"channels", model->toJsonArray()},
		{"speed", ui.sampleRate->value()}
	};
}

void LsBox0Spi::setJsonConfig(QJsonObject config)
{
	ui.sampleRate->setValue(config.value("speed").toInt(0));
	model->fromJsonArray(config.value("channels").toArray());
}
