/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_SPI_MODEL_H
#define LIAB_STUDIO_INTERFACES_BOX0_SPI_MODEL_H

#include <QWidget>
#include <QObject>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QModelIndex>
#include <QObject>
#include <QSize>
#include <muParser.h>
#include <QHash>

#include "widgets/table/table.h"
#include "widgets/graph/graph.h"
#include "widgets/graph/curve.h"

#include <libbox0/libbox0.h>
#include <libbox0/driver/max31855/max31855.h>

struct LsBox0SpiModelEntry
{
	QString channel;

	QString name;
	bool enable;
	QString type;
	QHash<QString, QVariant> config;

	union {
		b0_max31855 *max31855;
	} driver;

	struct {
		int total_row;
		int row;

		int x;
		QList<int> y;
	} feed;
};

class LsBox0SpiModel : public QAbstractTableModel
{
	Q_OBJECT

	public:
		LsBox0SpiModel(b0_spi *spi, QWidget *parent = Q_NULLPTR);
		QVariant headerData(int section, Qt::Orientation orientation, int role) const;
		QVariant data(const QModelIndex &index, int role) const;
		int columnCount(const QModelIndex &parent) const;
		int rowCount(const QModelIndex &parent) const;
		Qt::ItemFlags flags(const QModelIndex & index) const;
		bool setData(const QModelIndex & index, const QVariant & value, int role);

		QHash<QString, QVariant> config(int index);
		void setConfig(int index, const QHash<QString, QVariant> &config);
		QString type(int index);

		bool anyColumnEnabled();
		bool prepareColumns(LsTable *table, LsGraph *graph,
			bool assign_marker, size_t sample_rate, size_t total_time);
		void userEditable(bool e);
		void gather(LsTable *table, qint64 time_passed_ms);

		void onStop();

		QJsonArray toJsonArray();
		void fromJsonArray(QJsonArray arr);

	private:
		QList<LsBox0SpiModelEntry> entries;
		bool editable = true;
		b0_spi *module;
};

#endif
