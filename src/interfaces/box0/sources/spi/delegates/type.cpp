/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "type.h"
#include <QComboBox>
#include <Qt>
#include <QDebug>
#include <QApplication>
#include "../model.h"
#include "interfaces/box0/extra/sensor.h"

LsBox0SpiDelegateType::LsBox0SpiDelegateType(QWidget *parent) :
	QItemDelegate(parent)
{
}

QWidget *LsBox0SpiDelegateType::createEditor(QWidget *parent,
	const QStyleOptionViewItem &/* option */, const QModelIndex & /* index */) const
{
	QComboBox *cmb = new QComboBox(parent);
	cmb->setInsertPolicy(QComboBox::InsertAtBottom);

	Q_FOREACH(const LsBox0Sensor::Spi &i, LsBox0Sensor::spi) {
		QString title = i.desc.isEmpty() ? i.title :
			QString("%1 (%2)").arg(i.title).arg(i.desc);
		cmb->addItem(title, i.id);
	}

	connect(cmb, SIGNAL(activated(int)), this, SLOT(commitAndCloseEditor()));

	return cmb;
}

void LsBox0SpiDelegateType::commitAndCloseEditor()
{
	QWidget *editor = qobject_cast<QWidget *>(sender());
	Q_EMIT commitData(editor);
	Q_EMIT closeEditor(editor);
}

void LsBox0SpiDelegateType::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	QString id = index.model()->data(index, Qt::EditRole).toString();
	QComboBox *cmb = static_cast<QComboBox *>(editor);
	int sensor_index = LsBox0Sensor::sensor_index(id, LsBox0Sensor::spi);
	cmb->setCurrentIndex(sensor_index);
}

void LsBox0SpiDelegateType::setModelData(QWidget *editor,
	QAbstractItemModel *model, const QModelIndex & index) const
{
	QComboBox *cmb = static_cast<QComboBox *>(editor);
	model->setData(index, cmb->currentData(), Qt::EditRole);
}

void LsBox0SpiDelegateType::updateEditorGeometry(QWidget *editor,
	const QStyleOptionViewItem &option, const QModelIndex & /* index */) const
{
	editor->setGeometry(option.rect);
}

void LsBox0SpiDelegateType::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(option.rect, option.palette.highlight());
	}

	painter->save();
	painter->setRenderHint(QPainter::Antialiasing, true);
	if (option.state & QStyle::State_Selected) {
		painter->setPen(option.palette.color(QPalette::HighlightedText));
		painter->setBrush(option.palette.highlightedText());
	} else {
		painter->setPen(option.palette.color(QPalette::Text));
		painter->setBrush(option.palette.text());
	}

	QString id = index.data().toString();
	int _index = LsBox0Sensor::sensor_index(id, LsBox0Sensor::spi);
	const QString &title = LsBox0Sensor::spi.at(_index).title;
	QStyle* style = QApplication::style();
	QRect textRect = style->subElementRect(QStyle::SE_ItemViewItemText, &option);
	painter->drawText(textRect, option.displayAlignment, title);
	painter->restore();
}
