/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "result_code.h"
#include <libbox0/libbox0.h>
#include <QDebug>
#include "ease.h"

LsBox0ResultCode::LsBox0ResultCode(QWidget *parent) :
	QMessageBox(parent)
{
}

LsBox0ResultCode::LsBox0ResultCode(b0_result_code r, QWidget *parent) :
	QMessageBox(parent)
{
	setResultCode(r);
}

void LsBox0ResultCode::setResultCode(b0_result_code r)
{
	QString name, explain;
	setIcon(B0_ERR_RC(r) ?
		QMessageBox::Warning : QMessageBox::Information);

	name = utf8_to_qstring(b0_result_name(r));
	explain = utf8_to_qstring(b0_result_explain(r));

	setDetailedText(name + "\n" + explain);

	setStandardButtons(QMessageBox::Ok);
	setEscapeButton(QMessageBox::Ok);
}

void LsBox0ResultCode::setTitle(const QString & title)
{
	setWindowTitle(title);
}

void LsBox0ResultCode::setMessage(const QString & msg)
{
	setText(msg);
}

/**
 * @return false if error result code
 */
bool LsBox0ResultCode::handleInternal(b0_result_code r, const QString & msg)
{
	if (B0_SUCCESS_RESULT_CODE(r)) {
		return true;
	}

	QString name = utf8_to_qstring(b0_result_name(r));
	QString explain = utf8_to_qstring(b0_result_explain(r));

	qWarning() << msg  << "[" << name << ": " << explain << "]";
	return false;
}
