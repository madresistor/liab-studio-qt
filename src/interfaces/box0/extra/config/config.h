/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_I2C_CONFIG_H
#define LIAB_STUDIO_INTERFACES_BOX0_I2C_CONFIG_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QList>
#include <QHash>
#include <QLabel>
#include "interfaces/box0/extra/sensor.h"

#include "ui_box0_config.h"

class LsBox0Config : public QDialog
{
	Q_OBJECT

	public:
		LsBox0Config(QWidget *parent = Q_NULLPTR);

		void setConfig(const QList<LsBox0Sensor::Config *> &list,
			const QHash<QString, QVariant> &values,
			const QStringList &readonly_hidden = QStringList());
		QHash<QString, QVariant> config();

	private:
		QWidget *addCheckbox(LsBox0Sensor::ConfigCheckbox *ccb, QVariant v);
		QWidget *addNumber(LsBox0Sensor::ConfigNumber *cn, QVariant v);
		QWidget *addDropdown(LsBox0Sensor::ConfigDropdown *cdd, QVariant v);
		QWidget *addFunction(LsBox0Sensor::ConfigFunction *cf, QVariant v);

		Ui::LsBox0Config ui;

		QHash<QString, QVariant> m_hidden;
};

#endif
