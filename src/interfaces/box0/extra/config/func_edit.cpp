/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "func_edit.h"

#include <QMenu>
#include "extra/parser/operator.h"
#include "extra/parser/function.h"
#include "extra/parser/constant.h"

LsBox0ConfigFuncEdit::LsBox0ConfigFuncEdit(QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	m_variable_menu = new LsParserMenu(this);
	ui.addVariable->setMenu(m_variable_menu);
	connect(m_variable_menu, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsVariable(const QString &)));

	LsParserFunction *func = new LsParserFunction(this);
	ui.addFunction->setMenu(func);
	connect(func, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsFunction(const QString &)));

	LsParserOperator *oper = new LsParserOperator(this);
	ui.addOperator->setMenu(oper);
	connect(oper, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsOperator(const QString &)));

	LsParserConstant *constant = new LsParserConstant(this);
	ui.addConstant->setMenu(constant);
	connect(constant, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsConstant(const QString &)));
}

void LsBox0ConfigFuncEdit::setFunc(const QString &func,
		const QList<LsBox0Sensor::ConfigFunction::Input> &inputs)
{
	ui.equation->setText(func);

	m_variable_menu->clear();
	m_parser = LsParser(); /* clear previous */

	Q_FOREACH(const LsBox0Sensor::ConfigFunction::Input &i, inputs) {
		m_variable_menu->addEntry(i.id, i.title);
		m_parser.defineConstant(i.id, 0);
	}
}

QString LsBox0ConfigFuncEdit::func()
{
	return ui.equation->text();
}

/**
 * Done when user want to close the dialog.
 * If equation input is valid, then equation is validated.
 * if the equation is invalid, the dialog is not closed.
 * @param r Result
 */
void LsBox0ConfigFuncEdit::done(int r)
{
	if (r == QDialog::Accepted) {
		/* confirm that the equation is valid */
		if (!equationValid()) {
			return;
		}
	}

	QDialog::done(r);
}

/**
 * Validate the function and return the result
 * @return true on success
 */
bool LsBox0ConfigFuncEdit::equationValid()
{
	m_parser.setExpression(ui.equation->text());

	try {
		m_parser.Eval();
	} catch (mu::Parser::exception_type &e) {
		QString msg = QString::fromStdString(e.GetMsg());
		ui.msg->setText(msg);
		return false;
	}

	return true;
}
