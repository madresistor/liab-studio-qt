/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "func.h"

#include "func_edit.h"

LsBox0ConfigFunc::LsBox0ConfigFunc(QWidget *parent) :
	QWidget(parent)
{
	ui.setupUi(this);

	connect(ui.edit, SIGNAL(clicked()), SLOT(userWantToEdit()));
}

void LsBox0ConfigFunc::setFunc(const QString &value,
		const QList<LsBox0Sensor::ConfigFunction::Input> &inputs)
{
	ui.result->setText(value);
	m_inputs = inputs;
}

QString LsBox0ConfigFunc::func()
{
	return ui.result->text();
}

void LsBox0ConfigFunc::userWantToEdit()
{
	LsBox0ConfigFuncEdit e;
	e.setFunc(ui.result->text(), m_inputs);

	if (e.exec() == QDialog::Accepted) {
		ui.result->setText(e.func());
	}
}
