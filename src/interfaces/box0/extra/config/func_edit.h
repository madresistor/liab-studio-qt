/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_CONFIG_FUNC_EDIT_H
#define LIAB_STUDIO_INTERFACES_BOX0_CONFIG_FUNC_EDIT_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include "extra/parser/parser.h"
#include <QList>
#include "interfaces/box0/extra/sensor.h"
#include "extra/parser/menu.h"

#include "ui_box0_config_func_edit.h"

class LsBox0ConfigFuncEdit : public QDialog
{
	Q_OBJECT

	public:
		LsBox0ConfigFuncEdit(QWidget *parent = Q_NULLPTR);
		void setFunc(const QString &func,
				const QList<LsBox0Sensor::ConfigFunction::Input> &inputs);
		QString func();

	public Q_SLOTS:
		void done(int r);

	private:
		bool equationValid();
		Ui::LsBox0ConfigFuncEdit ui;
		LsParserMenu *m_variable_menu;
		LsParser m_parser;
};

#endif
