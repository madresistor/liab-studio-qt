/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "func.h"
#include <QDoubleValidator>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QDebug>

LsBox0Config::LsBox0Config(QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);
}


void LsBox0Config::setConfig(const QList<LsBox0Sensor::Config *> &list,
		const QHash<QString, QVariant> &values,
		const QStringList &readonly_hidden)
{
	Q_FOREACH(LsBox0Sensor::Config *i, list) {
		QVariant v = values.value(i->id);

		if (i->hidden || readonly_hidden.indexOf(i->id) != -1) {
			/* this setting is inisde the list of already configured */
			m_hidden[i->id] = v;
			continue;
		}

		QString prop_type;
		QWidget *input;
		switch (i->type) {
		case LsBox0Sensor::Config::CHECKBOX:
			input = addCheckbox(dynamic_cast<LsBox0Sensor::ConfigCheckbox *>(i), v);
			prop_type = "checkbox";
		break;
		case LsBox0Sensor::Config::NUMBER:
			input = addNumber(dynamic_cast<LsBox0Sensor::ConfigNumber *>(i), v);
			prop_type = "number";
		break;
		case LsBox0Sensor::Config::FUNCTION:
			input = addFunction(dynamic_cast<LsBox0Sensor::ConfigFunction *>(i), v);
			prop_type = "function";
		break;
		case LsBox0Sensor::Config::DROPDOWN:
			input = addDropdown(dynamic_cast<LsBox0Sensor::ConfigDropdown *>(i), v);
			prop_type = "dropdown";
		break;
		}

		input->setProperty("config-type", prop_type);
		input->setProperty("config-id", i->id);
		input->setEnabled(!i->readonly);
		ui.formLayout->addRow(new QLabel(i->title, input), input);
	}
}

QHash<QString, QVariant> LsBox0Config::config()
{
	QHash<QString, QVariant> result = m_hidden;

	for (int i = 0; i < ui.formLayout->rowCount(); i++) {
		QLayoutItem *li = ui.formLayout->itemAt(i, QFormLayout::FieldRole);
		if (li == Q_NULLPTR) {
			qDebug() << "item got null [unexpected]";
			continue;
		}

		QWidget *input = li->widget();
		if (input == Q_NULLPTR) {
			qDebug() << "input got null [unexpected]";
			continue;
		}

		QString type = input->property("config-type").toString();
		QString id = input->property("config-id").toString();

		if (type == "checkbox") {
			QCheckBox *cb = qobject_cast<QCheckBox *>(input);
			QVariant checked = input->property("config-checked");
			QVariant unchecked = input->property("config-unchecked");
			result[id] = cb->isChecked() ? checked : unchecked;
		} else if (type == "dropdown") {
			QComboBox *cb = qobject_cast<QComboBox *>(input);
			result[id] = cb->currentData();
		} else if (type == "function") {
			LsBox0ConfigFunc *bcf = qobject_cast<LsBox0ConfigFunc *>(input);
			result[id] = bcf->func();
		} else if (type == "number") {
			QLineEdit *le = qobject_cast<QLineEdit *>(input);
			result[id] = le->text().toDouble();
		} else {
			qDebug() << "unknown property value [config-type]: " << type;
		}
	}

	return result;
}

QWidget *LsBox0Config::addCheckbox(LsBox0Sensor::ConfigCheckbox *ccb, QVariant v)
{
	QCheckBox *cb = new QCheckBox(this);
	cb->setChecked(v == ccb->checked);

	cb->setProperty("config-checked", ccb->checked);
	cb->setProperty("config-unchecked", ccb->unchecked);

	return cb;
}

QWidget *LsBox0Config::addNumber(LsBox0Sensor::ConfigNumber *cn, QVariant v)
{
	QLineEdit *le = new QLineEdit(this);
	le->setValidator(new QDoubleValidator(cn->min, cn->max, 16, le));
	le->setText(QString::number(v.toDouble()));

	return le;
}

QWidget *LsBox0Config::addDropdown(LsBox0Sensor::ConfigDropdown *cdd, QVariant v)
{
	QComboBox *cb = new QComboBox(this);
	int selected = -1;

	QString _v = v.toString();

	for (int i = 0; i < cdd->entries.count(); i++) {
		const LsBox0Sensor::ConfigDropdown::Entry &e = cdd->entries.at(i);
		cb->addItem(e.title, e.value);
		if (e.value == _v) {
			selected = i;
		}
	}

	if (selected != -1) {
		cb->setCurrentIndex(selected);
	}

	return cb;
}

QWidget *LsBox0Config::addFunction(LsBox0Sensor::ConfigFunction *cf, QVariant v)
{
	Q_UNUSED(cf);

	LsBox0ConfigFunc *bcf = new LsBox0ConfigFunc(this);
	bcf->setFunc(v.toString(), cf->inputs);
	return bcf;
}
