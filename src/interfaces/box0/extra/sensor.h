/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_SENSOR_H
#define LIAB_STUDIO_INTERFACES_BOX0_SENSOR_H

#include <QString>
#include <QVariant>
#include <QList>
#include <QXmlStreamReader>
#include <cmath>

class LsBox0Sensor
{
	public:
		struct Config {
			public:
			enum Type {CHECKBOX, NUMBER, FUNCTION, DROPDOWN} type;
			QString id, title, desc;
			bool hidden = false, readonly = false;
			virtual ~Config() = default;

			/**
			 * Create a copy of the object
			 * @return clone
			 */
			virtual Config *clone() const = 0;

			/**
			 * Validate if the value is valid
			 * @param v Value
			 * @return true on valid else false
			 */
			virtual bool valueValid(const QVariant &v) const = 0;

			/**
			 * Return the current value
			 * @return Value
			 */
			virtual QVariant currentValue() const = 0;
		};

		struct ConfigNumber : public Config {
			public:
			double min = -INFINITY, max = +INFINITY, value = 0;
			ConfigNumber() {type = NUMBER;}
			ConfigNumber(const ConfigNumber &c) = default;

			Config *clone() const { return new ConfigNumber(*this); };
			bool valueValid(const QVariant &v) const;
			QVariant currentValue() const { return value; };
		};

		struct ConfigCheckbox : public Config {
			public:
			QString checked = "YES", unchecked = "NO", value = "NO";
			ConfigCheckbox() {type = CHECKBOX;}
			ConfigCheckbox(const ConfigCheckbox &c) = default;

			Config *clone() const { return new ConfigCheckbox(*this); };
			bool valueValid(const QVariant &v) const;
			QVariant currentValue() const { return value; };
		};

		struct ConfigDropdown : public Config {
			public:
			struct Entry {
				QString title, value;
			};
			QList<Entry> entries;
			QString value; /* selected value */
			ConfigDropdown() {type = DROPDOWN;}
			ConfigDropdown(const ConfigDropdown &c) = default;

			Config *clone() const { return new ConfigDropdown(*this); };
			bool valueValid(const QVariant &v) const;
			QVariant currentValue() const { return value; };
		};

		struct ConfigFunction : public Config {
			public:
			QString value;
			ConfigFunction() {type = FUNCTION;}
			ConfigFunction(const ConfigFunction &c) = default;

			struct Input {
				QString id, title;
			};

			QList<Input> inputs;

			Config *clone() const { return new ConfigFunction(*this); };
			bool valueValid(const QVariant &v) const;
			QVariant currentValue() const { return value; };
		};

		struct Spi {
			QString id, parent, title, desc;
			QList<QString> cat;
			QList<Config *> config;
		};

		struct I2c {
			QString id, parent, title, desc;
			QList<QString> cat;

			QList<Config *> config;

			struct Address {
				uint value;
				QString title;

				struct  Equal { QString id, value; };
				QList<Equal> equal;
			};

			QList<Address> addresses;
		};

		struct Ain {
			QString id, title, desc, func;
			QList<QString> cat;

			struct Parameter {
				QString unit;
				double max, min;
			} output, input;
		};

		static QList<Ain> ain;
		static QList<Spi> spi;
		static QList<I2c> i2c;

	public:
		static void update();

		/**
		 * Find a sensor with ID @a id from @a list
		 * @param id ID
		 * @param list Sensor list
		 * @return index if found
		 * @return -1 if not found
		 */
		static int sensor_index(const QString &id, const QList<Ain> &list);
		static int sensor_index(const QString &id, const QList<Spi> &list);
		static int sensor_index(const QString &id, const QList<I2c> &list);

		/**
		 * Get the ultimate driver class.
		 */
		//static QString sensor_driver(const QString &id, const QList<Ain> &list);
		static QString sensor_driver(const QString &id, const QList<Spi> &list);
		static QString sensor_driver(const QString &id, const QList<I2c> &list);

		template <class T>
		static bool sensor_exists(const QString &id, QList<T> &list) {
			return sensor_index(id, list) != -1;
		}

	private:
		static void configCopy(QList<Config *> &to, const QList<Config *> &from);

		static void readXmlSensorsConfig(QList<Config *> &config,
			QXmlStreamReader &reader, bool base_class);
		static void readXmlSensorsConfigHidden(QList<Config *> &config,
			QXmlStreamReader &reader);
		static void readXmlSensorsConfigReadonly(QList<Config *> &config,
			QXmlStreamReader &reader);
		static void readXmlSensorsConfigValue(QList<Config *> &config,
			QXmlStreamReader &reader);
		static void readXmlSensorsConfigFunction(QList<Config *> &config,
			QXmlStreamReader &reader, bool base_class);
		static void readXmlSensorsConfigNumber(QList<Config *> &config,
			QXmlStreamReader &reader, bool base_class);
		static void readXmlSensorsConfigDropdown(QList<Config *> &config,
			QXmlStreamReader &reader, bool base_class);
		static void readXmlSensorsConfigCheckbox(QList<Config *> &config,
			QXmlStreamReader &reader, bool base_class);

		static void readXmlSensorsSpi(QXmlStreamReader &reader);

		static void readXmlSensorsI2cAddress(I2c &sensor,
			QXmlStreamReader &reader);
		static void readXmlSensorsI2c(QXmlStreamReader &reader);

		static void readXmlSensorsAnalog(QXmlStreamReader &reader);
		static void readXmlSensorsAnalogParameter(Ain::Parameter &parameter,
			QXmlStreamReader &reader);

		static void consumeTag(QXmlStreamReader &reader);

		static void readXml(QString file_path);
		static void readXmlSensors(QXmlStreamReader &reader);
};


#endif
