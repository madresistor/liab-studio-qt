/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "threaded_timer.h"
#include <QTimer>

/**
 * Threaded sampler is the class that provide a mixed functionality of QThread
 *  and QTimer.
 *
 * QTimer generate timeout out on regular interval and the intended object
 *  can directly connect via (Qt::DirectConnection) to slot to get QTimer
 *  without having to manage the QTimer.
 *
 * This class is used by Box0 spi and i2c source for collecting data.
 *
 * QTimer is run in non-single-shot mode.
 */

LsBox0ThreadedTimer::LsBox0ThreadedTimer(QObject *parent) :
	QThread(parent)
{
}

void LsBox0ThreadedTimer::start(int interval)
{
	m_interval = interval;
	QThread::start();
}

void LsBox0ThreadedTimer::run()
{
	QTimer timer;
	timer.setSingleShot(false);
	timer.start(m_interval);

	/* direct connect so that timeout code is called from the thread. */
	connect(&timer, SIGNAL(timeout()), SIGNAL(timeout()), Qt::DirectConnection);

	timer.start();

	exec();

	timer.stop();
}
