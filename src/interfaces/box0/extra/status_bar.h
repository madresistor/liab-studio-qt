/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_DATA_LOGGER_BOX0_STATUS_BAR_H
#define LIAB_STUDIO_DATA_LOGGER_BOX0_STATUS_BAR_H

#include <QObject>
#include <QWidget>
#include <QStatusBar>
#include <QProgressBar>
#include <QLabel>
#include <QTimer>
#include <libbox0/libbox0.h>

class LsBox0StatusBar : public QStatusBar
{
	Q_OBJECT

	public:
		LsBox0StatusBar(QWidget *parent = Q_NULLPTR);
		void start(int time);
		void stop();
		void showDevice(b0_device *dev);

	private Q_SLOTS:
		void update();

	private:
		QLabel *label;
		QProgressBar *progressBar;
		QTimer *timer;

		int time;
		qint64 start_time;
};

#endif
