/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "status_bar.h"
#include <QDebug>
#include <QDateTime>
#include "ease.h"

#define INTERVAL_MS 50

LsBox0StatusBar::LsBox0StatusBar(QWidget *parent) :
	QStatusBar(parent)
{
	label = new QLabel(this);
	label->hide();
	progressBar = new QProgressBar(this);
	progressBar->hide();

	addPermanentWidget(label, 1);
	addPermanentWidget(progressBar, 3);

	timer = new QTimer(this);
	timer->setInterval(INTERVAL_MS);
	timer->setSingleShot(false);
	connect(timer, SIGNAL(timeout()), this, SLOT(update()));
}

void LsBox0StatusBar::start(int time)
{
	this->time = time;
	start_time = QDateTime::currentMSecsSinceEpoch();

	if (time > 0) {
		progressBar->setRange(0, time * 1000);
		progressBar->show();
	}

	label->show();

	update();

	timer->start();
}

void LsBox0StatusBar::stop()
{
	timer->stop();

	label->hide();
	progressBar->hide();

	showMessage("Stopped!", 1000);
}

void LsBox0StatusBar::update()
{
	qint64 diff_ms = QDateTime::currentMSecsSinceEpoch() - start_time;

	if (time > 0) {
		progressBar->setValue(diff_ms);
	}

	QString txt = QString("%1 Seconds").arg(diff_ms / 1000.0, 0, 'f', 1);
	label->setText(txt);
}

void LsBox0StatusBar::showDevice(b0_device *dev)
{
	QString message =
		QString("Connected to: %1 (Serial: %2) made by %3")
		.arg(utf8_to_qstring(dev->name))
		.arg(utf8_to_qstring(dev->serial))
		.arg(utf8_to_qstring(dev->manuf));
	showMessage(message, 5000);
}
