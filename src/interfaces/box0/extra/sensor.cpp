/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sensor.h"
#include <QXmlStreamReader>
#include <QFile>
#include <QDebug>
#include <cmath>
#include <QResource>
#include <QStandardPaths>
#include "extra/parser/parser.h"

/* =========================== [Config] ================================ */
bool LsBox0Sensor::ConfigNumber::valueValid(const QVariant &v) const
{
	double _v = v.toDouble();

	if (readonly) {
		return _v == value;
	}

	/* value is out of range? */
	return v >= min && v <= max;
}

bool LsBox0Sensor::ConfigCheckbox::valueValid(const QVariant &v) const
{
	QString _v = v.toString();

	if (readonly) {
		return _v == value;
	}

	/* the value is should be equal to checked or unchecked */
	return _v == checked || _v == unchecked;
}

bool LsBox0Sensor::ConfigDropdown::valueValid(const QVariant &v) const
{
	QString _v = v.toString();

	if (readonly) {
		return _v == value;
	}

	Q_FOREACH(const Entry &e, entries) {
		if (e.value == _v) {
			return true;
		}
	}

	/* the value is or-not in any of the possible values */
	return false;
}

bool LsBox0Sensor::ConfigFunction::valueValid(const QVariant &v) const
{
	QString _v = v.toString();

	if (readonly) {
		return _v == value;
	}

	try {
		LsParser parser;
		parser.setExpression(_v);
		return true;
	} catch (mu::Parser::exception_type &e) {
		return false;
	}
}

/* =========================== [General] ================================ */

#define DEFINE_SENSOR_INDEX_FUNC(TYPE)										\
int LsBox0Sensor::sensor_index(const QString &id, const QList<TYPE> &list)	\
{																			\
	for (int i = 0; i < list.count(); i++) {								\
		if (list.at(i).id == id) {											\
			return i;														\
		}																	\
	}																		\
																			\
	return -1;																\
}

DEFINE_SENSOR_INDEX_FUNC(Ain)
DEFINE_SENSOR_INDEX_FUNC(I2c)
DEFINE_SENSOR_INDEX_FUNC(Spi)

#define DEFINE_SENSOR_DRIVER_FUNC(TYPE)									\
QString LsBox0Sensor::sensor_driver(const QString &id,					\
						const QList<TYPE> &list)						\
{																		\
	QString next_id = id;												\
	Q_FOREVER {															\
		int sensor_index = LsBox0Sensor::sensor_index(next_id, list);	\
		if (sensor_index == -1) {										\
			return QString::null;										\
		}																\
																		\
		/* no parent */													\
		const LsBox0Sensor::TYPE &sensor = list.at(sensor_index);		\
		if (sensor.parent.isEmpty()) {									\
			return next_id;											\
		}																\
																		\
		next_id = sensor.parent;										\
	}																	\
}

//DEFINE_SENSOR_DRIVER_FUNC(Ain)
DEFINE_SENSOR_DRIVER_FUNC(I2c)
DEFINE_SENSOR_DRIVER_FUNC(Spi)

/* =========================== [AIN] ================================ */
QList<LsBox0Sensor::Ain> LsBox0Sensor::ain = QList<LsBox0Sensor::Ain>();

/**
 * For the Analog <analog> tag
 * <analog id="SENSOR_ID">
 *    <cat>CATEGORY_ID</cat>
 *    <title lang="en">SENSOR_TITLE</title>
 *    <function>SENSOR_TRANSFER_FUNCTION</function>
 *    <output>...</output>
 *    <input>...</input>
 *    <description>...</description>
 * </analog>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 */
void LsBox0Sensor::readXmlSensorsAnalog(QXmlStreamReader &reader)
{
	LsBox0Sensor::Ain sensor;
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	sensor.id = attr.value("id").toString();

	if (sensor.id.isEmpty()) {
		qDebug() << "ID is empty";
		return;
	}

	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "cat" || n == "category") {
				sensor.cat.append(reader.readElementText());
			} else if (n == "title") {
				sensor.title = reader.readElementText();
			} else if (n == "output") {
				readXmlSensorsAnalogParameter(sensor.output, reader);
			} else if (n == "input") {
				readXmlSensorsAnalogParameter(sensor.input, reader);
			} else if (n == "desc" || n == "description") {
				sensor.desc = reader.readElementText();
			} else if (n == "func" || n == "function") {
				sensor.func = reader.readElementText();
			} else {
				qDebug() << "readXmlSensorsAnalog| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}

	LsBox0Sensor::ain.append(sensor);
}

/**
 * Read the parameter <input> or <output> tag from <analog> tag
 * <....>
 *    <maximum>SENSOR_INPUT_MAX</maximum>
 *    <minimum>SENSOR_INPUT_MIN</minimum>
 *    <unit>SENSOR_INPUT_UNIT</unit>
 * </....>
 *
 * @param parameter Parameter
 * @param reader XML stream reader
 */
void LsBox0Sensor::readXmlSensorsAnalogParameter(
	LsBox0Sensor::Ain::Parameter &parameter, QXmlStreamReader &reader)
{
	QString tag = reader.name().toString();

	/* extract info */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "max" || n == "maximum") {
				QString _val = reader.readElementText();
				bool ok;
				parameter.max = _val.toDouble(&ok);
				if (!ok) {
					qDebug() << "readOutput| invalid double value: " << _val;
					parameter.max = NAN;
				}
			} else if (n == "min" || n == "maximum") {
				QString _val = reader.readElementText();
				bool ok;
				parameter.min = _val.toDouble(&ok);
				if (!ok) {
					qDebug() << "readXmlSensorsAnalogParameter| invalid double value: " << _val;
					parameter.min = NAN;
				}
			} else if (n == "unit") {
				parameter.unit = reader.readElementText();
			} else {
				qDebug() << "readXmlSensorsAnalogParameter| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}
}

/* =========================== [I2C] ================================ */
QList<LsBox0Sensor::I2c> LsBox0Sensor::i2c = QList<LsBox0Sensor::I2c>();

/**
 * Read the I2c tag <i2c>
 * <i2c id="SENSOR_ID" parent="PARENT_SENSOR_ID">
 *     <cat>CATEGORY_ID</cat>
 *     <name lang="en">SENSOR_TITLE</name>
 *     <description lang="en">SENSOR_DESCRIPTION</description>
 *     <config>...</config>
 *     <address ...></address>
 * </i2c>
 *
 * @param reader XML stream reader
 */
void LsBox0Sensor::readXmlSensorsI2c(QXmlStreamReader &reader)
{
	LsBox0Sensor::I2c sensor;
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	sensor.id = attr.value("id").toString();

	/* test for mandatory */
	if (sensor.id.isEmpty()) {
		qDebug() << "ID is empty";
		return;
	}

	if (attr.hasAttribute("parent")) {
		sensor.parent = attr.value("parent").toString();
	}

	bool base_class = sensor.parent.isEmpty();

	if (!base_class) {
		int si = LsBox0Sensor::sensor_index(sensor.parent, LsBox0Sensor::i2c);
		const LsBox0Sensor::I2c &p = LsBox0Sensor::i2c.at(si);
		sensor.addresses = p.addresses;
		configCopy(sensor.config, p.config);
	}

	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "cat" || n == "category") {
				sensor.cat.append(reader.readElementText());
			} else if (n == "title") {
				sensor.title = reader.readElementText();
			} else if (n == "config" || n == "configuration") {
				readXmlSensorsConfig(sensor.config, reader, base_class);
			} else if (n == "address" || n == "addr") {
				if (base_class) {
					readXmlSensorsI2cAddress(sensor, reader);
				} else {
					qDebug() << "only base class can add address fields";
					consumeTag(reader);
				}
			} else if (n == "desc" || n == "description") {
				sensor.desc = reader.readElementText();
			} else {
				qDebug() << "readXmlSensorsI2c| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}

	if (sensor.addresses.isEmpty()) {
		qDebug() << "atleast even one Address not provided";
		return;
	}

	i2c.append(sensor);
}

/**
 * Test if the address @a addr has been already used
 * @param addr Address
 * @param list Address list
 * @return true on used
 * @return false on un-used
 */
static bool address_used(uint addr, const QList<LsBox0Sensor::I2c::Address> &list)
{
	Q_FOREACH(const LsBox0Sensor::I2c::Address &i, list) {
		if (i.value == addr) {
			return true;
		}
	}

	return false;
}

/**
 * Read the <addr> (or <address> tag) and store it.
 * <address value="ADDRESS_VALUE">
 *     <equal id="CONFIG_ID" value="CONFIG_VALUE"></equal>
 * </address>
 *
 * @param sensor I2C Sensor
 * @param reader XML stream reader
 */
void LsBox0Sensor::readXmlSensorsI2cAddress(LsBox0Sensor::I2c &sensor,
		QXmlStreamReader &reader)
{
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("value")) {
		qDebug() << "value missing";
		return;
	}

	bool ok;
	uint addr_value = (uint) attr.value("value").toUInt(&ok, 0);
	if (!ok) {
		qDebug() << "value error";
		return;
	}

	bool base_class = sensor.parent.isEmpty();

	if (!base_class && !address_used(addr_value, sensor.addresses)) {
		qDebug() << "sub-class cannot add a new address";
		return;
	}

	QString title;
	QList<LsBox0Sensor::I2c::Address::Equal> equal;

	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "title") {
				title = reader.readElementText();
			} else if (n == "equal") {
				if (!base_class) {
					qDebug() << "sub-class cannot provide equal tag";
					continue;
				}

				QXmlStreamAttributes attr = reader.attributes();
				if (!attr.hasAttribute("id")) {
					qDebug() << "id missing";
				} else if (!attr.hasAttribute("value")) {
					qDebug() << "value missing";
				} else {
					LsBox0Sensor::I2c::Address::Equal entry;
					entry.id = attr.value("id").toString();
					entry.value = attr.value("value").toString();
					equal.append(entry);
				}
			} else {
				qDebug() << "readXmlSensorsI2cAddress| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}

	if (base_class) {
		if (title.isEmpty()) {
			if (equal.isEmpty()) {
				/* only one address */
				title = "Hard wired";
			} else {
				QStringList result;
				Q_FOREACH(const LsBox0Sensor::I2c::Address::Equal &i, equal) {
					result.append(QString("%1 = %2").arg(i.id).arg(i.value));
				}

				title = result.join(", ");
			}
		}

		LsBox0Sensor::I2c::Address entry;
		entry.value = addr_value;
		entry.title = title;
		entry.equal = equal;
		sensor.addresses.append(entry);
	} else {
		if (!title.isEmpty()) {
			/* we can update title only */
			for (int i = 0; i < sensor.addresses.count(); i++) {
				LsBox0Sensor::I2c::Address &addr = sensor.addresses[i];
				if (addr.value == addr_value) {
					addr.title = title;
					break;
				}
			}
		}
	}
}

/* =========================== [SPI] ================================ */
QList<LsBox0Sensor::Spi> LsBox0Sensor::spi = QList<LsBox0Sensor::Spi>();

/**
 * Read the SPI tag <spi>
 * <spi id="SENSOR_ID" parent="PARENT_SENSOR_ID">
 *     <cat>CATEGORY_ID</cat>
 *     <name lang="en">SENSOR_TITLE</name>
 *     <description lang="en">SENSOR_DESCRIPTION</description>
 *     <config>...</config>
 * </spi>
 *
 * @param reader XML stream reader
 */
void LsBox0Sensor::readXmlSensorsSpi(QXmlStreamReader &reader)
{
	LsBox0Sensor::Spi sensor;
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	sensor.id = attr.value("id").toString();

	/* test for mandatory */
	if (sensor.id.isEmpty()) {
		qDebug() << "ID is empty";
		return;
	}

	if (attr.hasAttribute("parent")) {
		sensor.parent = attr.value("parent").toString();
	}

	bool base_class = sensor.parent.isEmpty();

	if (!base_class) {
		int si = LsBox0Sensor::sensor_index(sensor.parent, LsBox0Sensor::spi);
		const LsBox0Sensor::Spi &p = LsBox0Sensor::spi.at(si);
		configCopy(sensor.config, p.config);
	}

	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "cat" || n == "category") {
				sensor.cat.append(reader.readElementText());
			} else if (n == "title") {
				sensor.title = reader.readElementText();
			} else if (n == "config" || n == "configuration") {
				readXmlSensorsConfig(sensor.config, reader, base_class);
			} else if (n == "desc" || n == "description") {
				sensor.desc = reader.readElementText();
			} else {
				qDebug() << "readXmlSensorsSpi| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}

	LsBox0Sensor::spi.append(sensor);
}

/* ========================== [Common] ============================== */

/**
 * Create a clone of the config array
 * @param to To
 * @param from From
 */
void LsBox0Sensor::configCopy(QList<Config *> &to, const QList<Config *> &from)
{
	to.clear();

	Q_FOREACH(const Config *f, from) {
		to.append(f->clone());
	}
}

/**
 * Read the <config> tag
 * <config>
 *  <checkbox ...>...</checkbox>
 *  <dropdown ...>...</dropdown>
 *  <number ...>...</number>
 *  <function ...>...</function>
 *  <value ...>...</value>
 *  <readonly ...></readonly>
 *  <hidden ...></hidden>
 * </config>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 * @param base_class @a config belongs to base class
 */
void LsBox0Sensor::readXmlSensorsConfig(QList<Config *> &config,
		QXmlStreamReader &reader, bool base_class)
{
	QString tag = reader.name().toString();

	/* extract info */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "checkbox") {
				readXmlSensorsConfigCheckbox(config, reader, base_class);
			} else if (n == "dropdown") {
				readXmlSensorsConfigDropdown(config, reader, base_class);
			} else if (n == "number") {
				readXmlSensorsConfigNumber(config, reader, base_class);
			} else if (n == "function" || n == "func") {
				readXmlSensorsConfigFunction(config, reader, base_class);
			} else if (n == "value") {
				readXmlSensorsConfigValue(config, reader);
			} else if (n == "readonly") {
				readXmlSensorsConfigReadonly(config, reader);
			} else if (n == "hidden") {
				readXmlSensorsConfigHidden(config, reader);
			} else {
				qDebug() << "readXmlSensorsConfig| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}
}

/**
 * Get the configuration from @a list which has ID @a id
 * @param id ID
 * @param list Configuration list
 * @return pointer to configuration on success
 * @return Q_NULLPTR on not found
 */
static LsBox0Sensor::Config *get_config_by_id(const QString &id,
			const QList<LsBox0Sensor::Config *> &list)
{
	Q_FOREACH(LsBox0Sensor::Config *i, list) {
		if (i->id == id) {
			return i;
		}
	}

	return Q_NULLPTR;
}

/**
 * Test if a configuration with @a id exists in @a config
 * @param id ID
 * @param config Config List
 * @return true if exists
 * @return false if do not exists
 */
static bool config_exists(const QString &id, const QList<LsBox0Sensor::Config *> &config)
{
	return get_config_by_id(id, config) != Q_NULLPTR;
}

/**
 * For the I2C/SPI Config, read the <checkbox> tag inside <config> tag
 * <checkbox id="CONFIG_ID" checked="CHECKED_VALUE" unchecked="UNCHECKED_VALUE">
 *     <title lang="en">CHECKBOX_TITLE</title>
 *     <desc lang="en">DESCRIPTION_VALUE</desc>
 * </checkbox>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 * @param base_class @a config belongs to base class
 */
void LsBox0Sensor::readXmlSensorsConfigCheckbox(QList<Config *> &config,
	QXmlStreamReader &reader, bool base_class)
{
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	QString id = attr.value("id").toString();
	bool already_exists = config_exists(id, config);

	if (!already_exists && !base_class) {
		qDebug() << "sub-class cannot create a new config";
		return;
	}

	if (!already_exists) {
		LsBox0Sensor::ConfigCheckbox *c = new LsBox0Sensor::ConfigCheckbox();
		c->id = c->title = id;
		config.append(c);
	}

	LsBox0Sensor::Config *_c = get_config_by_id(id, config);
	if (_c->type != LsBox0Sensor::Config::CHECKBOX) {
		qDebug() << "type did not match the expected item (CHECKBOX)";
		return;
	}

	LsBox0Sensor::ConfigCheckbox *c =
		dynamic_cast<LsBox0Sensor::ConfigCheckbox *>(_c);

	if (base_class) {
		if (attr.hasAttribute("checked")) {
			c->checked = attr.value("checked").toString();
		}

		if (attr.hasAttribute("unchecked")) {
			c->unchecked = attr.value("unchecked").toString();
			c->value = c->unchecked;
		}
	} else {
		if (attr.hasAttribute("checked")) {
			qDebug() << "ignoring checked attribute";
		}

		if (attr.hasAttribute("unchecked")) {
			qDebug() << "ignoring unchecked attribute";
		}
	}

	/* extract info */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "title") {
				c->title = reader.readElementText();
			} else if (n == "desc") {
				c->desc = reader.readElementText();
			} else {
				qDebug() << "readXmlSensorsConfigCheckbox| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}
}

/**
 * For the I2C/SPI Config, read the <dropdown> tag inside <config> tag
 * <dropdown id="CONFIG_ID">
 *    <entry lang="en" value="ENTRY_VALUE">ENTRY_TITLE</entry>
 *    <desc lang="en">DESCRIPTION_VALUE</desc>
 * </dropdown>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 * @param base_class @a config belongs to base class
 */
void LsBox0Sensor::readXmlSensorsConfigDropdown(QList<Config *> &config,
	QXmlStreamReader &reader, bool base_class)
{
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	QString id = attr.value("id").toString();
	bool already_exists = config_exists(id, config);

	if (!already_exists && !base_class) {
		qDebug() << "sub-class cannot create a new config";
		return;
	}

	if (!already_exists) {
		LsBox0Sensor::ConfigDropdown *c = new LsBox0Sensor::ConfigDropdown();
		c->id = c->title = id;
		config.append(c);
	}

	LsBox0Sensor::Config *_c = get_config_by_id(id, config);
	if (_c->type != LsBox0Sensor::Config::DROPDOWN) {
		qDebug() << "type did not match the expected item (DROPDOWN)";
		return;
	}

	LsBox0Sensor::ConfigDropdown *c =
		dynamic_cast<LsBox0Sensor::ConfigDropdown *>(_c);

	/* extract info */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "title") {
				c->title = reader.readElementText();
			} else if (n == "desc") {
				c->desc = reader.readElementText();
			} else if (n == "entry") {
				QXmlStreamAttributes attr = reader.attributes();
				if (attr.hasAttribute("value")) {
					LsBox0Sensor::ConfigDropdown::Entry entry;
					entry.title = reader.readElementText();
					entry.value = attr.value("value").toString();
					if (entry.title.isEmpty()) {
						entry.title = entry.value;
					}
					c->entries.append(entry);
				} else {
					qDebug() << "value missing for entry";
					consumeTag(reader);
				}
			} else {
				qDebug() << "readXmlSensorsConfigDropdown| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}

	/* select the first entry automatically (if nothing selected) */
	if (c->value.isEmpty() && !c->entries.isEmpty()) {
		c->value = c->entries.at(0).value;
	}

	if (c->entries.isEmpty()) {
		qDebug() << "no entries provided for ID" << id;
	}
}

static void convert_and_store(const QStringRef &value, double &store)
{
	bool ok;
	double v = value.toDouble(&ok);
	if (ok) {
		store = v;
	}
}

/**
 * For the I2C/SPI Config, read the <number> tag inside <config> tag
 * <number id="CONFIG_ID" maximum="MAXIMUM_VALUE" minimum="MINIMUM_VALUE">
 *    <title lang="en">NUMBER_TITLE</title>
 *    <desc lang="en">NUMBER_DESCRIPTION</desc>
 * </number>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 * @param base_class @a config belongs to base class
 */
void LsBox0Sensor::readXmlSensorsConfigNumber(QList<Config *> &config,
	QXmlStreamReader &reader, bool base_class)
{
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	QString id = attr.value("id").toString();
	bool already_exists = config_exists(id, config);

	if (!already_exists && !base_class) {
		qDebug() << "sub-class cannot create a new config";
		return;
	}

	if (!already_exists) {
		LsBox0Sensor::ConfigNumber *c = new LsBox0Sensor::ConfigNumber();

		if (attr.hasAttribute("max")) {
			convert_and_store(attr.value("max"), c->max);
		} else if (attr.hasAttribute("maximum")) {
			convert_and_store(attr.value("maximum"), c->max);
		}

		if (attr.hasAttribute("min")) {
			convert_and_store(attr.value("min"), c->min);
		} else if (attr.hasAttribute("minimum")) {
			convert_and_store(attr.value("minimum"), c->min);
		}

		c->id = c->title = id;
		config.append(c);
	}

	LsBox0Sensor::Config *c = get_config_by_id(id, config);

	/* extract info */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "title") {
				c->title = reader.readElementText();
			} else if (n == "desc") {
				c->desc = reader.readElementText();
			} else {
				qDebug() << "readXmlSensorsConfigNumber| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}
}

/**
 * For the I2C/SPI Config, read the <function> tag inside <config> tag
 * <function id="CONFIG_ID">
 *    <title lang="en">FUNCTION_TITLE</title>
 *    <desc lang="en">FUNCTION_DESCRIPTION</desc>
 * </function>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 * @param base_class @a config belongs to base class
 */
void LsBox0Sensor::readXmlSensorsConfigFunction(QList<Config *> &config,
	QXmlStreamReader &reader, bool base_class)
{
	QString tag = reader.name().toString();

	QXmlStreamAttributes attr = reader.attributes();
	if (!attr.hasAttribute("id")) {
		qDebug() << "id missing";
		return;
	}

	QString id = attr.value("id").toString();
	bool already_exists = config_exists(id, config);

	if (!already_exists && !base_class) {
		qDebug() << "sub-class cannot create a new config";
		return;
	}

	if (!already_exists) {
		LsBox0Sensor::Config *c = new LsBox0Sensor::ConfigFunction();
		c->id = c->title = id;
		config.append(c);
	}

	LsBox0Sensor::Config *_c = get_config_by_id(id, config);
	if (_c->type != LsBox0Sensor::Config::FUNCTION) {
		qDebug() << "type did not match the expected item (FUNCTION)";
		return;
	}

	LsBox0Sensor::ConfigFunction *c =
		dynamic_cast<LsBox0Sensor::ConfigFunction *>(_c);

	/* extract info */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "title") {
				c->title = reader.readElementText();
			} else if (n == "desc") {
				c->desc = reader.readElementText();
			} else if (n == "input") {
				if (base_class) {
					QXmlStreamAttributes attr = reader.attributes();
					if (attr.hasAttribute("id")) {
						LsBox0Sensor::ConfigFunction::Input input;
						input.id = attr.value("id").toString();
						input.title = reader.readElementText();
						if (input.title.isEmpty()) {
							input.title = input.id;
						}
						c->inputs.append(input);
					} else {
						qDebug() << "id missing for input";
						consumeTag(reader);
					}
				} else {
					qDebug() << "only base class can provide input's";
					consumeTag(reader);
				}
			} else {
				qDebug() << "readXmlSensorsConfigFunction| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}
}

/**
 * Test if any entry with @a value exists in @a entries
 * @param value Entry value
 * @param entries Entries
 * @return true on found
 * @return false on not found
 */
static bool entry_exists(const QString &value,
		QList<LsBox0Sensor::ConfigDropdown::Entry> &entries)
{
	Q_FOREACH(const LsBox0Sensor::ConfigDropdown::Entry &i, entries) {
		if (i.value == value) {
			return true;
		}
	}

	return false;
}

/**
 * For the I2C/SPI Config, read the <value> tag inside <config> tag
 * <value id="ID">VALUE</value>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 */
void LsBox0Sensor::readXmlSensorsConfigValue(QList<Config *> &config,
	QXmlStreamReader &reader)
{
	QString tag = reader.name().toString();

	/* read the attributes */
	QXmlStreamAttributes attr = reader.attributes();

	if (!attr.hasAttribute("id")) {
		qDebug() << "attribute id missing";
		return;
	}

	QString id = attr.value("id").toString();

	if (!config_exists(id, config)) {
		qDebug() << "no config with ID=" << id << "found";
		return;
	}

	Config *c = get_config_by_id(id, config);
	if (c->readonly) {
		qDebug() << "config " << id << " is marked as readonly";
		return;
	}

	QString value = reader.readElementText();
	if (value.isEmpty()) {
		qDebug() << "value is missing";
		return;
	}

	switch (c->type) {
	case Config::NUMBER: {
		ConfigNumber *cn = dynamic_cast<ConfigNumber *>(c);
		bool ok;
		double v = value.toDouble(&ok);
		if (ok && v < cn->max && v > cn->min) {
			cn->value = v;
		}
	} break;
	case Config::CHECKBOX: {
		ConfigCheckbox *ccb = dynamic_cast<ConfigCheckbox *>(c);
		if (value == ccb->checked || value == ccb->unchecked) {
			ccb->value = value;
		}
	} break;
	case Config::FUNCTION: {
		ConfigFunction *cf = dynamic_cast<ConfigFunction *>(c);
		/* TODO: validate the equation */
		cf->value = value;
	} break;
	case Config::DROPDOWN: {
		ConfigDropdown *cdd = dynamic_cast<ConfigDropdown *>(c);
		if (entry_exists(value, cdd->entries)) {
			cdd->value = value;
		}
	} break;
	}

	if (!reader.isEndElement() || reader.name() != tag) {
		qDebug() << "problem detected in xml, end tag problem";
	}
}

/**
 * Read the I2C/SPI <readonly> tag inside the <config> tag
 * <readonly id="ID"></readonly>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 */
void LsBox0Sensor::readXmlSensorsConfigReadonly(QList<Config *> &config,
	QXmlStreamReader &reader)
{
	QString tag = reader.name().toString();

	/* read the attributes */
	QXmlStreamAttributes attr = reader.attributes();

	if (!attr.hasAttribute("id")) {
		qDebug() << "attribute id missing";
		return;
	}

	QString id = attr.value("id").toString();

	if (!config_exists(id, config)) {
		qDebug() << "no config with ID=" << id << "found";
		return;
	}

	Config *c = get_config_by_id(id, config);
	if (c->readonly) {
		qDebug() << "config " << id << " is alread marked as readonly";
		return;
	}

	c->readonly = true;

	if (!reader.isEndElement() || reader.name() != tag) {
		qDebug() << "problem detected in xml, end tag problem";
	}
}

/**
 * Read the I2C/SPI <hidden> tag inside the <config> tag
 * <hidden id="ID"></hidden>
 *
 * @param config List of configuration
 * @param reader XML Stream Reader
 */
void LsBox0Sensor::readXmlSensorsConfigHidden(QList<Config *> &config,
	QXmlStreamReader &reader)
{
	QString tag = reader.name().toString();

	/* read the attributes */
	QXmlStreamAttributes attr = reader.attributes();

	if (!attr.hasAttribute("id")) {
		qDebug() << "attribute id missing";
		return;
	}

	QString id = attr.value("id").toString();

	if (!config_exists(id, config)) {
		qDebug() << "no config with ID=" << id << "found";
		return;
	}

	Config *c = get_config_by_id(id, config);
	if (c->hidden) {
		qDebug() << "config " << id << " is alread marked as hidden";
		return;
	}

	c->hidden = true;

	if (!reader.isEndElement() || reader.name() != tag) {
		qDebug() << "problem detected in xml, end tag problem";
	}
}

/**
 * Parse XML file (via @a reader) as sensor
 * @param reader XML Stream reader
 */
void LsBox0Sensor::readXmlSensors(QXmlStreamReader &reader)
{
	QString tag = reader.name().toString();

	/* read xml */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "analog" || n == "analogue") {
				readXmlSensorsAnalog(reader);
			} else if (n == "i2c") {
				readXmlSensorsI2c(reader);
			} else if (n == "spi") {
				readXmlSensorsSpi(reader);
			} else {
				qDebug() << "readXmlSensor| unknown tag: " << n;
				consumeTag(reader);
			}
		} else if (reader.isEndElement()) {
			if (reader.name() == tag) {
				break;
			}
		}
	}
}

/**
 * Consume the tag till the end tag of the same type is not found
 * @param reader XML Stream reader
 */
void LsBox0Sensor::consumeTag(QXmlStreamReader &reader)
{
	QStringList tags;
	tags << reader.name().toString();

	/* read xml */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			tags << reader.name().toString();
		} else if (reader.isEndElement()) {
			if (reader.name() != tags.takeLast()) {
				qDebug() << "something is wrong with XML, tag mismatch";
			}

			/* we are out of tags */
			if (tags.isEmpty()) {
				break;
			}
		}
	}
}

/**
 * Parse the file at @a file_path
 * @param file_path File Path
 */
void LsBox0Sensor::readXml(QString file_path)
{
	/* get the file */
	QFile file(file_path);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		qDebug() << "unable to open " << file_path;
		return;
	}

	QXmlStreamReader reader(&file);

	/* read xml */
	while (!reader.atEnd()) {
		reader.readNext();

		if (reader.isStartElement()) {
			QStringRef n = reader.name();
			if (n == "sensors") {
				readXmlSensors(reader);
			} else {
				qDebug() << "readXml| unknown tag: " << n;
			}
		}
	}

	if (reader.hasError()) {
		qDebug() << "XML error: " << reader.errorString();
	}

	file.close();

	qDebug() << "Got " << LsBox0Sensor::ain.size() << " AIN sensors";
	qDebug() << "Got " << LsBox0Sensor::i2c.size() << " I2C sensors";
	qDebug() << "Got " << LsBox0Sensor::spi.size() << " SPI sensors";
}

/**
 * Called once to read the sensor list
 */
void LsBox0Sensor::update()
{
	readXml(QResource(":/xml/box0-sensors.xml").absoluteFilePath());

	/* user specific */
	QString data_loc = QStandardPaths::writableLocation(
		QStandardPaths::DataLocation);
	readXml(data_loc + "/box0-sensors.xml");
}
