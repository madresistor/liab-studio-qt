/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_BOX0_RESULT_CODE_H
#define LIAB_STUDIO_BOX0_RESULT_CODE_H

#include <QWidget>
#include <QObject>
#include <QMessageBox>
#include <libbox0/libbox0.h>

class LsBox0ResultCode : public QMessageBox
{
	Q_OBJECT

	public:
		LsBox0ResultCode(QWidget *parent = Q_NULLPTR);
		LsBox0ResultCode(b0_result_code r, QWidget *parent = Q_NULLPTR);

		void setResultCode(b0_result_code r);
		void setTitle(const QString & title);
		void setMessage(const QString & msg);
		static bool handleInternal(b0_result_code r, const QString & msg);
};

#endif
