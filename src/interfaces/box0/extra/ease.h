/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_BOX0_EASE_H
#define LIAB_STUDIO_BOX0_EASE_H

#include <QString>
#include <libbox0/libbox0.h>

/**
 * Extract unit from libbox0 ref type
 * @param ref Reference
 * @return SI Unit
 */
inline QString ref_type_to_unit(b0_ref_type type)
{
	if (type == B0_REF_VOLTAGE) return "V";
	else if (type == B0_REF_CURRENT) return "A";
	else return QString::null;
}

inline QString utf8_to_qstring(const uint8_t *str)
{
	return QString::fromUtf8((const char *) str);
}

#endif
