/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "box0.h"
#include "sources/ain/ain.h"
#include "extra/sensor.h"
#include "extra/result_code.h"
#include <QFileDialog>
#include <QDebug>
#include <QJsonDocument>

/*
 * exp_counter mantain a counter value so that when user perform the same experiment,
 * instead of performing same experiment name, append a number to destingush
 */

const static QString GROUP_ID = "box0";
const static QString WIN_GEO_ID = "window-geometry";
const static QString CLOSE_ON_STOP_ID = "close-on-stop";
const static QString MIN_WIN_ON_START = "minimize-window-on-start";
const static QString AUTO_ADD_GRAPH = "auto-add-graph";
const static QString MAX_GRAPH_ON_ADD = "maximize-graph-on-adding";
const static QString GRAPH_ASSIGN_MARKER = "graph-assign-marker";

LsBox0::LsBox0(LsBank *bank, QWidget *parent) :
	LsInterface(bank, parent)
{
	ui.setupUi(this);
	resetExpCounter();

	if (B0_ERR_RC(b0_usb_open_supported(&device))) {
		statusBar()->showMessage(tr("No device found!"));
		ui.actionStartStop->setEnabled(false);
		ui.startStop->setEnabled(false);
		return;
	}

	/* show device information */
	dynamic_cast<LsBox0StatusBar *>(statusBar())->showDevice(device);

	ui.ain->setDevice(device);
	ui.spi->setDevice(device);
	ui.i2c->setDevice(device);

	b0_device_log(device, B0_LOG_WARN);

	iconStart.addFile(":/hardware/start");
	iconStop.addFile(":/hardware/stop");

	/* reset the running ui */
	onStateChange();

	connect(ui.actionStartStop, SIGNAL(triggered()), this, SLOT(onStartStop()));
	connect(ui.startStop, SIGNAL(clicked()), this, SLOT(onStartStop()));

	timer.setSingleShot(true);
	connect(&timer, SIGNAL(timeout()), this, SLOT(onStop()));

	connect(ui.inpExpName, SIGNAL(textChanged(QString)),
		this, SLOT(resetExpCounter()));

	/* load settings */
	QSettings s;
	s.beginGroup(GROUP_ID);

	if (s.contains(WIN_GEO_ID)) {
		restoreGeometry(s.value(WIN_GEO_ID).toByteArray());
	}

	if (s.contains(CLOSE_ON_STOP_ID)) {
		bool checked = s.value(CLOSE_ON_STOP_ID).toBool();
		ui.actionCloseOnStop->setChecked(checked);
	}

	if (s.contains(MIN_WIN_ON_START)) {
		bool checked = s.value(MIN_WIN_ON_START).toBool();
		ui.actionMinimizeWinOnStart->setChecked(checked);
	}

	if (s.contains(AUTO_ADD_GRAPH)) {
		bool checked = s.value(AUTO_ADD_GRAPH).toBool();
		ui.actionAutoAddGraph->setChecked(checked);
	}

	if (s.contains(MAX_GRAPH_ON_ADD)) {
		bool checked = s.value(MAX_GRAPH_ON_ADD).toBool();
		ui.actionMaximizeGraphOnAdding->setChecked(checked);
	}

	if (s.contains(GRAPH_ASSIGN_MARKER)) {
		bool checked = s.value(GRAPH_ASSIGN_MARKER).toBool();
		ui.actionAssignMarker->setChecked(checked);
	}

	s.endGroup();

	connect(ui.actionExportConfig, SIGNAL(triggered()), SLOT(exportConfig()));
	connect(ui.actionImportConfig, SIGNAL(triggered()), SLOT(importConfig()));
}

LsBox0::~LsBox0()
{
	if (device != NULL) {
		/* TODO: tell all model to unload their module */
		b0_device_close(device);
	}
}

void LsBox0::onStartStop()
{
	if (running) {
		onStop();
	} else {
		onStart();
	}
}

void LsBox0::onStart()
{
	int duration = 0;
	if (ui.durationEnable->isChecked()) {
		duration = ui.durationValue->value();
		timer.setInterval((duration + 1) * 1000);
	}

	table = m_bank->addTable();
	LsGraph *graph = Q_NULLPTR;

	if (ui.actionAutoAddGraph->isChecked()) {
		graph = m_bank->addGraph();
	}

	QString expName = ui.inpExpName->text();
	if (!expName.isEmpty()) {
		if (exp_counter > 0) {
			expName += QString(" (%1)").arg(exp_counter);
		}

		table->setTitle(expName);
		graph->setTitle(expName);
	}

	bool assign_marker = ui.actionAssignMarker->isChecked();
	int i = 0;
	int r;

	r = ui.ain->onStart(table, graph, assign_marker, duration);
	if (r < 0) {
		QMessageBox::warning(this, "AIN failed", "Failed to start AIN");
	} else {
		if (r > 0) {
			i++;
		}

		r = ui.spi->onStart(table, graph, assign_marker, duration);
		if (r < 0) {
			QMessageBox::warning(this, "SPI failed", "Failed to start SPI");
		} else {
			if (r > 0) {
				i++;
			}

			r = ui.i2c->onStart(table, graph, assign_marker, duration);
			if (r < 0) {
				QMessageBox::warning(this, "I2C failed", "Failed to start I2C");
			} else {
				if (r > 0) {
					i++;
				}
			}
		}
	}

	if (r < 0 || i == 0) {
		m_bank->removeWidget(table);
		m_bank->removeWidget(graph);
		table = Q_NULLPTR;

		ui.ain->onStop();
		ui.spi->onStop();
		ui.i2c->onStop();

		return;
	}

	if (r >= 0 && i == 0) {
		QMessageBox::warning(this, "Nothing started",
			"Their is nothing to start");
		return;
	}

	ui.statusBar->start(duration);

	running = true;
	onStateChange();
	exp_counter++;

	if (ui.actionAutoAddGraph->isChecked() &&
		ui.actionMaximizeGraphOnAdding->isChecked()) {
		graph->fullInteraction();
	}

	if (duration > 0) {
		timer.start();
	}

	if (ui.actionMinimizeWinOnStart->isChecked()) {
		showMinimized();
	}
}

void LsBox0::onStop()
{
	timer.stop();

	ui.ain->onStop();
	ui.spi->onStop();
	ui.i2c->onStop();
	ui.statusBar->stop();

	running = false;
	onStateChange();

	table = Q_NULLPTR;

	if (ui.actionCloseOnStop->isChecked()) {
		close();
	}
}

/*
 * To be called when the state change (run to stop) or (stop to run)
 */
void LsBox0::onStateChange()
{
	QString text = running ? "Stop All" : "Start All";
	QIcon icon = running ? iconStop : iconStart;

	ui.durationValue->setEnabled(ui.durationEnable->isChecked() && !running);

	/* do not let user import configuration when running */
	ui.actionImportConfig->setEnabled(!running);

	ui.actionStartStop->setIcon(icon);
	ui.actionStartStop->setIconText(text);

	ui.startStop->setIcon(icon);
	ui.startStop->setText(text);
}

void LsBox0::closeEvent(QCloseEvent *event)
{
	if (running) {
		onStop();
	}

	if (device != NULL) {
		ui.ain->setDevice(NULL);
		ui.spi->setDevice(NULL);
		ui.i2c->setDevice(NULL);
		LsBox0ResultCode::handleInternal(b0_device_close(device),
				"unable to close device");
	}

	/* save settings */
	QSettings s;
	s.beginGroup(GROUP_ID);
	s.setValue(WIN_GEO_ID, saveGeometry());
	s.setValue(CLOSE_ON_STOP_ID, ui.actionCloseOnStop->isChecked());
	s.setValue(MIN_WIN_ON_START, ui.actionMinimizeWinOnStart->isChecked());
	s.setValue(AUTO_ADD_GRAPH, ui.actionAutoAddGraph->isChecked());
	s.setValue(MAX_GRAPH_ON_ADD, ui.actionMaximizeGraphOnAdding->isChecked());
	s.setValue(GRAPH_ASSIGN_MARKER, ui.actionAssignMarker->isChecked());
	s.endGroup();

	device = NULL;

	LsInterface::closeEvent(event);
}

void LsBox0::resetExpCounter()
{
	exp_counter = 0;
}

/**
 * Show a dialog to user to import configuration from.
 *  If user agree's, save the configuration to file in form of JSON
 */
void LsBox0::importConfig()
{
	QString path = configDir();
	QString filter = "JSON (*.json)";
	QString filePath = QFileDialog::getOpenFileName(this, tr("Open data"), path, filter);

	if (!filePath.isEmpty()) {
		importConfig(filePath);
	}
}

/**
 * Read configuration from file at @a filepath
 * @param filepath File path
 */
void LsBox0::importConfig(const QString &filepath)
{
	QFile file(filepath);

	if (!file.open(QIODevice::ReadOnly)) {
		qWarning() << "Couldn't open read file." << filepath;
		return;
	}

	QByteArray saveData = file.readAll();
	QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

	QJsonObject obj = loadDoc.object();
	setJsonConfig(obj);
}

/**
 * Read configuration from JSON object
 * @param obj Object to read from
 */
void LsBox0::setJsonConfig(const QJsonObject &obj)
{
	QJsonValue val;

	val = obj.value("exp");
	if (val.isObject()) {
		QJsonObject exp = val.toObject();

		val = exp.value("duration");
		if (val.isObject()) {
			QJsonObject dur = val.toObject();

			val = dur.value("value");
			if (val.isDouble()) {
				ui.durationValue->setValue(val.toInt());
			}

			val = dur.value("enable");
			if (val.isBool()) {
				ui.durationEnable->setChecked(val.toBool());
			}
		}

		val = exp.value("name");
		if (val.isString()) {
			ui.inpExpName->setText(val.toString());
		}
	}

	/* ain */
	val = obj.value("ain");
	if (val.isObject()) {
		ui.ain->setJsonConfig(val.toObject());
	}

	/* spi */
	val = obj.value("spi");
	if (val.isObject()) {
		ui.spi->setJsonConfig(val.toObject());
	}

	/* i2c */
	val = obj.value("i2c");
	if (val.isObject()) {
		ui.i2c->setJsonConfig(val.toObject());
	}
}

/**
 * Show a dialog to user to store configuration.
 *  If user agrees and provide a path, store the configuration in form of JSON
 */
void LsBox0::exportConfig()
{
	QString path = QString("%1/%2.json").arg(configDir()).arg("box0-liab");
	QString filter = "JSON (*.json)";
	QString filepath = QFileDialog::getSaveFileName(this, tr("Configuration to file"), path, filter);

	/* do we need to save? */
	if (! filepath.isEmpty()) {
		/* Ref: http://stackoverflow.com/a/9822246/1500988 */
		if (QFileInfo(filepath).suffix().isEmpty() or QFileInfo(filepath).suffix() != "json") {
			filepath += ".json";
		}

		exportConfig(filepath);
	}
}

/**
 * Write configuration to file at @a filepath
 * @param filepath File path
 */
void LsBox0::exportConfig(const QString &filepath)
{
	QFile file(filepath);

	if (!file.open(QIODevice::WriteOnly)) {
		qWarning() << "Couldn't open save file." << filepath;
		return;
	}

	file.write(QJsonDocument(jsonConfig()).toJson());
}

/**
 * Return configuration stored inside JSON Object
 * @return json object
 */
QJsonObject LsBox0::jsonConfig()
{
	return {
		{"exp", QJsonObject({
			{"name", ui.inpExpName->text()},
			{"duration", QJsonObject({
				{"enable", ui.durationEnable->isChecked()},
				{"value", ui.durationValue->value()}
			})}
		})},
		{"ain", ui.ain->jsonConfig()},
		{"spi", ui.spi->jsonConfig()},
		{"i2c", ui.i2c->jsonConfig()}
	};
}

/* JSON config storage format:
 * {
 *  "exp": {
 *    "name": "NAME_OF_THE_EXPERIMENT" (string),
 *    "duration": {
 *      "value": DURATION_OF_THE_EXPERIMENT (double),
 *      "enable": False = Log infinite | True = use "value (bool)
 *     }
 *  },
 *  "ain": {... AIN specifi data ...},
 *  "spi": {... SPI specific data ...},
 *  "i2c": {... I2C specific data ...}
 * }
 */
