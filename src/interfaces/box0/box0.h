/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_INTERFACES_BOX0_H
#define LIAB_STUDIO_INTERFACES_BOX0_H

#include <QObject>
#include <QWidget>
#include "bank.h"
#include "interfaces/interface/interface.h"
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <QTimer>
#include <QSpinBox>

#include "ui_box0.h"

class LsBox0 : public LsInterface
{
	Q_OBJECT

	public:
		LsBox0(LsBank *bank, QWidget *parent = Q_NULLPTR);
		~LsBox0();

	private Q_SLOTS:
		void onStartStop();
		void onStop();
		void onStart();
		void closeEvent(QCloseEvent *event);
		void resetExpCounter();
		void importConfig();
		void exportConfig();

	private:
		void onStateChange();
		void exportConfig(const QString &filepath);
		QJsonObject jsonConfig();
		void importConfig(const QString &filepath);
		void setJsonConfig(const QJsonObject &obj);

		Ui::LsBox0 ui;
		b0_device *device = NULL;
		bool running = false;

		QIcon iconStart, iconStop;
		QTimer timer;
		int exp_counter;

		QPointer<LsTable> table;
};

#endif
