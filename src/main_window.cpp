/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main_window.h"
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QFileInfo>
#include <QPushButton>
#include <QWidgetAction>
#include "about.h"

/* widgets */
#include "widgets/table/table.h"
#include "widgets/thermometer/thermometer.h"
#include "widgets/graph/graph.h"
#include "widgets/guage/guage.h"
#include "widgets/text/text.h"

/* interfaces */
#include "interfaces/box0/box0.h"

LsMainWindow::LsMainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	ui.setupUi(this);

	bank = new LsBank(this);

	/* file */
	connect(ui.actionImport, SIGNAL(triggered()), this, SLOT(import()));
	connect(ui.actionExit, SIGNAL(triggered()), this, SLOT(close()));

	/* widgets */
	connect(ui.actionTable, SIGNAL(triggered()), bank, SLOT(addTable()));
	connect(ui.actionGraph, SIGNAL(triggered()), bank, SLOT(addGraph()));
	connect(ui.actionGuage, SIGNAL(triggered()), bank, SLOT(addGuage()));
	connect(ui.actionThermometer, SIGNAL(triggered()), bank, SLOT(addThermometer()));
	connect(ui.actionText, SIGNAL(triggered()), bank, SLOT(addText()));
	connect(ui.actionNote, SIGNAL(triggered()),
			bank, SLOT(addNote()));

	/* interfaces */
	connect(ui.actionBox0, SIGNAL(triggered()),
			this, SLOT(addInterface()));

	ui.mdiArea->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui.mdiArea,
		SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(showPopup(const QPoint &)));

	addStartupWidget();

	showMaximized();

	arrangeNicely();

	connect(ui.actionAboutLiaBStudio, SIGNAL(triggered()),
			this, SLOT(showAbout()));

	connect(ui.actionExit, SIGNAL(triggered()),
			qApp, SLOT(closeAllWindows()));

	/* TODO: before showing sub-window to user, make them nicely positioned */
}

/**
 * Nicely arrange all widgets.
 * @note This method is only intended/work for addStartupWidget() only.
 *   This just horizontally arrange them at the moment.
 */
void LsMainWindow::arrangeNicely()
{
	int x = 10, y = 10;
	Q_FOREACH (QMdiSubWindow *w, ui.mdiArea->subWindowList()) {
		w->move(QPoint(x, y));
		x += w->width() + 10;
	}
}

/**
 * Add some widget for user on startup
 */
void LsMainWindow::addStartupWidget()
{
	/* add a table */
	LsTable *table = bank->addTable();

	/* add some default column to table */
	table->appendColumn("X", "x");
	table->appendColumn("Y", "y");

	/* add a graph */
	bank->addGraph();

	/* TODO: add the default column to graph too. */
}

/**
 * Show an about dialog to user
 */
void LsMainWindow::showAbout()
{
	LsAbout about;
	about.exec();
}

/**
 * Show a context menu to user
 * @param pos Position
 */
void LsMainWindow::showPopup(const QPoint &pos)
{
	ui.menuAdd->popup(pos);
}

/**
 * a common function to add Interface's.
 * Based on sender pointer, it decided which interface to add
 */
void LsMainWindow::addInterface()
{
	QObject *sender = QObject::sender();
	LsInterface *intf;

	if (sender == ui.actionBox0) {
		intf = new LsBox0(bank);
	} else {
		/* ? */
		return;
	}

	connect(this, SIGNAL(aboutToClose()), intf, SLOT(close()));
	intf->show();
}

/**
 * Add a widget to MDI area
 * @param widget Widget to add
 * @param width Width of widget
 * @param height Height of widget
 */
void LsMainWindow::addWidgetToMdi(LsWidget *widget, int width, int height)
{
	QMdiSubWindow *mdiSubWindow = new QMdiSubWindow(ui.mdiArea);
	mdiSubWindow->resize(width, height);
	mdiSubWindow->setContextMenuPolicy(Qt::PreventContextMenu);
	mdiSubWindow->setWidget(widget);
	ui.mdiArea->addSubWindow(mdiSubWindow);
	mdiSubWindow->show();

	/* delete when closed */
	mdiSubWindow->setAttribute(Qt::WA_DeleteOnClose);
	widget->setAttribute(Qt::WA_DeleteOnClose);

	/* close the sub-window when the internal widget is closed */
	connect(widget, SIGNAL(destroyed()), mdiSubWindow, SLOT(close()));
}

/**
 * Import data from filesystem.
 * Show a fileselector dialog and open the file
 * @note Currently CSV file is only supported.
 */
void LsMainWindow::import()
{
	QString path = QStandardPaths::locate(QStandardPaths::DocumentsLocation,
		QString(), QStandardPaths::LocateDirectory);
	QStringList filter;
	filter << "Comma Separated Values (*.csv)";
	filter << "Tab Separated Values (*.tsv)";
	QString filePath = QFileDialog::getOpenFileName(this, tr("Open data"),
		path, filter.join(";;"));

	if (!filePath.isEmpty()) {
		bank->addTable(filePath);
		statusBar()->showMessage(tr("File loaded."), 2000);
	}
}

void LsMainWindow::closeEvent(QCloseEvent *event)
{
	/* emit a signal that is connected to widget "close()" so that
	 *  they can receive close event too. */
	Q_EMIT aboutToClose();

	QMainWindow::closeEvent(event);
}

/**
 * Remove a widget from MDI area
 * @param widget Widget to remove
 */
void LsMainWindow::removeWidgetFromMdi(LsWidget *widget)
{
	QList<QMdiSubWindow *> wins = ui.mdiArea->subWindowList();
	for (int i = 0; i < wins.size(); i++) {
		QMdiSubWindow *win = wins.at(i);
		if (win->widget() == widget) {
			win->deleteLater();
			return;
		}
	}

	qDebug() << "mdi sub window to delete not found!!!";
}
