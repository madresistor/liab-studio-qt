/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDir>
#include <QDebug>
#include <QRect>
#include <QIcon>
#include <QFile>
#include <QString>
#include "main_window.h"
#include "interfaces/box0/extra/sensor.h"

#if defined(USE_MULTI_PROC)
# include <omp.h>
# include <fftw3.h>
#endif

#if defined(__FAST_MATH__)
# error "do not compile with fast math, "
		"this cause problem in program, "
		"reason: isnan() will always return false"
#endif

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

#if defined(USE_MULTI_PROC)
	qDebug() << "compiled with OpenMP";

	fftwf_init_threads();
	int plan_threads = omp_get_max_threads();
	qDebug() << "Using" << plan_threads << "threads for FFTW plan execution";
	fftwf_plan_with_nthreads(plan_threads);
#endif

#if defined(Q_OS_WIN)
	/* fontconfig configuration file */
	qputenv("FONTCONFIG_PATH", a.applicationDirPath().toUtf8());
#endif

	QCoreApplication::setOrganizationName("Mad Resistor");
	QCoreApplication::setOrganizationDomain("madresistor.org");
	QCoreApplication::setApplicationName("LiaB Studio");

	/* style */
	QFile style_file(":/style/default");
	if (style_file.open(QFile::ReadOnly)) {
		QString style = QLatin1String(style_file.readAll());
		a.setStyleSheet(style);
	}

	/* app icon */
	QIcon appIcon(":/app-icon");
	QApplication::setWindowIcon(appIcon);

	/* fallback to Adwaita theme */
	if (QIcon::themeName().isEmpty()) {
		QStringList paths = QIcon::themeSearchPaths();
		paths << a.applicationDirPath() + QDir::separator() + "adwaita-icon-theme";
		QIcon::setThemeSearchPaths(paths);
		qDebug() << "Fallback to Adwaita icon theme";
		QIcon::setThemeName("Adwaita");
	}

	LsMainWindow w;
	w.show();

	/* read from sensors list */
	LsBox0Sensor::update();

	return a.exec();
}
