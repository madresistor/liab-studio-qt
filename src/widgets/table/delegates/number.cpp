/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "number.h"
#include <QLineEdit>
#include <QPainter>
#include <cmath>
#include <QRegExp>
#include <cstdio>

using namespace std;

LsTableDelegateNumber::LsTableDelegateNumber(int ufp, QWidget *parent) :
	QItemDelegate(parent),
	userSigDigits(ufp)
{
}

QWidget *LsTableDelegateNumber::createEditor(QWidget *parent, const
	QStyleOptionViewItem &/* option */, const QModelIndex & /* index */) const
{
	return new QLineEdit(parent);
}

void LsTableDelegateNumber::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	QLineEdit *lineEdit = static_cast<QLineEdit *>(editor);
	lineEdit->setText(stringData(index));
}

void LsTableDelegateNumber::setModelData(QWidget *editor,
	QAbstractItemModel *model, const QModelIndex &index) const
{
	QLineEdit *lineEdit = static_cast<QLineEdit *>(editor);

	bool ok;
	double value = lineEdit->text().toDouble(&ok);
	if (! ok) {
		value = NAN;
	}

	model->setData(index, value, Qt::EditRole);
}

void LsTableDelegateNumber::updateEditorGeometry(QWidget *editor,
	const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	editor->setGeometry(option.rect);
}

void LsTableDelegateNumber::paint(QPainter * painter,
	const QStyleOptionViewItem & option, const QModelIndex &index) const
{
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(option.rect, option.palette.highlight());
	}

	painter->save();
	painter->setRenderHint(QPainter::Antialiasing, true);
	painter->setPen(Qt::NoPen);
	if (option.state & QStyle::State_Selected) {
		painter->setBrush(option.palette.highlightedText());
	}

	QStyleOptionViewItem myOption = option;
	myOption.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;

	drawDisplay(painter, myOption, myOption.rect, stringData(index));
	drawFocus(painter, myOption, myOption.rect);

	painter->restore();
}

QString LsTableDelegateNumber::stringData(const QModelIndex &index) const
{
	bool ok;
	float value = index.model()->data(index, Qt::DisplayRole).toFloat(&ok);

	if ((! ok) || isnan(value)) {
		return "";
	}

	char buf[20];
	int sig_digits = index.model()->data(index, userSigDigits).toInt();
	if (sig_digits < 0) {
		snprintf(buf, sizeof(buf), "%.7g", value);
	} else {
		snprintf(buf, sizeof(buf), "%#.*g", sig_digits, value);
	}

	return QString::fromUtf8(buf);
}
