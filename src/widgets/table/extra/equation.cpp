/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "equation.h"
#include <QMenu>
#include <QSignalMapper>
#include "extra/parser/parser.h"
#include "extra/parser/constant.h"
#include "extra/parser/function.h"
#include "extra/parser/operator.h"

#include "widgets/table/table.h"
#include "widgets/table/table_data.h"

LsTableColumnEquation::LsTableColumnEquation(LsBank *bank, QWidget *parent) :
	QWidget(parent),
	m_bank(bank)
{
	ui.setupUi(this);

	LsParserFunction *func = new LsParserFunction(ui.insertFunction);
	ui.insertFunction->setMenu(func);
	connect(func, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsFunction(const QString &)));

	LsParserOperator *oper = new LsParserOperator(ui.insertOperator);
	ui.insertOperator->setMenu(oper);
	connect(oper, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsOperator(const QString &)));

	LsParserConstant *constant = new LsParserConstant(ui.insertConstant);
	ui.insertConstant->setMenu(constant);
	connect(constant, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsConstant(const QString &)));

	setupVariable();
}

void LsTableColumnEquation::setupVariable()
{
	QMenu *variableMenu = new QMenu(this);

	/* Iteration variables */
	{
	LsParserMenu *menu = new LsParserMenu(variableMenu);
	connect(menu, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsVariable(const QString &)));

	menu->addEntry("row", "Index, varies from 1 to ROWS");
	menu->addEntry("ROWS", "Number of Iterations");
	m_parser.defineConstant("row", 0);
	m_parser.defineConstant("ROWS", 0);

	QAction *action = variableMenu->addAction("Iteration");
	action->setMenu(menu);

	variableMenu->addSeparator();
	}

	const QList<QPointer<LsTable>> tables = m_bank->tables();

	/* iterate over all tables */
	for (int i = 0; i < tables.count(); i++) {
		LsTable *table = tables.at(i);

		const QList<LsTableDataColumn *> &columns = table->tableData->columns;

		/* worth further? */
		if (!columns.count()) {
			continue;
		}

		LsParserMenu *menu = new LsParserMenu(variableMenu);

		connect(menu, SIGNAL(userWantToInsert(const QString &)),
			ui.equation, SLOT(insertAsVariable(const QString &)));

		/* interate over all columns in the table */
		for (int j = 0; j < columns.count(); j++) {

			/* parser insert the variable */
			QString id = variableId(i, j);
			m_parser.defineConstant(id, 0);

			/* list the variable inside the table menu */
			LsTableDataColumn *column = columns.at(j);
			QString desc = column->short_name.isEmpty() ?
				column->long_name:
				QString ("%1 (%2)").arg(column->long_name).arg(column->short_name);

			menu->addEntry(id, desc);
		}

		/* add the table menu to variable menu */
		QAction *action = variableMenu->addAction(table->title());
		action->setMenu(menu);
	}

	ui.insertVariable->setMenu(variableMenu);
}

/**
 * Validate the equation and return the result
 * @return EMPTY if there is no equation
 * @return INVALID if equation is incorrect
 * @return VALID if equation is valid
 */
LsTableColumnEquation::EquationStatus LsTableColumnEquation::validateEquation()
{
	QString eq = ui.equation->text();

	if (!eq.length()) {
		return LsTableColumnEquation::EMPTY;
	}

	m_parser.setExpression(eq);

	try {
		m_parser.Eval();
	} catch (mu::Parser::exception_type &e) {
		QString msg = QString::fromStdString(e.GetMsg());
		ui.equationError->setText(msg);
		return LsTableColumnEquation::INVALID;
	}

	return LsTableColumnEquation::VALID;
}

QString LsTableColumnEquation::equation()
{
	return ui.equation->text();
}

/**
 * @return number of iterations required.
 * @return 0 means count is not provided
 */
int LsTableColumnEquation::count()
{
	if (ui.countEnable->isChecked()) {
		return ui.countValue->value();
	}

	return 0;
}
