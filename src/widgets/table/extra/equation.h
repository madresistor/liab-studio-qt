/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_TABLE_CHANNEL_EQUATION_H
#define LIAB_STUDIO_TABLE_CHANNEL_EQUATION_H

#include <QObject>
#include <QWidget>
#include "extra/parser/parser.h"
#include "bank.h"

#include "ui_table_column_equation.h"

class LsTableColumnEquation : public QWidget
{
	Q_OBJECT

	public:
		LsTableColumnEquation(LsBank *bank, QWidget *parent = Q_NULLPTR);

		enum EquationStatus {EMPTY, INVALID, VALID};

		EquationStatus validateEquation();
		QString equation();
		int count();

		static QString variableId(int table_index, int column_index) {
			return QString("t%1c%2").arg(table_index).arg(column_index);
		}

		static QRegExp variableIdRegx() {
			return QRegExp("t(\\d+)c(\\d+)");
		}

	private:
		Ui::LsTableColumnEquation ui;
		void setupVariable();
		bool equationValid();
		LsParser m_parser;
		LsBank *m_bank;

};

#endif
