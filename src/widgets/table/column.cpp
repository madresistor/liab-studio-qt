/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QVariant>
#include <QMenu>
#include "column.h"

#include <QColorDialog>
#include <QWidgetAction>
#include <QMenu>
#include <QLineEdit>

/*
 * TODO:
 * 	accept whole source list,
 *  - if user select a Unit, then show only source with compatible unit
 *  - if user havent input any list, show all source, and which ever source is selected,
 * 	    use the source unit as column unit
 */

LsTableColumn::LsTableColumn(LsBank *bank, QWidget * parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	fillButtonWithSymbol(ui.btnLongNameSymbol);
	fillButtonWithSymbol(ui.btnShortNameSymbol);
	fillButtonWithSymbol(ui.btnUnitSymbol);

	if (bank != Q_NULLPTR) {
		m_equation = new LsTableColumnEquation(bank, this);
		ui.tabWidget->addTab(m_equation, "Equation");
	}
}

/**
 * Done when user want to close the dialog.
 * If equation input is valid, then equation is validated.
 * if the equation is invalid, the dialog is not closed.
 * @param r Result
 */
void LsTableColumn::done(int r)
{
	if (r == QDialog::Accepted && m_equation != Q_NULLPTR) {
		/* confirm that the equation is valid or empty */
		if (m_equation->validateEquation() == LsTableColumnEquation::INVALID) {
			return;
		}
	}

	QDialog::done(r);
}

void LsTableColumn::fillButtonWithSymbol(QPushButton *button)
{
	QMenu *symbolMenu = new QMenu();
	addFromUnicodeArray(symbolMenu->addMenu("Greek"), unicode_greek);
	addFromUnicodeArray(symbolMenu->addMenu("Super Script"), unicode_super);
	addFromUnicodeArray(symbolMenu->addMenu("Sub Script"), unicode_sub);
	button->setMenu(symbolMenu);
	connect(symbolMenu, SIGNAL(triggered(QAction *)),
			this, SLOT(appendTextFromAction(QAction *)));
}

void LsTableColumn::appendTextFromAction(QAction *action)
{
	QObject *sender = QObject::sender();
	QLineEdit *lineEdit;
	if (sender == ui.btnLongNameSymbol->menu()) {
		lineEdit = ui.inpLongName;
	} else if (sender == ui.btnShortNameSymbol->menu()) {
		lineEdit = ui.inpShortName;
	} else if (sender == ui.btnUnitSymbol->menu()) {
		lineEdit = ui.inpUnit;
	} else {
		/* ?? */
		return;
	}

	QString text = lineEdit->text() + action->text();
	lineEdit->setText(text);
}

QString LsTableColumn::longName()
{
	return ui.inpLongName->text();
}

QString LsTableColumn::shortName()
{
	return ui.inpShortName->text();
}

QString LsTableColumn::unit()
{
	return ui.inpUnit->text();
}

void LsTableColumn::setLongName(QString &value)
{
	ui.inpLongName->setText(value);
}

void LsTableColumn::setShortName(QString &value)
{
	ui.inpShortName->setText(value);
}

void LsTableColumn::setUnit(QString &value)
{
	ui.inpUnit->setText(value);
}

void LsTableColumn::setColor(QColor &value)
{
	ui.btnColor->setColor(value);
}

QColor LsTableColumn::color()
{
	return ui.btnColor->color();
}

void LsTableColumn::addFromUnicodeArray(QMenu *menu, const uint16_t list[])
{
	for (size_t i = 0; list[i] > 0; i++) {
		menu->addAction(QString::fromUtf16(&list[i], 1));
	}
}

void LsTableColumn::setPreventEdit(bool prevent_edit)
{
	ui.cbPreventEdit->setChecked(prevent_edit);
}

bool LsTableColumn::preventEdit()
{
	return ui.cbPreventEdit->isChecked();
}

void LsTableColumn::setPrecision(int sig_digits)
{
	ui.cmbPrecision->setCurrentIndex(sig_digits + 1);
}

int LsTableColumn::sig_digits()
{
	return ui.cmbPrecision->currentIndex() - 1;
}

/* http://en.wikipedia.org/wiki/Greek_letters_used_in_mathematics,_science,_and_engineering */
const uint16_t LsTableColumn::unicode_greek[] = {
	0x03B1 /* α */,
	0x03DD /* ϝ */,
	0x03BA /* κ */,
	0x03C7 /* ϰ */,
	0x03BF /* ο */,
	0x03C5 /* υ */,
	0x03B2 /* β */,
	0x03B6 /* ζ */,
	//0x /* Λ */,
	0x03B8 /* λ */,
	0x03A0 /* Π */,
	0x03C0 /* π */,
	0x03D6 /* ϖ */,
	0x03A6 /* Φ */,
	//0x /* ϕ */,
	0x03C6 /* φ */,
	0x0393 /* Γ */,
	0x03B3 /* γ */,
	0x03B7 /* η */,
	0x03BC /* μ */,
	0x03C1 /* ρ */,
	0x03F1 /* ϱ */,
	0x03C7 /* χ */,
	0x0394 /* Δ */,
	0x03B4 /* δ */,
	0x0398 /* Θ */,
	0x03B8 /* θ */,
	0x03D1 /* ϑ */,
	0x03BD /* ν */,
	0x03A3 /* Σ */,
	0x03C3 /* σ */,
	0x03C2 /* ς */,
	0x03A8 /* Ψ */,
	0x03C8 /* ψ */,
	0x03F5 /* ϵ */,
	0x03B5 /* ε */,
	0x03B9 /* ι */,
	0x039E /* Ξ */,
	0x03BE /* ξ */,
	0x03C4 /* τ */,
	0x03A9 /* Ω */,
	0x03C9 /* ω */,
	0 /* end */
};

/* http://en.wikipedia.org/wiki/Unicode_subscripts_and_superscripts */
const uint16_t LsTableColumn::unicode_super[] = {
	0x2070, /* <super> 0 */
	0x00B9, /* <super> 1 */
	0x00B2, /* <super> 2 */
	0x00B3, /* <super> 3 */
	0x2074, /* <super> 4 */
	0x2075, /* <super> 5 */
	0x2076, /* <super> 6 */
	0x2077, /* <super> 7 */
	0x2078, /* <super> 8 */
	0x2079, /* <super> 9 */
	0x207A, /* <super> + */
	0x207B, /* <super> - */
	0 /* end */
};

/* http://en.wikipedia.org/wiki/Unicode_subscripts_and_superscripts */
const uint16_t LsTableColumn::unicode_sub[] = {
	0x2080, /* <sub> 0 */
	0x2081, /* <sub> 1 */
	0x2082, /* <sub> 2 */
	0x2083, /* <sub> 3 */
	0x2084, /* <sub> 4 */
	0x2085, /* <sub> 5 */
	0x2086, /* <sub> 6 */
	0x2087, /* <sub> 7 */
	0x2088, /* <sub> 8 */
	0x2089, /* <sub> 9 */
	0x208A, /* <sub> + */
	0x208B, /* <sub> - */

	0x2090, /* <sub> a */
	0x2091, /* <sub> e */
	0x2092, /* <sub> o */
	0x2093, /* <sub> x */
	0x2094, /* <sub> <invert>e */
	0x2095, /* <sub> h */
	0x2096, /* <sub> k */
	0x2097, /* <sub> l */
	0x2098, /* <sub> m */
	0x2099, /* <sub> n */
	0x209A, /* <sub> p */
	0x209B, /* <sub> s */
	0x209C, /* <sub> t */
	0 /* end */
};

QString LsTableColumn::equation()
{
	return m_equation->equation();
}

/**
 * @return 0 if no iteration not required
 * @return > 0 if fixed iteration is required
 */
int LsTableColumn::equationIterations()
{
	return m_equation->count();
}
