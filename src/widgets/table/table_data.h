/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_TABLE_DATA_H
#define LIAB_STUDIO_WIDGETS_TABLE_DATA_H

#include <QObject>
#include <QAbstractTableModel>
#include <QVector>
#include <QList>

#include <QDebug>
#include <iostream>

#include <cmath>
#include <csv.h>
#include <QColor>
#include <QMutex>

class LsTableDataColumn : public QObject
{
	Q_OBJECT

	public:
		/** lock need to be locked to hold exclusive access */
		QMutex lock;
		float *data;
		int data_count;
		int data_capacity;
		QString long_name;
		QString short_name;
		QString unit;
		QColor color;
		bool prevent_edit;
		int sig_digits;

		/* if not zero, the data is assumed to be sampled at this frequency. */
		qreal sampling_freq;

		LsTableDataColumn() {
			data = NULL;
			data_count = 0;
			data_capacity = 0;
			sig_digits = -1;
			sampling_freq = 0;
		}
};

/**
 * set NAN for the @a data starting from @a off upto @a len
 * @param data Data
 * @param offset Offset
 * @param len Length
 */
inline void ls_table_set_nan_data_range(float *data, int offset, int len)
{
	for (int i = offset; i < len; i++) {
		data[i] = NAN;
	}
}

/**
 * Get allocated raw memory (raw = not initalized to NAN)
 * @param capacity Number of element
 * @param previous pointer to allocate memory (NULL if not)
 * @return pointer to float
 * @note return can be NULL (failure)
 */
inline float *ls_table_alloc_raw_memory(int count, float *ptr = NULL)
{
	return (float *) realloc((void *) ptr, sizeof(float) * count);
}

inline void ls_table_free_raw_memory(float *ptr)
{
	if (ptr != NULL) {
		free((void *) ptr);
	}
}

inline void ls_table_assure_capacity(LsTableDataColumn *col, int capacity,
	bool init_to_nan = true)
{
	if (col->data_capacity >= capacity) {
		return;
	}

	/* atleast alloc 100 item or so */
	capacity = qMax(capacity, col->data_capacity + 100);

	col->data = ls_table_alloc_raw_memory(capacity, col->data);
	if (init_to_nan) {
		ls_table_set_nan_data_range(col->data, col->data_capacity, capacity);
	}
	col->data_capacity = capacity;
}

class LsTableData : public QAbstractTableModel
{
	Q_OBJECT
	public:
		LsTableData(QObject *parent);
		int rowCount(const QModelIndex &parent = QModelIndex()) const ;
		int columnCount(const QModelIndex &parent = QModelIndex()) const;
		QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
		QVariant headerData(int section, Qt::Orientation orientation, int role) const;

		bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
		Qt::ItemFlags flags(const QModelIndex & index) const;

		bool removeRow(int row, const QModelIndex & parent = QModelIndex());
		bool removeColumn(int column, const QModelIndex & parent = QModelIndex());
		bool insertRow(int row, const QModelIndex & parent = QModelIndex());
		bool insertColumn(int column, const QModelIndex & parent = QModelIndex());

		/* custom */

		bool clearColumn(int column, const QModelIndex & parent = QModelIndex());
		bool clearColumns(int start, int end, const QModelIndex & parent = QModelIndex());

		QList<LsTableDataColumn *> columns;
		inline void setDataDirect(int row, int column, float value) {
			Q_ASSERT(column >= 0 && column < columns.size());
			LsTableDataColumn *col = columns.at(column);

			col->lock.lock();

			ls_table_assure_capacity(col, row + 1);
			col->data[row] = value;
			col->data_count = qMax(row, col->data_count);

			col->lock.unlock();
		}

		void writeToCsv(const QString &file_name, bool tsv = false);

		int appendColumn(QString long_name, QString short_name,
			QString unit, QColor color, bool prevent_edit,
			int sig_digits, float *data, int data_count);
		void fakeReset();
};

#endif
