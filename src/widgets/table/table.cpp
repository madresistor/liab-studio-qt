/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "table.h"
#include <QTextStream>
#include <QGridLayout>
#include <QLabel>
#include "column.h"
#include "column_ft.h"
#include <cmath>
#include <QDateTime>
#include <QMessageBox>
#include <QFileDialog>
#include "delegates/number.h"

#include "extra/parser/parser.h"
#include "extra/equation.h"
#include "extra/sigproc/fft.h"

using namespace std;

/*
 * BUG:
 *  - if a row is cleared, its data is not cleared.
 * 		reason: also reference text() to check if their is any string
 *
 *  - if source is removed, and change to setting
 * 		(other than source_index) is performed,
 *		a clear to column occur
 * 		 reason: because the index isnt set to -1, thats why it will
 * 		   unknowingly think that their is a source change
 *
 *	TODO:
 *   - add rows + add row at arbitary position
 *   - save as csv
 *   - load from csv
 *
 */

LsTable::LsTable(LsBank *bank, QWidget *parent):
	LsWidget(bank, parent)
{
	ui.setupUi(this);

	tableData = new LsTableData(this);
	ui.bind->setModel(tableData);

	//Qt::UserRole is for the sig_digits flag
	ui.bind->setItemDelegate(new LsTableDelegateNumber(Qt::UserRole, this));

	setup();

	/* save, load */
	connect(ui.actionSave, SIGNAL(triggered()), this, SLOT(save()));
	connect(ui.actionClear, SIGNAL(triggered()), this, SLOT(clear()));
	connect(ui.actionResizeColumnsToContent, SIGNAL(triggered()),
			ui.bind, SLOT(resizeColumnsToContents()));

	/* edit title facility */
	ui.lblTitle->setText(title());
	connect(ui.lblTitle, SIGNAL(userWantToChange(const QString &)),
		SLOT(setTitle(const QString &)));
	connect(this, SIGNAL(titleChanged(const QString &)),
		ui.lblTitle, SLOT(setText(const QString &)));
}

void LsTable::save()
{
	QString path = QString("%1/%2").arg(documentDir()).arg(title());
	QStringList filter, filter_ext;
	filter << "Comma Separated Values (*.csv)"; filter_ext << "csv";
	filter << "Tab Separated Values (*.tsv)"; filter_ext << "tsv";
	QString selected_filter;
	QString filename = QFileDialog::getSaveFileName(this, tr("Save to file"),
		path, filter.join(";;"), &selected_filter);

	/* do we need to save? */
	if (!filename.isEmpty()) {
		/* Ref: http://stackoverflow.com/a/9822246/1500988 */
		if (QFileInfo(filename).suffix().isEmpty()) {
			int index = filter.indexOf(selected_filter);
			QString ext = filter_ext.at(index);
			filename.append('.').append(ext);
		}

		tableData->writeToCsv(filename, QFileInfo(filename).suffix() == "tsv");
	}
}

/* ======================= columns related =================== */
void LsTable::showMenuColumn(const QPoint & pos)
{
	int logical_index = ui.bind->horizontalHeader()->logicalIndexAt(pos);

	if (logical_index < 0) {
		qDebug() << "really occured?";
		return;
	}

	QMenu menu;

	QAction actionFt("Fourier Transform", &menu);
	QAction actionModify("Modify", &menu);
	QAction actionClear("Clear", &menu);
	QAction actionDelete("Delete", &menu);

	/* has data? */
	LsTableDataColumn *col = tableData->columns.at(logical_index);
	actionFt.setEnabled(col->data_count > 2);

	menu.addAction(&actionFt);
	menu.addAction(&actionModify);
	menu.addAction(&actionClear);
	menu.addAction(&actionDelete);

	/* TODO: make a list of source available compatible with
	 * type so that user can select them without going to modify.
	 */

	QAction *sel = menu.exec(QCursor::pos());
	if (sel == &actionFt) {
		userFourierTransformColumn(logical_index);
	} else if (sel == &actionModify) {
		userModifyColumn(logical_index);
	} else if (sel == &actionClear) {
		QString title = tr("Clear Column");
		QString msg = tr("Do you really want to clean the column content?");
		if (QMessageBox::question(this, title, msg) == QMessageBox::Yes) {
			userClearColumn(logical_index);
		}
	} else if (sel == &actionDelete) {
		QString title = tr("Delete Column");
		QString msg = tr("Do you really want to delete the column?");
		if (QMessageBox::question(this, title, msg) == QMessageBox::Yes) {
			userDeleteColumn(logical_index);
		}
	}
}

void LsTable::setup()
{
	QHeaderView *headerView = ui.bind->horizontalHeader();

	/* show Modify, Delete popup on right click on column header */
	headerView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(headerView, SIGNAL(customContextMenuRequested(const QPoint &)),
		SLOT(showMenuColumn(const QPoint &)));

	/* double click column to modify */
	connect(headerView, SIGNAL(sectionDoubleClicked(int)),
		SLOT(userModifyColumn(int)));

	/* column add on column-add-action */
	connect(ui.actionColumnAdd, SIGNAL(triggered()), SLOT(userAddColumn()));
}

void LsTable::userAddColumn()
{
	LsTableColumn dialog(m_bank, this);
	QString longName = QString("Column %1").arg(inc_column);
	QString shortName = QString("col %1").arg(inc_column);
	QColor color;
	color.setHslF(fmod(qrand() / 1000.0, 1.0), 0.8, 0.5);

	dialog.setWindowTitle("New column");
	dialog.setLongName(longName);
	dialog.setShortName(shortName);
	dialog.setColor(color);
	if (dialog.exec() == QDialog::Accepted) {
		appendColumn(dialog.longName(),
					dialog.shortName(),
					dialog.unit(),
					dialog.color(),
					dialog.preventEdit(),
					dialog.sig_digits(),
					dialog.equation(),
					dialog.equationIterations());
		inc_column++;
	}
}

void LsTable::userFourierTransformColumn(int logical_index)
{
	LsTableColumnFt dialog(this);

	LsTableDataColumn *col = tableData->columns.at(logical_index);
	if (col->sampling_freq > 0) {
		dialog.setFrequency(col->sampling_freq);
	}

	if (dialog.exec() != QDialog::Accepted) {
		return;
	}

	size_t N = col->data_count;
	size_t N_2 = N / 2;
	float *input = col->data;
	qreal sampling_freq = dialog.frequency();
	float *freq = ls_table_alloc_raw_memory(N_2);
	float *amp = ls_table_alloc_raw_memory(N_2);
	size_t valid = ls_fft_forward(sampling_freq, input, N, freq, amp);

	/* show to user */
	int id_col_x = appendColumn(
		col->long_name + " - Frequency (FFT)",
		col->short_name + "_freq",
		"Hertz", col->color, true, -1);
	LsTableDataColumn *col_x = tableData->columns.at(id_col_x);
	col_x->data = freq;
	col_x->data_count = valid;
	col_x->data_capacity = N_2;

	int id_col_y = appendColumn(
		col->long_name + " - Amplitude (FFT)",
		col->short_name + "_amp",
		col->unit, col->color, true, -1);
	LsTableDataColumn *col_y = tableData->columns.at(id_col_y);
	col_y->data = amp;
	col_y->data_count = valid;
	col_y->data_capacity = N_2;
}

void LsTable::userModifyColumn(int logical_index)
{
	LsTableColumn dialog(this);
	dialog.setWindowTitle("Modify column");

	LsTableDataColumn *col = tableData->columns.at(logical_index);
	dialog.setLongName(col->long_name);
	dialog.setShortName(col->short_name);
	dialog.setUnit(col->unit);
	dialog.setColor(col->color);
	dialog.setPreventEdit(col->prevent_edit);
	dialog.setPrecision(col->sig_digits);

	if (dialog.exec() == QDialog::Accepted) {
		tableData->columns.at(logical_index)->long_name = dialog.longName();
		tableData->columns.at(logical_index)->short_name = dialog.shortName();
		tableData->columns.at(logical_index)->unit = dialog.unit();
		tableData->columns.at(logical_index)->color = dialog.color();
		tableData->columns.at(logical_index)->prevent_edit = dialog.preventEdit();
		tableData->columns.at(logical_index)->sig_digits = dialog.sig_digits();
	}
}

void LsTable::userDeleteColumn(int logical_index)
{
	tableData->removeColumn(logical_index);
}

void LsTable::userClearColumn(int logical_index)
{
	tableData->clearColumn(logical_index);
}

static bool extract_deps(
	LsBank *bank, LsParser &parser, QHash<QString, LsTableDataColumn*> *deps)
{
	mu::varmap_type vars = parser.GetUsedVar();
	mu::varmap_type::const_iterator item;

	/* regular expression for seperating variable name into ID and TYPE */
	QRegExp var_regx = LsTableColumnEquation::variableIdRegx();

	QList<QPointer<LsTable>> tables = bank->tables();

	for (item = vars.begin(); item != vars.end(); item++) {
		QString id = QString::fromStdString(item->first);

		/* "row" is the iteration variable
		 *  and "ROWS" is the number of iterations */
		if (id == "row" || id == "ROWS") {
			continue;
		}

		/* valid variable name? */
		if (var_regx.indexIn(id) == -1) {
			qWarning() << id << "not a valid variable name";
			return false;
		}

		bool ok;

		/* Extract table index from "id" */
		int table_index = var_regx.cap(1).toInt(&ok);
		if (!ok) {
			qWarning() << id << "table index not valid";
			return false;
		}

		/* Extract column index from "id" */
		int column_index = var_regx.cap(2).toInt(&ok);
		if (!ok) {
			qWarning() << id << "column index not valid";
			return false;
		}

		/* find the column */
		if (table_index < 0 && table_index >= tables.count()) {
			qWarning() << table_index << "table index out of range";
			return false;
		}

		LsTable *table = tables.at(table_index);
		QList<LsTableDataColumn*> columns = table->tableData->columns;

		/* find the column */
		if (column_index < 0 && column_index >= columns.count()) {
			qWarning() << column_index << "column index out of range";
			return false;
		}

		LsTableDataColumn *column = columns.at(column_index);

		deps->insert(id, column);
	}

	return true;
}

/**
 * Convert @a equation and @a count to floating data
 * @param bank Bank
 * @param equation Equation
 * @param count Count
 * @return floating data or NULL
 * @note If @a count is 0, calculate from dependency columns
 */
static float *convert_equation_to_data(LsBank *bank,QString equation, int &count)
{
	LsParser parser;
	parser.setExpression(equation);

	QHash<QString, LsTableDataColumn*> deps;

	if (!extract_deps(bank, parser, &deps)) {
		return NULL;
	}

	/* calculate the iteration count */
	if (!count) {
		QHashIterator<QString, LsTableDataColumn*> i(deps);
		while (i.hasNext()) {
			i.next();
			count = qMax(i.value()->data_count, count);
		}
	}

	/* parser evaluation specific data */
	int deps_count = deps.size();
	double deps_data[deps_count];
	LsTableDataColumn *deps_col[deps_count];
	double row;

	/* define all constant and variable of the equation */
	parser.defineVariable("row", &row);
	parser.defineConstant("ROWS", count);
	QHashIterator<QString, LsTableDataColumn*> deps_iter(deps);
	int offset = 0;
	while (deps_iter.hasNext()) {
		deps_iter.next();
		deps_col[offset] = deps_iter.value();
		parser.defineVariable(deps_iter.key(), &deps_data[offset]);
		offset++;
	}

	float *data = ls_table_alloc_raw_memory(count);
	if (data == NULL) {
		qWarning() << "unable to allocate raw float memory for equation";
		return NULL;
	}

	for (int i = 0; i < count; i++) {
		for (int j = 0; j < deps_count; j++) {
			LsTableDataColumn *col = deps_col[j];

			if (i >= col->data_count) {
				/* there is not enough data in dependency to count */
				goto skip;
			}

			/* copy the content */
			float val = col->data[i];

			if (isnan(val)) {
				/* data is already NAN, evaluation is undetermined */
				goto skip;
			}

			deps_data[j] = val;
		}

		row = i + 1; /* "row" goes from 1 to COUNT */
		data[i] = parser.Eval();
		continue;

		skip:
		data[i] = NAN;
	}

	return data;
}

int LsTable::appendColumn(QString long_name, QString short_name,
	QString unit, QColor color, bool prevent_edit, int sig_digits,
	QString equation, int iteration_count)
{
	float *data = equation.isEmpty() ? Q_NULLPTR :
		convert_equation_to_data(m_bank, equation, iteration_count);

	if (data == Q_NULLPTR) {
		iteration_count = 0;
	}

	return tableData->appendColumn(long_name, short_name, unit,
		color, prevent_edit, sig_digits, data, iteration_count);
}

void LsTable::update()
{
	tableData->fakeReset();
}

void LsTable::clear()
{
	QString title = tr("Clear table");
	QString msg = tr("Do you really want to clear the full table?");
	if (QMessageBox::question(this, title, msg) == QMessageBox::Yes) {
		tableData->clearColumns(0, tableData->columns.size() - 1);
	}
}
