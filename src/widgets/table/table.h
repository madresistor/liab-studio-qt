/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_TABLE_H
#define LIAB_STUDIO_WIDGETS_TABLE_H

#include <QtGui>
#include <QWidget>
#include <QFile>

#include "widgets/widget/widget.h"
#include <QVariant>
#include "table_data.h"
#include <QPoint>

#include "ui_table.h"

class LsTable : public LsWidget
{
	Q_OBJECT

	public:
		LsTable(LsBank *bank, QWidget *parent = Q_NULLPTR);

	public Q_SLOTS:
		void userAddColumn();
		void userFourierTransformColumn(int logical_index);
		void userModifyColumn(int logical_index);
		void userDeleteColumn(int logical_index);
		void userClearColumn(int);

		/* column */
		void showMenuColumn(const QPoint & pos);

	public:
		int appendColumn(QString long_name,
			QString short_name = QString::null,
			QString unit = QString::null, QColor color = QColor(),
			bool prevent_edit = false, int sig_digits = -1,
			QString equation = QString::null, int iteration_count = 0);

		inline void setDataDirect(int row, int column, float value) {
			tableData->setDataDirect(row, column, value);
		}

	public Q_SLOTS:
		void update();

	protected Q_SLOTS:
		void save();
		void clear();

	private:
		Ui::LsTable ui;

		struct LsTableDataColumnInfo {
			public:
			QString long_name;
			QString short_name;
			QString unit;
			QColor color;
		};
		QList<LsTableDataColumnInfo> m_columnInfo;

		void setup();

	public:
		LsTableData *tableData;
		uint inc_column = 1;
};

#endif
