/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_TABLE_COLUMN_H
#define LIAB_STUDIO_WIDGETS_TABLE_COLUMN_H

#include <stdint.h>
#include <QDialog>
#include <QMenu>
#include <QList>
#include <QPair>
#include <QLineEdit>
#include <QPushButton>

#include "bank.h"
#include "extra/equation.h"

#include "ui_table_column.h"

class LsTableColumn : public QDialog
{
	Q_OBJECT

	public:
		explicit LsTableColumn(QWidget *parent = Q_NULLPTR) :
			LsTableColumn(Q_NULLPTR, parent) {};
		explicit LsTableColumn(LsBank *bank, QWidget *parent = Q_NULLPTR);

		void setLongName(QString &);
		void setShortName(QString &);
		void setUnit(QString &);
		void setColor(QColor &value);
		void setPreventEdit(bool prevent_edit);
		void setPrecision(int sig_digits);
		void setWidth(int width);

		QString longName();
		QString shortName();
		QString unit();
		QColor color();
		bool preventEdit();
		int sig_digits();

		QString equation();
		int equationIterations();

	public Q_SLOTS:
		virtual void done(int r);

	private Q_SLOTS:
		void appendTextFromAction(QAction *action);

	private:
		Ui::LsTableColumn ui;
		static void addFromUnicodeArray(QMenu *menu, const uint16_t list[]);
		void fillButtonWithSymbol(QPushButton *button);

		static const uint16_t unicode_greek[];
		static const uint16_t unicode_super[];
		static const uint16_t unicode_sub[];

		LsTableColumnEquation *m_equation = Q_NULLPTR;
};

#endif
