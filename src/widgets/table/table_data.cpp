/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "table_data.h"
#include <QDebug>
#include <cmath>
#include <QColor>
#include <QTextStream>
#include <QFile>
#include <cstdio>

using namespace std;

LsTableData::LsTableData(QObject *parent) :
	QAbstractTableModel(parent)
{
}

int LsTableData::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	int max = 0;
	for (int i = 0; i < columns.size(); i++) {
		int siz = columns.at(i)->data_count;

		if (max < siz) {
			max = siz;
		}
	}

	return max + 100;
}

int LsTableData::appendColumn(QString long_name, QString short_name,
	QString unit, QColor color, bool prevent_edit,
	int sig_digits, float *data, int data_count)
{
	beginResetModel();

	int index = columns.size();

	LsTableDataColumn *col = new LsTableDataColumn();
	col->long_name = long_name;
	col->short_name = short_name;
	col->unit = unit;

	if (!color.isValid()) {
		qreal hue = (qrand() / (qreal) RAND_MAX);
		color.setHslF(hue, 0.8f, 0.5f);
	}

	col->color = color;
	col->prevent_edit = prevent_edit;
	col->sig_digits = sig_digits;

	col->data = data;
	col->data_count = data_count;
	col->data_capacity = data_count;

	columns.append(col);

	endResetModel();

	return  index;
}

int LsTableData::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	}

	return columns.size();
}

QVariant LsTableData::data(const QModelIndex &index, int role) const
{
	int column = index.column();
	if (column >= 0 && column < columns.size()) {
		const LsTableDataColumn *col = columns.at(column);

		if (role == Qt::DisplayRole || role == Qt::EditRole) {
			int row = index.row();
			if (row >= 0 && row < col->data_count) {
				float _v = col->data[row];
				return isnan(_v) ? QVariant() : _v;
			}
		} else if (role == Qt::ForegroundRole) {
			return col->color;
		} else if (role == Qt::UserRole) { // sig_digits
			return col->sig_digits;
		}
	}

	return QVariant();
}

QVariant LsTableData::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal) {
			if (section < columns.size()) {
				return columns.at(section)->long_name;
			}
		} else {
			return (section + 1);
		}
	} else if (role == Qt::ForegroundRole) {
		if (orientation == Qt::Horizontal && section < columns.size()) {
			return columns.at(section)->color;
		}
	}

	return QVariant();
}

bool LsTableData::insertColumn(int column, const QModelIndex & parent)
{
	if (parent.column() != -1 && parent.row() != -1) {
		return false;
	}

	if (column >= 0 && column < columns.size()) {
		columns.insert(column, new LsTableDataColumn());
		return true;
	}

	return false;
}

//~ bool LsTableData::insertColumns(int column, int count, const QModelIndex & parent = QModelIndex())
//~ {
	//~
//~ }

bool LsTableData::insertRow(int row, const QModelIndex & parent)
{
	if (parent.column() != -1 && parent.row() != -1) {
		return false;
	}

	for (int i = 0; i < columns.size(); i++) {
		LsTableDataColumn *col = columns.at(i);
		col->lock.lock();

		ls_table_assure_capacity(col, row + 1);

		/* move the values */
		float prev = NAN;
		for (int j = row; j < col->data_count; j++) {
			float tmp = col->data[j];
			col->data[j] = prev;
			prev = tmp;
		}

		col->lock.unlock();
	}

	return true;
}

//~ bool LsTableData::insertRows(int row, int count, const QModelIndex & parent = QModelIndex())

//~ //another note
    //~ Q_EMIT layoutAboutToBeChanged
    //~ Remember the QModelIndex that will change
    //~ Update your internal data
    //~ Call changePersistentIndex()
    //~ Q_EMIT layoutChanged

//~ bool QAbstractItemModel::moveLsTableDataColumn(const QModelIndex & sourceParent, int sourceLsTableDataColumn, const QModelIndex & destinationParent, int destinationChild)

//~ bool QAbstractItemModel::moveLsTableDataColumns(const QModelIndex & sourceParent, int sourceLsTableDataColumn, int count, const QModelIndex & destinationParent, int destinationChild)

//~ bool QAbstractItemModel::moveRow(const QModelIndex & sourceParent, int sourceRow, const QModelIndex & destinationParent, int destinationChild)

//~ bool QAbstractItemModel::moveRows(const QModelIndex & sourceParent, int sourceRow, int count, const QModelIndex & destinationParent, int destinationChild)

bool LsTableData::removeColumn(int column, const QModelIndex & parent)
{
	if (parent.column() != -1 && parent.row() != -1) {
		return false;
	}

	if (column >= 0 && column < columns.size()) {
		beginRemoveColumns(parent, column, column);
		delete columns.takeAt(column);
		endRemoveColumns();
		return true;
	}

	return false;
}

//~ bool QAbstractItemModel::removeColumns(int column, int count, const QModelIndex & parent = QModelIndex())

bool LsTableData::removeRow(int row, const QModelIndex & parent)
{
	if (parent.column() != -1 && parent.row() != -1) {
		return false;
	}

	bool foundAny = false;
	for (int i = 0; i < columns.size(); i++) {
		LsTableDataColumn *col = columns.at(i);
		if (row < col->data_count) {
			foundAny = true;
			beginRemoveRows(parent, row, row);

			col->lock.lock();
			size_t bytes = sizeof(float) * (col->data_count - row - 1);
			memcpy(&col->data[row], &col->data[row + 1], bytes);
			col->data_count -= 1;
			col->lock.unlock();

			endRemoveRows();
		}
	}

	return foundAny;
}

//~ bool QAbstractItemModel::removeRows(int row, int count, const QModelIndex & parent = QModelIndex())

//~ bool LsTableData::setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role = Qt::EditRole)

bool LsTableData::clearColumn(int column, const QModelIndex & parent)
{
	if (parent.column() != -1 && parent.row() != -1) {
		return false;
	}

	if (column >= 0 && column < columns.size()) {
		beginResetModel();
		LsTableDataColumn *col = columns.at(column);
		col->lock.lock();
		ls_table_set_nan_data_range(col->data, 0, col->data_count);
		col->lock.unlock();
		endResetModel();
		return true;
	}

	return false;
}

bool LsTableData::clearColumns(int start, int end, const QModelIndex & parent)
{
	if (parent.column() != -1 && parent.row() != -1) {
		return false;
	}

	if (start >= 0 && end >= 0 && start < columns.size() && end < columns.size()) {
		beginResetModel();

		for (int i = start; i <= end; i++) {
			LsTableDataColumn *col = columns.at(i);
			col->lock.lock();
			ls_table_set_nan_data_range(col->data, 0, col->data_count);
			col->lock.unlock();
		}

		endResetModel();
		return true;
	}

	return false;
}

bool LsTableData::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole) {
		int column = index.column();
		LsTableDataColumn *col = columns.at(column);

		col->lock.lock();

		int row = index.row();
		ls_table_assure_capacity(col, row + 1);

		bool ok;
		float _val = value.toFloat(&ok);
		col->data[row] = ok ? _val : NAN;
		col->data_count = qMax(col->data_count, row + 1);

		col->lock.unlock();

		Q_EMIT dataChanged(index, index);
	}

	return true;
}

void LsTableData::fakeReset()
{
	beginResetModel();
	endResetModel();
}

Qt::ItemFlags LsTableData::flags(const QModelIndex &index) const
{
	int column = index.column();
	if (column >= 0 && column < columns.size()) {
		const LsTableDataColumn *col = columns.at(column);
		if (col->prevent_edit) {
			return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
		}
	}

	return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
}

void LsTableData::writeToCsv(const QString &file_name, bool tsv)
{
	int cols_count = columns.size();
	if (!cols_count) {
		qDebug() << "nothing to save for " << file_name;
		return;
	}

	QByteArray _file_name = file_name.toLocal8Bit();
	FILE *file = fopen(_file_name.constData(), "wt");
	if (file == NULL) {
		qDebug() << "unable to open file" << file_name;
		return;
	}

	QVector<LsTableDataColumn *> cols_vector =
		QVector<LsTableDataColumn *>::fromList(columns);
	LsTableDataColumn **cols = cols_vector.data();

	int row_count = 0;
	int last_col = cols_count - 1;

	const char *value_sep = tsv ? "\t" : ", ";
	const char *new_line = "\n";

	/* print the header */
	for (int c = 0; c < cols_count; c++) {
		LsTableDataColumn *col = cols[c];

		/* calculate the largest column in samples number */
		row_count = qMax(row_count, col->data_count);

		const char *end_with = (c != last_col) ? value_sep : new_line;
		QByteArray name = col->long_name.toLocal8Bit();
		fprintf(file, "\"%s\"%s", name.constData(), end_with);
	}

	/* print the rows */
	for (int r = 0; r < row_count; r++) {
		for (int c = 0; c < cols_count; c++) {
			LsTableDataColumn *col = cols[c];
			float value = (r < col->data_count) ? col->data[r] : NAN;
			const char *end_with = (c != last_col) ? value_sep : new_line;

			if (isnan(value)) {
				fputs(end_with, file);
			} else if (col->sig_digits < 0) {
				fprintf(file, "%.7g%s", value, end_with);
			} else {
				fprintf(file, "%#.*g%s", col->sig_digits, value, end_with);
			}
		}
	}

	fclose(file);
}
