/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_TEXT_H
#define LIAB_STUDIO_WIDGETS_TEXT_H

#include <QObject>
#include <QWidget>
#include <QMenu>

#include "widgets/widget/widget.h"

#include "ui_text.h"

class LsText : public LsWidget
{
	Q_OBJECT

	public:
		LsText(LsBank *bank, QWidget *parent = Q_NULLPTR);

	private:
		Ui::LsText ui;
};

#endif
