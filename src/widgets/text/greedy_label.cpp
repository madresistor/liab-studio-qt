/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "greedy_label.h"
#include <QPaintEvent>
#include <QPainter>
#include <QDebug>

LsGreedyLabel::LsGreedyLabel(QWidget *parent) :
	QLabel(parent)
{
}

/*
 * Ref: http://developer.nokia.com/community/wiki/How_to_get_maximum_Font_Size_in_Qt_to_fit_text_into_Widget
 */
void LsGreedyLabel::resizeEvent(QResizeEvent * /* event */)
{
	int width = size().width();
	int height = size().height();

	//~ int fontPointSize = 8;
	const QString aText = text();
	QFont aFont = font();

	const int min = 14, max = 1000;
	int calc_min = min, calc_max = max, calc;

	//~ qDebug() << "calc " << "started";

	/*
	 * Algorithm:
	 *  try to find a font value
	 *   start from midpoint
	 *   if value greater than require, make it greater
	 *
	 * complexity: TODO -- afaic imagine, reaches to solution quicker because of /2
	 *   afai seen, takes 9 iterations
	 */
	//~ int iteration = 0;
	for (;;) {
		//~ iteration++;
		calc = (calc_min + calc_max) / 2;

		/* break if we get out of range */
		if (calc <= min) {
			calc = min;
			//~ qDebug() << "unexpected, calc <= min";
			break;
		} else if (calc >= max) {
			calc = max;
			//~ qDebug() << "unexpected, calc >= max";
			break;
		}

		if ((calc_max - calc_min) < 3) {
			/* we can manage with this value */
			break;
		}

		aFont.setPointSize(calc);
		QFontMetrics fm(aFont);

		if ((fm.width(aText) > width) || (fm.height() > height)) {
			calc_max = calc;
			//~ qDebug() << "calc " << calc << "is greater";
		} else {
			//~ qDebug() << "calc " << calc << "is smaller";
			calc_min = calc;
		}
	}

	//~ qDebug() << "calc " << "ended with" << iteration << " iterations";

	aFont.setPointSize(calc - 3);
	setFont(aFont);
}
