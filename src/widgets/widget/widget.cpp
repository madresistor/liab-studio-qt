/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "widget.h"
#include <QMessageBox>
#include <QStandardPaths>

/**
 * All Widgets need to subclass LsWidget
 * A Widget is /usually/ used to visualize data and shown to user.
 */

LsWidget::LsWidget(LsBank *bank, QWidget *parent):
	QMainWindow(parent),
	m_bank(bank)
{
	connect(this, SIGNAL(windowTitleChanged(const QString &)),
			this, SIGNAL(titleChanged(const QString &)));
}

/**
 * Return the instance of bank provided to widget
 * @param Bank instance
 */
LsBank *LsWidget::bank()
{
	return m_bank;
}

/**
 * Return the current title of widget
 * @param title
 */
QString LsWidget::title()
{
	return windowTitle();
}

/**
 * Set the title of widget
 * @param title New title
 * @note emit titleChange(new-title)
 */
void LsWidget::setTitle(const QString &title)
{
	setWindowTitle(title);
	Q_EMIT titleChanged(title);
}

/**
 * Make the widget so that it can do full interaction to user.
 */
void LsWidget::fullInteraction()
{
	showMaximized();
}

/**
 * Get directory of user where documents to be stored
 * @return path
 */
QString LsWidget::documentDir()
{
	return QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
}

/**
 * Get directory of user where picture to be stored
 * @return path
 */
QString LsWidget::pictureDir()
{
	return QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
}
