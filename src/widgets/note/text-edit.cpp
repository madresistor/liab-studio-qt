/*
 * Copyright (C) 2010 Narek
 * Author: http://stackoverflow.com/users/163394/narek
 * Source: http://stackoverflow.com/a/3720560/1500988 ("The third way")
 * Licence: CC-BY-SA 3.0
 */

#include "text-edit.h"
#include <QFile>
#include <QFileInfo>
#include <QImageReader>

LsNoteTextEdit::LsNoteTextEdit(QWidget *parent) :
	QTextEdit(parent)
{
}

bool LsNoteTextEdit::canInsertFromMimeData(const QMimeData *source) const
{
	return source->hasImage() || source->hasUrls() ||
		QTextEdit::canInsertFromMimeData(source);
}

void LsNoteTextEdit::insertFromMimeData(const QMimeData *source)
{
	if (source->hasImage()) {
		static int i = 1;
		QUrl url(QString("dropped_image_%1").arg(i++));
		dropImage(url, qvariant_cast<QImage>(source->imageData()));
	} else if (source->hasUrls()) {
		Q_FOREACH (QUrl url, source->urls()) {
			QFileInfo info(url.toLocalFile());
			QByteArray ext = info.suffix().toLower().toLatin1();
			if (QImageReader::supportedImageFormats().contains(ext)) {
				dropImage(url, QImage(info.filePath()));
			} else {
				dropTextFile(url);
			}
		}
	} else {
		QTextEdit::insertFromMimeData(source);
	}
}

void LsNoteTextEdit::dropImage(const QUrl &url, const QImage &image)
{
	if (!image.isNull()) {
		document()->addResource(QTextDocument::ImageResource, url, image);
		textCursor().insertImage(url.toString());
	}
}

void LsNoteTextEdit::dropTextFile(const QUrl &url)
{
	QFile file(url.toLocalFile());
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		textCursor().insertText(file.readAll());
	}
}
