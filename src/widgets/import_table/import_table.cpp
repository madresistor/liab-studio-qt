/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "import_table.h"
#include "extra/color_gen/color_gen.h"
#include <cerrno>
#include <cstdlib>
#include <QFileInfo>

using namespace std;

LsImportTable::LsImportTable(const QString &filePath, LsBank *bank, QWidget *parent) :
	LsTable(bank, parent)
{
	/* read the file */
	QFileInfo info(filePath);
	readFromCsv(filePath, info.suffix() == "tsv");
}

static int is_space(unsigned char c)
{
	return (c == CSV_SPACE || c == CSV_TAB) ? 1 : 0;
}

static int is_term(unsigned char c)
{
	return (c == CSV_CR || c == CSV_LF) ? 1 : 0;
}

/* this is the data type that is passed to callback to keep track of state */
struct csv_table_arg {
	LsTableData *tableData;
	int column_num, row_num;
	bool reset_raw;
};

/* Currently double values are only supported */
static void callback_1(void *_s, size_t len, void *data)
{
	struct csv_table_arg *arg = (struct csv_table_arg *) data;
	const char *s = (const char *) _s;

	if (arg->tableData->columns.size() <= arg->column_num) {
		/* FIX: if a new column appear after some rows,
		 *   its first element will be used for header
		 */
		LsTableDataColumn *col = new LsTableDataColumn();
		col->long_name = QString::fromLocal8Bit(s, len);
		col->prevent_edit = true;
		col->color = LsColorGen::color();
		arg->tableData->columns.append(col);
		arg->reset_raw = true;
		arg->column_num++;
		return;
	}

	/* convert to float */
	errno = 0;
	char *endptr;
	float value = strtof(s, &endptr);
	if (endptr == _s || errno) {
		value = NAN;
		qDebug() << "problem in data at" << arg->row_num << arg->column_num;
	}

	LsTableDataColumn *col = arg->tableData->columns.at(arg->column_num);
	ls_table_assure_capacity(col, arg->row_num + 1, false);
	col->data[arg->row_num] =  value;
	col->data_count = arg->row_num;
	arg->column_num++;
}

static void callback_2(int c, void *data)
{
	Q_UNUSED(c);
	struct csv_table_arg *arg = (struct csv_table_arg *) data;

	if (arg->reset_raw) {
		arg->row_num = 0;
		arg->reset_raw = false;
	} else {
		arg->row_num++;
	}
	arg->column_num = 0;
}

/**
 * Read CSV file
 * @param file_name File to read
 * @param tab_sep Tab seperated data
 */
void LsImportTable::readFromCsv(const QString &file_name, bool tab_sep)
{
	FILE *fp;
	struct csv_parser p;
	size_t bytes_read;
	unsigned char options = CSV_APPEND_NULL;
	char buf[1024];

	if (csv_init(&p, options) != 0) {
		qDebug() << "Failed to initialize csv parser";
		return;
	}

	/* set callback */
	csv_set_space_func(&p, is_space);
	csv_set_term_func(&p, is_term);

	if (tab_sep) {
		csv_set_delim(&p, CSV_TAB);
	}

	/* open the file */
	QByteArray _file_name = file_name.toLocal8Bit();
	if ((fp = fopen(_file_name.constData(), "rb")) == NULL) {
		qDebug() << "Failed to open " << file_name
				<< ", errno:" << strerror(errno);
	}

	struct csv_table_arg arg;
	arg.tableData = tableData;
	arg.column_num = 0;
	arg.row_num = 0;
	arg.reset_raw = false;

	while ((bytes_read = fread(buf, 1, sizeof(buf), fp)) > 0) {
		if (csv_parse(&p, buf, bytes_read, callback_1, callback_2, &arg)
				!= bytes_read) {
			qDebug() << "Error while parsing file: " <<
				csv_strerror(csv_error(&p));
		}
	}

	csv_fini(&p, callback_1, callback_2, &arg);

	/* data got updated */
	tableData->fakeReset();

	fclose(fp);
	csv_free(&p);
}
