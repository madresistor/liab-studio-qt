/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_IMPORT_TABLE_H
#define LIAB_STUDIO_WIDGETS_IMPORT_TABLE_H

#include <QObject>
#include <QWidget>
#include "widgets/table/table.h"

/**
 * A specialized table that show data from file
 * Currently support: CSV, TSV
 */

class LsImportTable : public LsTable
{
	Q_OBJECT

	public:
		LsImportTable(const QString &filePath, LsBank *bank,
			QWidget *parent = Q_NULLPTR);

	private:
		void readFromCsv(const QString &file_name, bool tab_sep = false);
};

#endif
