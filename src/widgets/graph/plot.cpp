/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plot.h"
#include <QEvent>
#include <QFile>
#include <QMouseEvent>
#include <QSurfaceFormat>
#include <cmath>
#include <QMessageBox>
#include <QDialog>
#include "widgets/table/table_data.h"
#include "plot_fit.h"
#include "fit/fit.h"
#include "extra/dialog/select_one.h"
#include "extra/sigproc/array.h"
#include "extra/parser/parser.h"

using namespace std;

#define MAX_GPU_SAMPLE_COUNT 500000

LsGraphPlot::LsGraphPlot(QWidget *parent) :
	Cartesian(parent)
{
	m_fit.state = LsGraphPlot::DISABLED;
	m_fit.rect = NULL;
	m_fit.line = NULL;
	m_fit.start = QPointF(NAN, NAN);
	m_fit.end = QPointF(NAN, NAN);

	/* for multisampling antialias.
	 *  outcome: i see no visible benifits.
	 */
	//QSurfaceFormat fmt = format();
	//fmt.setSamples(2);
	//setFormat(fmt);

	setMouseTracking(true);
}

LsGraphPlot::~LsGraphPlot()
{
	makeCurrent();

	lp_cartesian_curve_del(m_curve);
	lp_line_del(m_line);
	LsMarker::freeTexture(m_textures);
	lp_rectangle_del(m_fit.rect);
	lp_line_del(m_fit.line);

	doneCurrent();
}

/**
 * Set the label that will show the coordinate.
 * @param label Label to use
 */
void LsGraphPlot::setCoordLabel(QLabel *label)
{
	m_coord_label = label;
}

static void set_axis_spaced_range(lp_cartesian_axis *axis,
		float min, float max, float side_space)
{
	if (isnormal(min) && isnormal(max) && (min < max)) {
		float space = (max - min) * (side_space / 100);
		lp_cartesian_axis_2float(axis,
			LP_CARTESIAN_AXIS_VALUE_RANGE, min - space, max + space);
	}
}

/**
 * Find the best zoom level
 * @note add 10% margin
 */
void LsGraphPlot::zoomFitBest()
{
	float x_min, y_min, y_max, x_max;
	x_min = y_min = +INFINITY;
	x_max = y_max = -INFINITY;

	for (int i = 0; i < m_curves.count(); i++) {
		LsGraphCurve *crv = m_curves.at(i);

		if (!crv->visible) {
			continue;
		}

		LsTableDataColumn *xcol = crv->x;
		LsTableDataColumn *ycol = crv->y;

		for (int j = 0; j < qMin(xcol->data_count, ycol->data_count); j++) {
			float x = xcol->data[j];
			if (isnormal(x)) {
				x_min = qMin(x, x_min);
				x_max = qMax(x, x_max);
			}

			float y = ycol->data[j];
			if (isnormal(y)) {
				y_min = qMin(y, y_min);
				y_max = qMax(y, y_max);
			}
		}
	}

	const qreal side_space = 10; /* percentage */
	set_axis_spaced_range(m_axis_bottom, x_min, x_max, side_space);
	set_axis_spaced_range(m_axis_left, y_min, y_max, side_space);

	update();
}

/**
 * Draw the curve data
 * @param c libreplot cartesian
 * @param gc Curve
 * @param cc libreplot cartesian curve
 * @param texture List of texuture markers
 */
static void draw_curve(lp_cartesian *c, LsGraphCurve *gc,
	lp_cartesian_curve *cc, const QHash<LsMarker::Type, lp_texture*> &textures)
{
	if (!gc->visible) {
		return;
	}

	LsTableDataColumn *xcol = gc->x;
	LsTableDataColumn *ycol = gc->y;

	if (xcol == Q_NULLPTR || ycol == Q_NULLPTR) {
		return;
	}

	xcol->lock.lock();
	ycol->lock.lock();

	unsigned count = qMin(xcol->data_count, ycol->data_count);
	if (!count) {
		/* no data to show */
		xcol->lock.unlock();
		ycol->lock.unlock();
		return;
	}

	GLsizei stride = sizeof(float);

	if (count > MAX_GPU_SAMPLE_COUNT) {
		qDebug() << "gpu [soft] limit exceed " << count << ", max: " << MAX_GPU_SAMPLE_COUNT;
		unsigned div = ceil(count / (1.0 * MAX_GPU_SAMPLE_COUNT));
		count /= div;
		stride *= div;
		qDebug() << "skipping " << (div - 1) << " samples between points";
		qDebug() << "will now plot " << count << " samples";
	}

	qreal r, g, b, a;
	gc->color.getRgbF(&r, &g, &b, &a);

	lp_cartesian_curve_bool(cc, LP_CARTESIAN_CURVE_DOT_SHOW, false);

	lp_cartesian_curve_4float(cc,
		LP_CARTESIAN_CURVE_LINE_COLOR, r, g, b, a);
	lp_cartesian_curve_float(cc,
		LP_CARTESIAN_CURVE_LINE_WIDTH, gc->line_width);
	lp_cartesian_curve_bool(cc,
		LP_CARTESIAN_CURVE_LINE_SHOW, gc->line_show);

	lp_cartesian_curve_4float(cc,
		LP_CARTESIAN_CURVE_MARKER_COLOR, r, g, b, a);
	lp_cartesian_curve_bool(cc,
		LP_CARTESIAN_CURVE_MARKER_SHOW, gc->marker_show);
	lp_cartesian_curve_pointer(cc,
		LP_CARTESIAN_CURVE_MARKER_TEXTURE, textures.value(gc->marker));

	lp_cartesian_curve_data(cc, LP_CARTESIAN_CURVE_X, 0,
		GL_FLOAT, stride, xcol->data, count);

	lp_cartesian_curve_data(cc, LP_CARTESIAN_CURVE_Y, 0,
		GL_FLOAT, stride, ycol->data, count);

	lp_cartesian_draw_curve(c, cc);

	xcol->lock.unlock();
	ycol->lock.unlock();
}

/**
 * Calculate plot data
 * @param eq Equation
 * @param coef Coefficient
 * @param x X value to generate and store to
 * @param y Y values to generate and store to
 * @param xmin Starting X value
 * @param xmax Last X value
 * @param count Number of samples to generate
 * @return true if success
 */
static bool calc_fit_data(const QString &eq,
	const QMap<QString, qreal> &coef,
	float *x, float *y, qreal xmin, qreal xmax, int count)
{
	/* prepare the parser */
	LsParser parser;
	parser.setExpression(eq);

	/* define variables */
	double px;
	parser.defineVariable("x", &px);

	parser.defineConstants(coef);

	qreal dx = (xmax - xmin) / count;

	try {
		for (int i = 0; i < count; i++) {
			px = x[i] = xmin + (dx * i);
			y[i] = parser.Eval();
		}

		return true;
	} catch (mu::Parser::exception_type &e) {
		qWarning() << "MuParser failed: " << QString::fromStdString(e.GetMsg());
		return false;
	}
}

/**
 * Draw a subset of the fit data
 * This is used to draw dotted or line plot based on the region of interest.
 * @param cartesian libreplot cartesian
 * @param curve libreplot curve
 * @param x X data (WITHOUT offset added)
 * @param y Y data (WITHOUT offset added)
 * @param first First index to be draw in @a x and @a y
 * @param last Last index to be draw in @a x and @a y
 * @param line_show True to show line, False to show dots
 */
static inline void draw_fit_subset(lp_cartesian *cartesian,
	lp_cartesian_curve *curve, float *x, float *y,
	int first, int last, bool line_show)
{
	int count = last - first + 1;
	if (!count) {
		return;
	}

	lp_cartesian_curve_bool(curve, LP_CARTESIAN_CURVE_LINE_SHOW, line_show);
	lp_cartesian_curve_bool(curve, LP_CARTESIAN_CURVE_DOT_SHOW, !line_show);

	/* X (fitted) axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
		sizeof(float), &x[first], count);

	/* Y (fitted) axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
		sizeof(float), &y[first], count);

	/* do the draw */
	lp_cartesian_draw_curve(cartesian, curve);
}

/**
 * Draw fit data
 * @param c libreplot cartesian
 * @param cc libreplot cartesian curve
 * @param x X data (Fitting)
 * @param y Y data (Fitting)
 * @param COUNT number of points in @a x and @a y
 * @param xmin Range starting X value of interest
 * @param xmax Rabge ending X value of interest
 * @note Range @a xmin to @a xmax user is specifically interesting in
 * @return index of @a x and @a y that is [likely] the center point
 */
static int draw_fit_data(lp_cartesian *c, lp_cartesian_curve *cc,
	float *x, float *y, int COUNT, qreal xmin, qreal xmax)
{
	/* inital state of the curve */
	lp_cartesian_curve_bool(cc, LP_CARTESIAN_CURVE_MARKER_SHOW, false);
	lp_cartesian_curve_bool(cc, LP_CARTESIAN_CURVE_LINE_SHOW, false);
	lp_cartesian_curve_float(cc, LP_CARTESIAN_CURVE_LINE_WIDTH, 1);
	lp_cartesian_curve_4float(cc, LP_CARTESIAN_CURVE_LINE_COLOR, 0, 0, 0, 1);
	lp_cartesian_curve_bool(cc, LP_CARTESIAN_CURVE_DOT_SHOW, false);
	lp_cartesian_curve_float(cc, LP_CARTESIAN_CURVE_DOT_WIDTH, 1);
	lp_cartesian_curve_4float(cc, LP_CARTESIAN_CURVE_DOT_COLOR, 0, 0, 0, 1);

	ssize_t first, last;
	int center;
	if (lp_array_find_region(xmin, xmax, x, COUNT, &first, &last)) {
		draw_fit_subset(c, cc, x, y, 0, first, false);
		draw_fit_subset(c, cc, x, y, first, last, true);
		draw_fit_subset(c, cc, x, y, last, COUNT - 1, false);
		center = (first + last) / 2;
	} else {
		draw_fit_subset(c, cc, x, y, 0, COUNT - 1, false);
		center = COUNT / 2;
		qDebug() << "only showing outside data" << center;
	}

	/* calculate the best position that describe the data center
	 *  (possibly: center to the user requested fit data). */
	return center;
}

/**
 * Draw the fit line that connect the data center point
 *  and information box widget
 * @param c libreplot cartesian
 * @param l libreplot line
 * @param fit Fit information box
 * @param startx Data center X
 * @param starty Data center Y
 * @param GL_HEIGHT Height of OpenGL Area (convert screen to OpenGL coordinate)
 */
static void draw_fit_line(lp_cartesian *c, lp_line *l, LsGraphPlotFit *fit,
	float startx, float starty, int GL_HEIGHT)
{
	/* calculate the line end position
	 *  (center of the widget) */
	float endx, endy;
	QPoint end = fit->geometry().center();
	lp_cartesian_pixel_to_data_coord(c, end.x(),
		GL_HEIGHT - end.y(), /* top-left coord to top-bottom */
		&endx, &endy);

	/* draw the line */
	lp_line_2float(l, LP_LINE_START, startx, starty);
	lp_line_2float(l, LP_LINE_END, endx, endy);
	lp_cartesian_draw_line(c, l);
}

// in future, read axis max-min
// convert that to px to get actual px
// using that px, calculate the number of points
//
// for now, ignoring the drawn axis
static int calc_fit_num_points(int px, int xdpi)
{
	const int POINTS_PER_INCH = 50;
	return (px / (qreal) xdpi) * POINTS_PER_INCH;
}

void LsGraphPlot::paintGL()
{
	Cartesian::paintGL();

	lp_cartesian_draw_start(m_cartesian);

	for (int i = 0; i < m_curves.count(); i++) {
		draw_curve(m_cartesian, m_curves.at(i), m_curve, m_textures);
	}

	/* thing common for all fit */
	const int FIT_COUNT = calc_fit_num_points(width(), physicalDpiX());
	float fit_x[FIT_COUNT], fit_y[FIT_COUNT];
	float fit_xmin, fit_xmax;
	lp_get_cartesian_axis_2float(m_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_RANGE, &fit_xmin, &fit_xmax);

	int HEIGHT = height();
	m_fits.removeAll(0); /* remove all closed entries */
	for (int i = 0; i < m_fits.count(); i++) {
		LsGraphPlotFit *fit = m_fits.at(i);

		/* first try to calculate the data */
		if (!calc_fit_data(fit->equation(), fit->coef(),
					fit_x, fit_y, fit_xmin, fit_xmax, FIT_COUNT)) {
			continue;
		}

		/* find the region that is actually user requested for fitting */
		qreal user_xmin, user_xmax; /* The X range user actually selected */
		fit->xRange(&user_xmin, &user_xmax);

		/* draw the data */
		int center = draw_fit_data(m_cartesian, m_curve,
			fit_x, fit_y, FIT_COUNT, user_xmin, user_xmax);

		/* draw the line */
		draw_fit_line(m_cartesian, m_line,
			fit, fit_x[center], fit_y[center], HEIGHT);
	}

	if (m_fit.state != LsGraphPlot::DISABLED) {
		lp_line *line = m_fit.line;
		lp_rectangle *rect = m_fit.rect;
		qreal startx = m_fit.start.x();
		qreal starty = m_fit.start.y();
		qreal endx = m_fit.end.x();
		qreal endy = m_fit.end.y();

		/* draw the first line */
		if (!isnan(startx)) {
			lp_line_2float(line, LP_LINE_START, startx, -INFINITY);
			lp_line_2float(line, LP_LINE_END, startx, +INFINITY);
			lp_cartesian_draw_line(m_cartesian, line);
		}

		/* draw the second line */
		if (!isnan(endx)) {
			lp_line_2float(line, LP_LINE_START, endx, -INFINITY);
			lp_line_2float(line, LP_LINE_END, endx, +INFINITY);
			lp_cartesian_draw_line(m_cartesian, line);
		}

		/* draw the rectangle */
		if (!isnan(startx) && !isnan(starty) && !isnan(endx) && !isnan(endy)) {
			QRectF shape = QRectF(m_fit.start, m_fit.end).normalized();
			lp_rectangle_4float(rect, LP_RECTANGLE_GEOMETRY,
				shape.x(), shape.y(), shape.width(), shape.height());
			lp_cartesian_draw_rectangle(m_cartesian, rect);
		}
	}

	lp_cartesian_draw_end(m_cartesian);
}

void LsGraphPlot::initializeGL()
{
	Cartesian::initializeGL();

	/* curves */
	m_curve = lp_cartesian_curve_gen();
	m_line  = lp_line_gen();

	lp_line_4float(m_line, LP_LINE_COLOR, 0, 0, 0, 1);
	lp_line_float(m_line, LP_LINE_WIDTH, 2);

	/* curves marker */
	m_textures = LsMarker::buildTexture(this);

	/* fit rectangle */
	m_fit.rect = lp_rectangle_gen();
	lp_rectangle_bool(m_fit.rect, LP_RECTANGLE_LINE_SHOW, false);
	lp_rectangle_bool(m_fit.rect, LP_RECTANGLE_FILL_SHOW, true);
	lp_rectangle_4float(m_fit.rect, LP_RECTANGLE_FILL_COLOR, 0, 0, 0, 0.3);

	/* fit line */
	m_fit.line = lp_line_gen();
	lp_line_4float(m_fit.line, LP_LINE_COLOR, 0, 0, 0, 1);
	lp_line_float(m_fit.line, LP_LINE_WIDTH, 2);
}

/**
 * Called when user want to enable/disable curve fitting facility
 * @param enable Enable (true to enable)
 */
void LsGraphPlot::enableCurveFit(bool enable)
{
	m_fit.state = enable ? LsGraphPlot::ENABLED : LsGraphPlot::DISABLED;

	/* invalidate */
	m_fit.start = QPointF(NAN, NAN);
	m_fit.end = QPointF(NAN, NAN);

	/* get mouse move event even if no button is pressed */
	setMouseTracking(enable);

	update();
}

/**
 * The state machine in mousePressEvent, mouseReleaseEvent and mouseMoveEvent
 *  handle both type of selection:
 *  - press, move, release
 *  - press, release, move, press, release
 *
 * ENABLE --(move)----> ENABLE
 * ENABLE --(press)---> STATE1
 *
 * STATE1 --(move)----> STATE2
 * STATE1 --(release)-> STATE2
 *
 * STATE2 --(press)---> STATE2
 * STATE2 --(move)----> STATE2
 *
 * STATE2 --(release)-> ENABLE
 */

/**
 * Convert mouse @a e event coordnate to OpenGL pixel coordinate.
 *  and store the result to @a res
 * @param e Event
 * @param[out] res Result
 */
void LsGraphPlot::mouseFitCoord(QMouseEvent *e, QPointF *res)
{
	int x = e->x();

	/* mouse coordinate to pixel coordinate */
	int y = height() - e->y();

	float cx, cy;

	lp_cartesian_pixel_to_data_coord(m_cartesian, x, y, &cx, &cy);

	if (!isfinite(cx) || !isfinite(cy)) {
		cx = NAN;
		cy = NAN;
	}

	res->setX(cx);
	res->setY(cy);
}

void LsGraphPlot::mousePressEvent(QMouseEvent *e)
{
	switch (m_fit.state) {
	case LsGraphPlot::DISABLED:
		Cartesian::mousePressEvent(e);
	return;
	case LsGraphPlot::ENABLED:
		m_fit.state = LsGraphPlot::STATE1;
		mouseFitCoord(e, &m_fit.start);
	break;
	default:
	return;
	}

	update();
}

void LsGraphPlot::mouseReleaseEvent(QMouseEvent *e)
{
	switch (m_fit.state) {
	case LsGraphPlot::DISABLED:
		Cartesian::mouseReleaseEvent(e);
	return;
	case LsGraphPlot::STATE1:
		m_fit.state = LsGraphPlot::STATE2;
		mouseFitCoord(e, &m_fit.end);
	break;
	case LsGraphPlot::STATE2: {
		m_fit.state = LsGraphPlot::ENABLED;
		mouseFitCoord(e, &m_fit.end);

		/* we are done! */
		QRectF rect = QRectF(m_fit.start, m_fit.end);
		m_fit.start = QPointF(NAN, NAN);
		m_fit.end = QPointF(NAN, NAN);
		userWantToFit(rect.normalized());
	} break;
	default:
	return;
	}

	update();
}

/**
 * Update the coordinate of data based on mouse event @a e
 * @param e Mouse event (Move)
 */
void LsGraphPlot::updateDataCoord(QMouseEvent *e)
{
	if (m_coord_label == Q_NULLPTR) {
		return;
	}

	int x = e->x();
	int y = height() - e->y();
	float cx, cy;
	lp_cartesian_pixel_to_data_coord(m_cartesian, x, y, &cx, &cy);

	/* valid coordinates? */
	if (!isfinite(cx) || !isfinite(cy)) {
		m_coord_label->clear();
		return;
	}

	m_coord_label->setText(QString("%1, %2").arg(cx).arg(cy));
}

void LsGraphPlot::mouseMoveEvent(QMouseEvent *e)
{
	updateDataCoord(e);

	switch (m_fit.state) {
	case LsGraphPlot::DISABLED:
		Cartesian::mouseMoveEvent(e);
		return;
	break;
	case LsGraphPlot::ENABLED:
		mouseFitCoord(e, &m_fit.start);
	break;
	case LsGraphPlot::STATE1:
		m_fit.state = LsGraphPlot::STATE2;
		mouseFitCoord(e, &m_fit.end);
	break;
	case LsGraphPlot::STATE2:
		mouseFitCoord(e, &m_fit.end);
	break;
	default:
	return;
	}

	update();
}

/**
 * User want to fit @a crv with @a rect region of interest
 * @param crv Curve
 * @param rect Region
 */
void LsGraphPlot::userWantToFit(LsGraphCurve *crv, const QRectF &rect)
{
	/* find the start in curve data */
	LsTableDataColumn *xcol = crv->x;
	LsTableDataColumn *ycol = crv->y;

	/* we need both data */
	if (xcol == Q_NULLPTR || ycol == Q_NULLPTR) {
		return;
	}

	/* in this technique, we are assuming that the data is in increment order.
	 *  x is incrementing in with index incremention
	 */
	int first = -1, last = -1;

	for (int i = 0; i < qMin(xcol->data_count, ycol->data_count); i++) {
		qreal x = xcol->data[i];
		qreal y = ycol->data[i];

		/* data is valid? */
		if (isnan(x) || isnan(y)) {
			continue;
		}

		if (rect.contains(x, y)) {
			if (first == -1) {
				first = i;
			} else {
				last = i;
			}
		}
	}

	if (first ==-1 || last == -1) {
		QMessageBox::information(this, "Insufficient", "The provided curve do not have sufficient points (selected region too small?)");
		return;
	}

	LsGraphFit fit(crv, first, last, this);
	if (fit.exec() != QDialog::Accepted) {
		return;
	}

	/* append the item */
	LsGraphPlotFit *item = new LsGraphPlotFit(this);
	item->setTitle(fit.title());
	item->setEquation(fit.equation());
	item->setCoef(fit.coef());
	item->setXRange(xcol->data[first], xcol->data[last]);
	m_fits.append(item);
	connect(item, SIGNAL(compressed(bool)), SLOT(update()));
	item->show();

	Q_EMIT userDidFitting();
}

/**
 * Check if any data point of @a crv fall inside @a rect
 * @param rect Region to test
 * @param crv Curve
 * @return true if atleast on point fall inside
 * @param false if all points lie outside
 */
static bool curve_in_rect(const QRectF &rect, LsGraphCurve *crv)
{
	/* find the start in curve data */
	LsTableDataColumn *xcol = crv->x;
	LsTableDataColumn *ycol = crv->y;

	/* we need both data */
	if (xcol == Q_NULLPTR || ycol == Q_NULLPTR) {
		return false;
	}

	/* check each point contains */
	for (int i = 0; i < qMin(xcol->data_count, ycol->data_count); i++) {
		float x = xcol->data[i];
		float y = ycol->data[i];

		/* data is valid? */
		if (isnan(x) || isnan(x)) {
			continue;
		}

		if (rect.contains(x, y)) {
			return true;
		}
	}

	return false;
}

/**
 * Executed when user want to fit data on curve
 * @param rect Region of interest
 */
void LsGraphPlot::userWantToFit(const QRectF &rect)
{
	QList<LsGraphCurve*> curves;

	for (int i = 0; i < m_curves.count(); i++) {
		LsGraphCurve *crv = m_curves.at(i);

		/* only visible curve are possible to fit */
		if (crv->visible && curve_in_rect(rect, crv)) {
			curves.append(crv);
		}
	}

	int curves_count = curves.size();

	if (!curves_count) {
		showStatusMessage("No data to fit!", 3000);
		return;
	} else if (curves_count == 1) {
		userWantToFit(curves.at(0), rect);
		return;
	}

	/* show user a list of items to select from */
	LsSelectOneDialog sod(this);

	for (int i = 0; i < curves_count; i++) {
		sod.addEntry(curves.at(i)->title);
	}

	if (sod.exec() == QDialog::Accepted) {
		int i = sod.selected();
		userWantToFit(curves.at(i), rect);
	}
}

/**
 * When widget resized, move the internal fitting widget.
 * Move the children to visible area, so that do not get hidden/inaccessible.
 * @param event Event data
 */
void LsGraphPlot::resizeEvent(QResizeEvent *event)
{
	Cartesian::resizeEvent(event);

	m_fits.removeAll(0);

	QRect parent(QPoint(0, 0), event->size());

	for (int i = 0; i < m_fits.count(); i++) {
		LsGraphPlotFit *fit = m_fits.at(i);

		/* fully visible? */
		QRect child = fit->geometry();
		if (!parent.contains(child)) {
			if (child.right() > parent.right()) {
				child.moveRight(parent.right());
			}
			if (child.bottom() > parent.bottom()) {
				child.moveBottom(parent.bottom());
			}
			fit->setGeometry(child);
		}
	}
}
