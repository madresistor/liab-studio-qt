/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "thread.h"
#include <lmmin.h>
#include <lmstruct.h>
#include "extra/parser/parser.h"

using namespace std;

LsGraphFitThread::LsGraphFitThread(LsGraphCurve *crv, int first, int last,
			QWidget *parent) :
	QThread(parent),
	m_curve(crv),
	m_first(first),
	m_last(last)
{
}

void LsGraphFitThread::setMaxIterCount(int max_iter)
{
	m_max_iter = max_iter;
}

struct eval_data {
	struct {
		double *x;
		double *coef;
		uint coef_count;
	} var;

	LsParser *parser;
	LsGraphCurve *curve;
	int first, last;
	LsGraphFitThread *thread;
	double last_rmse;
};

static void copy_data(double *dest, const double *src, uint count)
{
	for (uint i = 0; i < count; i++) {
		dest[i] = src[i];
	}
}

static void parser_eval(const double *_param, const int m_data,
		const void *_data, double *fvec, int *user_break)
{
	struct eval_data *data = (struct eval_data *) _data;

	if (data->thread->isInterruptionRequested()) {
		/* stop processing. thread need to stop! */
		*user_break = -1;
		return;
	}

	/* copy the param to parser variable memory */
	copy_data(data->var.coef, _param, data->var.coef_count);

	LsTableDataColumn *xcol = data->curve->x;
	LsTableDataColumn *ycol = data->curve->y;
	int processed = 0;
	qreal se = 0; /* square error */

	try {
		for (int i = data->first; i <= data->last; i++) {
			float x = xcol->data[i];
			float y = ycol->data[i];

			if (isnan(x) || isnan(y)) {
				continue;
			}

			/* store back the difference from real values. */
			*data->var.x = x;
			qreal err = y - data->parser->Eval();
			fvec[processed++] = err;
			se = err * err;

			/* processed number of point as required */
			if (processed >= m_data) {
				break;
			}
		}

		data->last_rmse = sqrt(se / processed);
	} catch (mu::Parser::exception_type &e) {
		*user_break = -1;
		qWarning() << "MuParser failed: " << QString::fromStdString(e.GetMsg());
	}
}

/**
 * Read back the coefficients value
 * In the same sequence, update the values
 * @param coef Coefficients
 * @param mem Memory containing results
 * @param const_coef Coefficients to ignore
 */
static void readback_coef(QMap<QString, qreal> &coef, double *mem)
{
	size_t offset = 0;

	QMapIterator<QString, qreal> iter(coef);
	while (iter.hasNext()) {
		iter.next();
		coef[iter.key()] = mem[offset];
		offset++;
	}
}

/**
 * Calculate the number of points that are possible to fit.
 * @param curve Curve
 * @return number of points
 */
static uint count_fitable_points(LsGraphCurve *curve, int first, int last)
{
	LsTableDataColumn *xcol = curve->x;
	LsTableDataColumn *ycol = curve->y;
	uint result = 0;

	for (int i = first; i <= last; i++) {
		float x = xcol->data[i];
		float y = ycol->data[i];

		if (isfinite(x) && isfinite(y)) {
			result++;
		}
	}

	return result;
}

void LsGraphFitThread::run_fit_and_rmse()
{
	/* number of points to fit [once only require] */
	if (!m_fitable_points) {
		m_fitable_points = count_fitable_points(m_curve, m_first, m_last);
	}

	if (m_fitable_points < m_var_coef.count()) {
		m_succeded = false;
		m_rmse = NAN;
		m_failure_reason = "Insufficient number of points";
		return;
	}

	/* prepare the parser */
	LsParser parser;
	parser.setExpression(m_equation);

	/* define x */
	double x;
	parser.defineVariable("x", &x);

	/* define & read coefficients */
	parser.defineConstants(m_const_coef);
	uint VAR_COEF_COUNT = m_var_coef.count();
	double var_coef_value[VAR_COEF_COUNT];
	double var_coef0_value[VAR_COEF_COUNT];
	parser.defineVariables(m_var_coef, var_coef_value);

	/* inital state */
	copy_data(var_coef0_value, var_coef_value, VAR_COEF_COUNT);

	/* arguments passed to the lmfit callback */
	struct eval_data user_data;
	user_data.var.x = &x;
	user_data.var.coef = var_coef_value;
	user_data.var.coef_count = VAR_COEF_COUNT;
	user_data.curve = m_curve;
	user_data.first = m_first;
	user_data.last = m_last;
	user_data.parser = &parser;
	user_data.thread = this;
	user_data.last_rmse = NAN;

	/* prepare lmfit arguments */
	lm_control_struct control = lm_control_float;
	lm_status_struct status;
	control.verbosity = 0;
	control.patience = (VAR_COEF_COUNT + 1) * m_max_iter;

	/* fitting... */
	lmmin(VAR_COEF_COUNT, var_coef0_value, m_fitable_points, NULL,
		(const void *) &user_data, parser_eval, &control, &status);

	if (status.outcome >= 0) {
		/* success (status.nfev contain number of iteration done) */
		readback_coef(m_var_coef, var_coef0_value);
		m_rmse = user_data.last_rmse;
		m_succeded = true;
	} else {
		/* failure */
		const char *info = lm_infmsg[-status.outcome];
		m_succeded = false;
		m_rmse = NAN;
		m_failure_reason = QString::fromUtf8(info);
	}
}

void LsGraphFitThread::run_rmse()
{
	/* prepare the parser */
	LsParser parser;
	parser.setExpression(m_equation);

	/* define variables */
	double px;
	parser.defineVariable("x", &px);

	parser.defineConstants(m_const_coef);
	parser.defineConstants(m_var_coef);

	LsTableDataColumn *xcol = m_curve->x;
	LsTableDataColumn *ycol = m_curve->y;

	try {
		int N = 0;
		qreal se = 0; /* square error */

		for (int i = 0; i < qMin(xcol->data_count, ycol->data_count); i++) {
			float x = xcol->data[i];
			float y = ycol->data[i];

			if (!isfinite(x) || !isfinite(y)) {
				continue;
			}

			px = x;
			qreal fy = parser.Eval();

			qreal err = y - fy;
			se = err * err;
			N++;
		}

		m_rmse = sqrt(se / N);
		m_succeded = true;
	} catch (mu::Parser::exception_type &e) {
		m_succeded = false;
		m_failure_reason = QString::fromStdString(e.GetMsg());
	}
}

/**
 * @note Assumption: \n
 *  curve.{x, y} is not NULL \n
 *  curve.{x, y}.data_count is greater than 0 \n
 */
void LsGraphFitThread::run()
{
	if (m_var_coef.count()) {
		run_fit_and_rmse();
	} else {
		run_rmse();
	}
}
