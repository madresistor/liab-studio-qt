/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_FIT_EQUATION_H
#define LIAB_STUDIO_WIDGETS_GRAPH_FIT_EQUATION_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include "extra/parser/parser.h"

#include "ui_graph_fit_equation.h"

class LsGraphFitEquation : public QDialog
{
	Q_OBJECT

	public:
		LsGraphFitEquation(QWidget *parent = Q_NULLPTR);

		void setEquation(const QString &eq);
		void setDesc(const QString &desc);

		QString equation();
		QString desc();

	public Q_SLOTS:
		void done(int r);

	protected:
		bool equationValid();

	private:
		void setupVariable();

		Ui::LsGraphFitEquation ui;

		LsParser m_parser;

};

#endif
