/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "equation.h"

#include "extra/parser/parser.h"
#include "extra/parser/constant.h"
#include "extra/parser/function.h"
#include "extra/parser/operator.h"

LsGraphFitEquation::LsGraphFitEquation(QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	LsParserFunction *func = new LsParserFunction(ui.insertFunction);
	ui.insertFunction->setMenu(func);
	connect(func, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsFunction(const QString &)));

	LsParserOperator *oper = new LsParserOperator(ui.insertOperator);
	ui.insertOperator->setMenu(oper);
	connect(oper, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsOperator(const QString &)));

	LsParserConstant *constant = new LsParserConstant(ui.insertConstant);
	ui.insertConstant->setMenu(constant);
	connect(constant, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsConstant(const QString &)));

	setupVariable();
}

void LsGraphFitEquation::setupVariable()
{
	QMenu *variable = new QMenu(ui.insertVariable);

	LsParserMenu *data = new LsParserMenu(variable);
	data->addEntry("x", "X Axis");
	m_parser.defineConstant("x", 0);

	variable->addAction("Data")->setMenu(data);
	connect(data, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsVariable(const QString &)));

	/* Coefficients */
	LsParserMenu *coef = new LsParserMenu(variable);
	for (int i = 0; i < 26; i++) {
		QChar ch('A' + i);
		QAction *action = coef->addAction(ch);
		action->setData(ch);
		m_parser.defineConstant(ch, 0);
	}

	variable->addAction("Coefficients")->setMenu(coef);
	connect(coef, SIGNAL(userWantToInsert(const QString &)),
		ui.equation, SLOT(insertAsVariable(const QString &)));

	/* set to button */
	ui.insertVariable->setMenu(variable);
}

bool LsGraphFitEquation::equationValid()
{
	QString eq = equation();

	if (!eq.length()) {
		return false;
	}

	m_parser.setExpression(eq);

	/* try to evaluate the equation */
	try {
		m_parser.Eval();
	} catch (mu::Parser::exception_type &e) {
		QString msg = QString::fromStdString(e.GetMsg());
		ui.equationError->setText(msg);
		return false;
	}

	return true;
}

/**
 * Done when user want to close the dialog.
 * If equation input is valid, then equation is validated.
 * if the equation is invalid, the dialog is not closed.
 * @param r Result
 */
void LsGraphFitEquation::done(int r)
{
	if (r == QDialog::Accepted) {
		/* confirm that the equation is valid or empty */
		if (!equationValid()) {
			return;
		}
	}

	QDialog::done(r);
}

void LsGraphFitEquation::setEquation(const QString &eq)
{
	ui.equation->setText(eq);
}

void LsGraphFitEquation::setDesc(const QString &desc)
{
	ui.desc->setText(desc);
}

QString LsGraphFitEquation::equation()
{
	return ui.equation->text();
}

QString LsGraphFitEquation::desc()
{
	return ui.desc->text();
}
