/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_FIT_H
#define LIAB_STUDIO_WIDGETS_GRAPH_FIT_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QList>
#include <QMap>
#include "widgets/table/table.h"
#include "widgets/graph/curve.h"

#include "ui_graph_fit.h"

class LsGraphFitThread;

class LsGraphFit : public QDialog
{
	Q_OBJECT

	public:
		struct Item {
			public:
				bool valid;

				LsGraphCurve *curve;
				float *y;
				int y_first, y_last;
				QColor color;

				/* count of actual data point that can be fitted. */
				QString equation; /* fit equation */
				QMap<QString, qreal> coef; /* fit coefficients */
				qreal rmse; /* root mean square error */
		};

		LsGraphFit(LsGraphCurve *crv, int first, int last,
			QWidget *parent = Q_NULLPTR);
		~LsGraphFit();

		/* retrive the result */
		QString title() { return m_last_result.title; }
		QString equation() { return m_last_result.equation; }
		QMap<QString, qreal> coef() {
			QMap<QString, qreal> coef;
			coef.unite(m_last_result.var_coef);
			coef.unite(m_last_result.const_coef);
			return coef;
		}

	private Q_SLOTS:
		void add();
		void modify();
		void remove();

		void calc();
		void equationChanged();
		void userChangedCoef();
		void userPinnedCoef();

		void threadFinished();

	private:
		void loadList();
		void storeList();
		void loadExtra();
		void storeExtra();
		void autoCalc();
		void setFitStatus(const QString &text);

		void initCoef();
		void setCoefValue(const QMap<QString, qreal> &coef);
		void getCoefValue(const QStringList &used,
			QMap<QString, qreal> &var_coef, QMap<QString, qreal> &const_coef);
		void showOnlyCoef(const QStringList &coef);
		void setAllCoefToDefault();

		Ui::LsGraphFit ui;
		Item *m_item;
		LsGraphCurve *m_curve;
		int m_first, m_last;
		LsGraphFitThread *m_thread;
		bool global_ignore_coef_change = false;

		struct {
			QString title;
			QString equation;
			QMap<QString, qreal> var_coef, const_coef;
		} m_last_result;
};

#endif
