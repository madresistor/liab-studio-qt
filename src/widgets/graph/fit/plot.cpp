/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plot.h"
#include <cmath>
#include "extra/parser/parser.h"
#include "extra/sigproc/array.h"

using namespace std;

LsGraphFitPlot::LsGraphFitPlot(QWidget *parent) :
	Cartesian(parent)
{
}

LsGraphFitPlot::~LsGraphFitPlot()
{
	makeCurrent();
	lp_cartesian_curve_del(m_curve);
	lp_texture_del(m_marker);
	doneCurrent();
}

void LsGraphFitPlot::initializeGL()
{
	Cartesian::initializeGL();
	m_curve = lp_cartesian_curve_gen();
	if (m_target.curve != Q_NULLPTR) {
		m_marker = LsMarker::buildTexture(this, m_target.curve->marker);
	} else {
		qWarning() << "[plot] target curve is NULL";
	}
}

void LsGraphFitPlot::drawRealPoint()
{
	qreal r, g, b, a;
	LsGraphCurve *crv = m_target.curve;

	/* find the data */
	LsTableDataColumn *x = crv->x;
	LsTableDataColumn *y = crv->y;

	/* we need both data */
	if (x == Q_NULLPTR || y == Q_NULLPTR) {
		return;
	}

	uint offset = m_target.first;
	uint count = m_target.last - m_target.first + 1;

	lp_cartesian_draw_start(m_cartesian);

	/* drawing actual data */
	lp_cartesian_curve_bool(m_curve,
		LP_CARTESIAN_CURVE_DOT_SHOW, false);
	lp_cartesian_curve_bool(m_curve,
		LP_CARTESIAN_CURVE_MARKER_SHOW, crv->marker_show);
	lp_cartesian_curve_bool(m_curve,
		LP_CARTESIAN_CURVE_LINE_SHOW, crv->line_show);
	lp_cartesian_curve_pointer(m_curve,
		LP_CARTESIAN_CURVE_MARKER_TEXTURE, m_marker);
	lp_cartesian_curve_float(m_curve,
		LP_CARTESIAN_CURVE_LINE_WIDTH, crv->line_width);

	/* set color */
	crv->color.getRgbF(&r, &g, &b, &a);
	lp_cartesian_curve_4float(m_curve,
		LP_CARTESIAN_CURVE_MARKER_COLOR, r, g, b, a);
	lp_cartesian_curve_4float(m_curve,
		LP_CARTESIAN_CURVE_LINE_COLOR, r, g, b, a);

	/* X axis data */
	lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
		sizeof(float), &x->data[offset], count);

	/* Y (input) axis data */
	lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
		sizeof(float), &y->data[offset], count);

	/* do the draw */
	lp_cartesian_draw_curve(m_cartesian, m_curve);
}

/**
 * Calculate plot data
 * @param eq Equation
 * @param coef Coefficient
 * @param x X value to generate and store to
 * @param y Y values to generate and store to
 * @param xmin Starting X value
 * @param xmax Last X value
 * @param count Number of samples to generate
 * @return true if success
 */
static bool calc_plot_data(const QString &eq,
	const QMap<QString, qreal> &coef,
	float *x, float *y, qreal xmin, qreal xmax, int count)
{
	/* prepare the parser */
	LsParser parser;
	parser.setExpression(eq);

	/* define variables */
	double px;
	parser.defineVariable("x", &px);

	parser.defineConstants(coef);

	qreal dx = (xmax - xmin) / count;

	try {
		for (int i = 0; i < count; i++) {
			px = x[i] = xmin + (dx * i);
			y[i] = parser.Eval();
		}

		return true;
	} catch (mu::Parser::exception_type &e) {
		qWarning() << "MuParser failed: " << QString::fromStdString(e.GetMsg());
		return false;
	}
}

// in future, read axis max-min
// convert that to px to get actual px
// using that px, calculate the number of points
//
// for now, ignoring the drawn axis
static int calc_num_points(int px, int xdpi)
{
	const int POINTS_PER_INCH = 50;
	return (px / (qreal) xdpi) * POINTS_PER_INCH;
}

static inline void fit_point_draw_subset(lp_cartesian *cartesian,
	lp_cartesian_curve *curve, float *x, float *y,
	int first, int last, bool show_line)
{
	int count = last - first;
	if (!count) {
		return;
	}

	lp_cartesian_curve_bool(curve, LP_CARTESIAN_CURVE_LINE_SHOW, show_line);
	lp_cartesian_curve_bool(curve, LP_CARTESIAN_CURVE_DOT_SHOW, !show_line);

	/* X (fitted) axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_X, 0, GL_FLOAT,
		sizeof(float), &x[first], count);

	/* Y (fitted) axis data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_Y, 0, GL_FLOAT,
		sizeof(float), &y[first], count);

	/* do the draw */
	lp_cartesian_draw_curve(cartesian, curve);
}

void LsGraphFitPlot::drawFitPoint()
{
	/* drawing fit data */
	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_MARKER_SHOW, false);
	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_LINE_SHOW, false);
	lp_cartesian_curve_float(m_curve, LP_CARTESIAN_CURVE_LINE_WIDTH, 1);
	lp_cartesian_curve_4float(m_curve, LP_CARTESIAN_CURVE_LINE_COLOR, 0, 0, 0, 1);
	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_DOT_SHOW, false);
	lp_cartesian_curve_float(m_curve, LP_CARTESIAN_CURVE_DOT_WIDTH, 1);
	lp_cartesian_curve_4float(m_curve, LP_CARTESIAN_CURVE_DOT_COLOR, 0, 0, 0, 1);

	/* range of data we need to generate data */
	float min, max;
	lp_get_cartesian_axis_2float(m_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	/* calculate the data */
	const int COUNT = calc_num_points(width(), physicalDpiX());
	float x[COUNT], y[COUNT];

	if (!calc_plot_data(m_fit.equation, m_fit.coef, x, y, min, max, COUNT)) {
		return;
	}

	/* find the region that overlaps with real data */
	LsTableDataColumn *xcol = m_target.curve->x;
	min = xcol->data[m_target.first];
	max = xcol->data[m_target.last];

	ssize_t first, last;
	if (lp_array_find_region(min, max, x, COUNT, &first, &last)) {
		fit_point_draw_subset(m_cartesian, m_curve, x, y, 0, first, false);
		fit_point_draw_subset(m_cartesian, m_curve, x, y, first, last, true);
		fit_point_draw_subset(m_cartesian, m_curve, x, y, last, COUNT - 1, false);
	} else {
		fit_point_draw_subset(m_cartesian, m_curve, x, y, 0, COUNT - 1, false);
	}

}

void LsGraphFitPlot::paintGL()
{
	Cartesian::paintGL();

	if (m_target.curve == Q_NULLPTR) {
		return;
	}

	if (m_perform_best_fit) {
		calcBestFit();
		m_perform_best_fit = false;
	}

	drawRealPoint();

	if (!m_fit.equation.isEmpty()) {
		drawFitPoint();
	}

	lp_cartesian_draw_end(m_cartesian);
}

void LsGraphFitPlot::calcBestFit() const
{
	float xmin = +INFINITY, xmax = -INFINITY;
	float ymin = +INFINITY, ymax = -INFINITY;

	LsTableDataColumn *xcol = m_target.curve->x;
	LsTableDataColumn *ycol = m_target.curve->y;

	for (int i = m_target.first; i <= m_target.last; i++) {
		float x = xcol->data[i];
		float y = ycol->data[i];

		if (isfinite(x)) {
			xmin = fminf(xmin, x);
			xmax = fmaxf(xmax, x);
		}

		if (isfinite(y)) {
			ymin = fminf(ymin, y);
			ymax = fmaxf(ymax, y);
		}
	}

	const qreal side_space = 10; /* percentage */

	if (isfinite(xmin) && isfinite(xmax) && xmin < xmax) {
		float space = (xmax - xmin) * (side_space / 100);
		lp_cartesian_axis_2float(m_axis_bottom,
			LP_CARTESIAN_AXIS_VALUE_RANGE, xmin - space, xmax + space);
	}

	if (isfinite(ymin) && isfinite(ymax) && ymin < ymax) {
		float space = (ymax - ymin) * (side_space / 100);
		lp_cartesian_axis_2float(m_axis_left,
			LP_CARTESIAN_AXIS_VALUE_RANGE, ymin - space, ymax + space);
	}
}
