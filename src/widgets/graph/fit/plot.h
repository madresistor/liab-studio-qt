/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_FIT_PLOT_H
#define LIAB_STUDIO_WIDGETS_GRAPH_FIT_PLOT_H

#include <QObject>
#include <QWidget>
#include "extra/replot/cartesian.h"
#include "widgets/graph/curve.h"
#include "extra/marker/marker.h"

class LsGraphFitPlot : public Cartesian
{
	Q_OBJECT

	public:
		LsGraphFitPlot(QWidget *parent = Q_NULLPTR);
		~LsGraphFitPlot();

		void setTarget(LsGraphCurve *crv, int first, int last) {
			m_target.curve = crv;
			m_target.first = first;
			m_target.last = last;
			Cartesian::update();
		}

		void update(const QString &eq, const QMap<QString, qreal> &coef) {
			m_fit.equation = eq;
			m_fit.coef = coef;
			Cartesian::update();
		}

		void zoomFitBest() {
			m_perform_best_fit = true;
			Cartesian::update();
		}

	protected:
		void paintGL() Q_DECL_OVERRIDE;
		void initializeGL() Q_DECL_OVERRIDE;

	private:
		void calcBestFit() const;
		void drawRealPoint();
		void drawFitPoint();

		struct {
			LsGraphCurve *curve = Q_NULLPTR;
			int first, last;
		} m_target;

		struct {
			QString equation;
			QMap<QString, qreal> coef;
		} m_fit;

		lp_cartesian_curve *m_curve = NULL;
		lp_texture *m_marker = NULL;
		bool m_perform_best_fit = false;
};

#endif
