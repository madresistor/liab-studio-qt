/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_GRAPH_FIT_THREAD_H
#define LIAB_STUDIO_GRAPH_FIT_THREAD_H

#include <QWidget>
#include <QObject>
#include <QThread>
#include "fit.h"

class LsGraphFitThread : public QThread
{
	Q_OBJECT

	public:
		LsGraphFitThread(LsGraphCurve *crv, int first, int last, QWidget *parent);

		void start(const QString &eq,
				const QMap<QString, qreal> &var_coef,
				const QMap<QString, qreal> &const_coef) {
			m_equation = eq;
			m_var_coef = var_coef;
			m_const_coef = const_coef;
			QThread::start();
		}

		QString equation() { return m_equation; }
		QMap<QString, qreal> varCoef() { return m_var_coef; }
		QMap<QString, qreal> constCoef() { return m_const_coef; }
		qreal rmse() { return m_rmse; }

		bool succedded() { return m_succeded; }
		QString failureReason() { return m_failure_reason; }

	public Q_SLOTS:
		void setMaxIterCount(int max_iter);

	protected:
		void run();

	private:
		void run_fit_and_rmse();
		void run_rmse();

		LsGraphCurve *m_curve;
		int m_first, m_last;
		int m_fitable_points = 0;

		int m_max_iter = 100;
		QString m_equation;
		QMap<QString, qreal> m_var_coef, m_const_coef;
		qreal m_rmse;

		bool m_succeded = false;
		QString m_failure_reason;
};

#endif
