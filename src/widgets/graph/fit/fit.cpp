/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fit.h"
#include "equation.h"
#include "thread.h"
#include <QTableWidgetItem>
#include <QDoubleSpinBox>
#include "extra/delegate/double_spin_box.h"
#include "extra/parser/parser.h"

static const QString GROUP_ID = "curve-fit";
static const QString LIST_ID = "list";
static const QString EQ_LIST_ID = "eq";
static const QString DESC_LIST_ID = "desc";
static const QString COEF_ID = "coef";
static const QString AUTO_UPDATE_ID = "auto-update";
static const QString MAX_ITER_ID = "max-iter";
static const QString EQ_INDEX_ID = "eq-index";

static const double COEF_DEFAULT_VALUE = 1;

/**
 * Check if the coef name is valid
 * @param name Name
 * @return true if valid
 */
static inline bool is_coef_name(const QString &name)
{
	return name.length() == 1 && name[0] >= 'A' && name[0] <= 'Z';
}

/**
 * Convert @a i to coef name
 * @param i Index
 * @param Name
 */
static inline QString to_coef_name(int i)
{
	return QChar('A' + i);
}

/**
 * Get the list of coef in the equation
 * @param eq Equation
 * @return List of string of coef
 */
static inline QStringList get_used_coef_names(const QString &eq)
{
	QStringList list;

	LsParser parser;
	parser.setExpression(eq);

	mu::varmap_type vars = parser.GetUsedVar();
	mu::varmap_type::const_iterator item;
	for (item = vars.begin(); item != vars.end(); item++) {
		QString name = QString::fromStdString(item->first);
		if (is_coef_name(name)) {
			list.append(name);
		}
	}

	return list;
}

/**
 * Extract the Integer number of coef
 * @param name Name
 * @param Number
 */
static inline int from_coef_name(const QString &name)
{
	return name[0].cell() - 'A';
}

struct ListItem {
	public:
		QString eq;
		QString desc;
		ListItem(QString q, QString d) : eq(q), desc(d) {}
};

static const QList<ListItem> INTERNAL_LIST = QList<ListItem>()
	<< ListItem("A*x", "Proportional")
	<< ListItem("A*x + B", "Linear (1°)")
	<< ListItem("A*x^2 + B*x + C", "Quadratic (2°)")
	<< ListItem("A + B*x + C*x^2 + D*x^3", "Cubic (3°)")
	<< ListItem("A + B*x + C*x^2 + D*x^3 + E*x^4", "Quartic (4°)")
	<< ListItem("A + B*x + C*x^2 + D*x^3 + E*x^4 + F*x^5", "Quintic (5°)")
	<< ListItem("A/x", "Inverse")
	<< ListItem("A/x^2", "Inverse square")
	<< ListItem("A*x^B + C", "Power")
	<< ListItem("A*exp(-C*x) + B", "Natural Exponent")
	<< ListItem("A*(1 - exp(-C*x)) + B", "Inverse Exponent")
	<< ListItem("A*ln(B*x)", "Natural Logarithm")
	<< ListItem("A*10^(B*x)", "Exponent ― Base 10 ")
	<< ListItem("A*log(B*x)", "Logarithm ― Base 10")
	<< ListItem("A*sin(B*x + C) + D", "Sine")
	<< ListItem("A*cos(B*x + C)^2 + D", "Cosine Squared")
	<< ListItem("A*exp(-(x-B)^2/(C^2)) + D", "Gaussian")
	<< ListItem("1/(sqrt(2*PI)*S) * exp(-(x-M)^2/(2*S^2))", "Normalized Gaussian")
;

LsGraphFit::LsGraphFit(LsGraphCurve *crv, int first, int last, QWidget *parent) :
	QDialog(parent),
	m_curve(crv),
	m_first(first),
	m_last(last)
{
	ui.setupUi(this);

	/* plot related */
	ui.plot->setTarget(crv, first, last);
	ui.plot->zoomFitBest();

	/* channel related */
	connect(ui.add, SIGNAL(clicked()), SLOT(add()));
	connect(ui.modify, SIGNAL(clicked()), SLOT(modify()));
	connect(ui.remove, SIGNAL(clicked()), SLOT(remove()));
	connect(ui.list, SIGNAL(doubleClicked(const QModelIndex &)), SLOT(modify()));

	m_thread = new LsGraphFitThread(crv, first, last, this);
	connect(m_thread, SIGNAL(finished()), SLOT(threadFinished()));

	connect(ui.try_eq, SIGNAL(clicked()), SLOT(calc()));

	/* prepare the inputs */
	loadList();
	initCoef();

	loadExtra();

	/* max iteration */
	connect(ui.maxIter, SIGNAL(valueChanged(int)),
		m_thread, SLOT(setMaxIterCount(int)));
	m_thread->setMaxIterCount(ui.maxIter->value());

	/* handle inputs */
	connect(ui.list, SIGNAL(itemSelectionChanged()), SLOT(equationChanged()));

	/* perform first time fit */
	equationChanged();

	/* "Insert" text look more intutive then "Ok" */
	QPushButton *insertBtn = ui.buttonBox->button(QDialogButtonBox::Ok);
	insertBtn->setText("Insert");
	insertBtn->setEnabled(false);
}

LsGraphFit::~LsGraphFit()
{
	/* stop any previous work */
	if (m_thread->isRunning()) {
		m_thread->requestInterruption();
		m_thread->wait();
	}

	storeList();

	storeExtra();
}

void LsGraphFit::loadExtra()
{
	/* load config */
	QSettings settings;
	settings.beginGroup(GROUP_ID);

	if (settings.contains(AUTO_UPDATE_ID)) {
		ui.autoUpdate->setChecked(settings.value(AUTO_UPDATE_ID).toBool());
	}

	if (settings.contains(MAX_ITER_ID)) {
		ui.maxIter->setValue(settings.value(MAX_ITER_ID).toInt());
	}

	if (settings.contains(EQ_INDEX_ID)) {
		int row = settings.value(EQ_INDEX_ID).toInt();
		if (row >= 0 && ui.list->rowCount()) {
			ui.list->selectRow(row);
		}
	}

	settings.endGroup();
}

void LsGraphFit::storeExtra()
{
	QSettings settings;
	settings.beginGroup(GROUP_ID);

	settings.setValue(AUTO_UPDATE_ID, ui.autoUpdate->isChecked());

	settings.setValue(MAX_ITER_ID, ui.maxIter->value());

	int row = ui.list->selectedRow();
	if (row != -1) {
		settings.setValue(EQ_INDEX_ID, row);
	}

	settings.endGroup();
}

static inline void set_in_list(QTableWidget *t, int r, const QString &eq,
	const QString &desc)
{
	t->setItem(r, 0, new QTableWidgetItem(eq));
	t->setItem(r, 1, new QTableWidgetItem(desc));
}

void LsGraphFit::loadList()
{
	int INTERNAL_LIST_SIZE = INTERNAL_LIST.size();

	ui.list->setRowCount(INTERNAL_LIST_SIZE);
	for (int i = 0; i < INTERNAL_LIST_SIZE; i++) {
		const QString &eq = INTERNAL_LIST.at(i).eq;
		const QString &desc = INTERNAL_LIST.at(i).desc;
		set_in_list(ui.list, i, eq, desc);
	}

	/* read user provided list */
	QSettings settings;
	settings.beginGroup(GROUP_ID);
	int size = settings.beginReadArray(LIST_ID);

	/* store some standard know equations */
	if (size > 0) {
		ui.list->setRowCount(INTERNAL_LIST_SIZE + size);
		for (int i = 0; i < size; i++) {
			settings.setArrayIndex(i);
			QString eq = settings.value(EQ_LIST_ID).toString();
			QString desc = settings.value(DESC_LIST_ID).toString();
			set_in_list(ui.list, INTERNAL_LIST_SIZE + i, eq, desc);
		}
	}

	settings.endArray();
	settings.endGroup();

	ui.list->selectRow(0);
	ui.list->resizeColumnsToContents();
	ui.list->horizontalHeader()->setStretchLastSection(true);
}

void LsGraphFit::storeList()
{
	int INTERNAL_LIST_SIZE = INTERNAL_LIST.size();
	int size = ui.list->rowCount() - INTERNAL_LIST_SIZE;

	QSettings settings;
	settings.beginGroup(GROUP_ID);
	settings.beginWriteArray(LIST_ID, size);

	/* write back the data */
	for (int i = 0; i < size; i++) {
		int row = INTERNAL_LIST_SIZE + i;
		QString eq = ui.list->item(row, 0)->text();
		QString desc = ui.list->item(row, 1)->text();

		settings.setArrayIndex(i);
		settings.setValue(EQ_LIST_ID, eq);
		settings.setValue(DESC_LIST_ID, desc);
	}

	settings.endArray();
	settings.endGroup();
}

void LsGraphFit::initCoef()
{
	global_ignore_coef_change = true;

	ui.coef->setRowCount(26);
	ui.coef->setColumnCount(2);

	for (int i = 0; i < 26; i++) {
		QString name = to_coef_name(i);
		ui.coef->setVerticalHeaderItem(i, new QTableWidgetItem(name));

		QDoubleSpinBox *dsp = new QDoubleSpinBox(ui.coef);
		dsp->setRange(-INFINITY, +INFINITY);
		dsp->setValue(COEF_DEFAULT_VALUE);
		dsp->setDecimals(3);
		dsp->setSingleStep(0.1);
		ui.coef->setCellWidget(i, 0, dsp);
		connect(dsp, SIGNAL(valueChanged(double)), SLOT(userChangedCoef()));

		QCheckBox *chk = new QCheckBox(ui.coef);
		chk->setFocusPolicy(Qt::NoFocus);
		chk->setToolTip("Mark the Coefficient as constant");
		ui.coef->setCellWidget(i, 1, chk);
		connect(chk, SIGNAL(toggled(bool)), SLOT(userPinnedCoef()));
	}

	global_ignore_coef_change = false;
}

void LsGraphFit::getCoefValue(const QStringList &coef_names,
	QMap<QString, qreal> &var_coef, QMap<QString, qreal> &const_coef)
{
	Q_FOREACH(const QString &name, coef_names) {
		int row = from_coef_name(name);
		QWidget *w = ui.coef->cellWidget(row, 0);
		double value = qobject_cast<QDoubleSpinBox*>(w)->value();
		w = ui.coef->cellWidget(row, 1);
		bool pinned = qobject_cast<QCheckBox*>(w)->isChecked();
		if (pinned) {
			const_coef[name] = value;
		} else {
			var_coef[name] = value;
		}
	}
}

void LsGraphFit::add()
{
	LsGraphFitEquation a;
	if (a.exec() == QDialog::Accepted) {
		int index = ui.list->rowCount();
		ui.list->setRowCount(index + 1);
		set_in_list(ui.list, index, a.equation(), a.desc());
		ui.list->resizeColumnsToContents();
		ui.list->horizontalHeader()->setStretchLastSection(true);
	}
}

void LsGraphFit::modify()
{
	int index = ui.list->selectedRow();
	if (index == -1 || index < INTERNAL_LIST.size()) {
		return;
	}

	LsGraphFitEquation m;
	m.setWindowTitle("Modify");
	QString eq = ui.list->item(index, 0)->text();
	QString desc = ui.list->item(index, 1)->text();
	m.setEquation(eq);
	m.setDesc(desc);

	if (m.exec() == QDialog::Accepted) {
		set_in_list(ui.list, index, m.equation(), m.desc());
		ui.list->resizeColumnsToContents();
		ui.list->horizontalHeader()->setStretchLastSection(true);
	}
}

void LsGraphFit::remove()
{
	int index = ui.list->selectedRow();
	if (index == -1 || index < INTERNAL_LIST.size()) {
		return;
	}

	ui.list->removeRow(index);
	ui.list->selectRow(qMax(index - 1, 0));
	ui.list->resizeColumnsToContents();
	ui.list->horizontalHeader()->setStretchLastSection(true);
}

void LsGraphFit::setCoefValue(const QMap<QString, qreal> &coef)
{
	/* fitting was performed, update the coef */
	global_ignore_coef_change = true;

	/* update table coefficients */
	QMapIterator<QString, double> iter(coef);
	while (iter.hasNext()) {
		iter.next();
		QString n = iter.key();
		double v = iter.value();
		int row = from_coef_name(n);
		QWidget *w = ui.coef->cellWidget(row, 0);
		qobject_cast<QDoubleSpinBox*>(w)->setValue(v);
	}

	global_ignore_coef_change = false;
}

void LsGraphFit::setFitStatus(const QString &text)
{
	ui.status->setText(QString("Status: %1").arg(text));
}

void LsGraphFit::calc()
{
	/* stop any previous work */
	if (m_thread->isRunning()) {
		m_thread->requestInterruption();
		m_thread->wait();
	}

	int index = ui.list->selectedRow();
	if (index == -1) {
		setFitStatus("Unable to proceed, no equation selected");
		return;
	}

	QString eq = ui.list->item(index, 0)->text();
	QStringList coef_used = get_used_coef_names(eq);
	QMap<QString, qreal> var_coef, const_coef;
	getCoefValue(coef_used, var_coef, const_coef);

	m_last_result.title = ui.list->item(index, 1)->text();
	m_last_result.equation = eq;
	m_last_result.var_coef = var_coef;
	m_last_result.const_coef = const_coef;

	/* first calculate coef And then update plot */
	m_thread->start(eq, var_coef, const_coef);
	setFitStatus("Started");
}

/**
 * Perform calculation if autoMode enabled
 */
void LsGraphFit::autoCalc()
{
	if (ui.autoUpdate->isChecked()) {
		calc();
	}
}

void LsGraphFit::threadFinished()
{
	bool success = m_thread->succedded();

	/* we have some result? */
	QPushButton *insertBtn = ui.buttonBox->button(QDialogButtonBox::Ok);
	insertBtn->setEnabled(success);

	if (!success) {
		/* hide graph from user */
		QMap<QString, qreal> EMPTY;
		ui.plot->update(QString::null, EMPTY);
		QString reason = m_thread->failureReason();
		setFitStatus(QString("Failed ― %1").arg(reason));
		return;
	}

	QString eq = m_thread->equation();
	QMap<QString, qreal> var_coef = m_thread->varCoef();
	QMap<QString, qreal> const_coef = m_thread->constCoef();
	double rmse = m_thread->rmse();

	m_last_result.var_coef = var_coef;
	setCoefValue(var_coef);

	QMap<QString, qreal> coef;
	coef.unite(var_coef);
	coef.unite(const_coef);
	ui.plot->update(eq, coef);

	setFitStatus(QString("Finished (RMSE = %1)").arg(rmse));
}

/**
 * Only show those coef input whose name exists in @a coef
 */
void LsGraphFit::showOnlyCoef(const QStringList &coef)
{
	global_ignore_coef_change = true;

	for (int i = 0; i < 26; i++) {
		if (coef.indexOf(to_coef_name(i)) == -1) {
			ui.coef->hideRow(i);
		} else {
			ui.coef->showRow(i);
		}
	}

	global_ignore_coef_change = false;
}

void LsGraphFit::setAllCoefToDefault()
{
	global_ignore_coef_change = true;

	for (int i = 0; i < 26; i++) {
		QWidget *w = ui.coef->cellWidget(i, 0);
		qobject_cast<QDoubleSpinBox*>(w)->setValue(COEF_DEFAULT_VALUE);

		w = ui.coef->cellWidget(i, 1);
		qobject_cast<QCheckBox*>(w)->setChecked(false);
	}

	global_ignore_coef_change = false;
}

void LsGraphFit::equationChanged()
{
	int index = ui.list->selectedRow();
	if (index == -1) {
		return;
	}

	/* disable modify/remove for internal list */
	bool internal_list = (index < INTERNAL_LIST.size());
	ui.modify->setDisabled(internal_list);
	ui.remove->setDisabled(internal_list);

	QString eq = ui.list->item(index, 0)->text();
	QStringList coef = get_used_coef_names(eq);
	showOnlyCoef(coef);
	setAllCoefToDefault();

	autoCalc();
}

void LsGraphFit::userPinnedCoef()
{
	if (global_ignore_coef_change) {
		return;
	}

	autoCalc();
}

void LsGraphFit::userChangedCoef()
{
	if (global_ignore_coef_change) {
		return;
	}

	QObject *w = QObject::sender();
	QDoubleSpinBox *dsp = qobject_cast<QDoubleSpinBox*>(w);
	QCheckBox *chk = Q_NULLPTR;

	for (int i = 0; i < 26; i++) {
		QWidget *_w = ui.coef->cellWidget(i, 0);
		QDoubleSpinBox *_dsp = qobject_cast<QDoubleSpinBox*>(_w);
		if (dsp == _dsp) {
			/* mark the item as constant (since it is provided by user) */
			QWidget *__w = ui.coef->cellWidget(i, 1);
			chk = qobject_cast<QCheckBox*>(__w);
			break;
		}
	}

	if (chk == Q_NULLPTR) {
		qWarning() << "Checkbox not found!";
		return;
	}

	if (chk->isChecked()) {
		autoCalc();
		return;
	}

	chk->setChecked(true);
}
