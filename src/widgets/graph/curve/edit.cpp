/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "edit.h"

#include <QPushButton>
#include <QAbstractItemModel>

#include "widgets/table/table.h"
#include "extra/marker/model.h"
#include "table_model.h"
#include "table_tree.h"

#include <libreplot/libreplot.h>

LsGraphCurveEdit::LsGraphCurveEdit(LsGraphCurve *crv, QWidget *parent) :
	QDialog(parent),
	m_curve(crv)
{
	ui.setupUi(this);

	LsMarkerModel *marker_model = new LsMarkerModel();
	ui.cmbMarker->setModel(marker_model);

	QSize iconSize(24, 24);
	ui.cmbMarker->setIconSize(iconSize);

	/* replicate to ui elements */
	ui.title->setText(crv->title);
	ui.spinWidth->setValue(crv->line_width);
	ui.chkWidth->setChecked(crv->line_show);
	ui.btnColor->setColor(crv->color);

	ui.cmbMarker->setCurrentIndex(static_cast<int>(crv->marker));
	ui.chkMarker->setChecked(crv->marker_show);
}

int LsGraphCurveEdit::exec()
{
	int r = QDialog::exec();
	if (r == QDialog::Accepted) {
		m_curve->title = ui.title->text();
		m_curve->line_width = ui.spinWidth->value();
		m_curve->line_show = ui.chkWidth->isChecked();
		m_curve->color = ui.btnColor->color();

		int marker = ui.cmbMarker->currentData().toInt();
		m_curve->marker = static_cast<LsMarker::Type>(marker);
		m_curve->marker_show = ui.chkMarker->isChecked();
	}

	return r;
}
