/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "list_model.h"
#include "widgets/graph/curve.h"
#include <QDebug>

#include "extra/marker/model.h"

LsGraphCurveListModel::LsGraphCurveListModel(QList<QPointer<LsGraphCurve>> *curves,
		QObject *parent) :
	QAbstractTableModel(parent),
	m_curves(curves)
{
}

int LsGraphCurveListModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 1;
}

int LsGraphCurveListModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return m_curves->count();
}

QVariant LsGraphCurveListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}

	LsGraphCurve *curve = m_curves->value(index.row());
	if (curve == NULL) {
		return QVariant();
	}

	switch(role) {
	case Qt::DisplayRole:
	return QVariant(curve->title);
	case Qt::ForegroundRole:
	return QVariant(curve->color);
	case Qt::DecorationRole:
	return curve->marker_show ?
		QVariant(LsMarker::valueToIcon(curve->marker)) :
		QVariant();
	case Qt::CheckStateRole:
	return QVariant(curve->visible ? Qt::Checked : Qt::Unchecked);
	default:
	break;
	}

	return QVariant();
}


bool LsGraphCurveListModel::setData(const QModelIndex &index,
			const QVariant &value, int role)
{
	if (!index.isValid()) {
		return false;
	}

	LsGraphCurve *curve = m_curves->at(index.row());
	if (curve == NULL) {
		return false;
	}

	if (role != Qt::CheckStateRole) {
		return false;
	}

	curve->visible = value.toBool();

	QVector<int> roles;
	roles << role;
	dataChanged(index, index, roles);

	return true;
}

Qt::ItemFlags LsGraphCurveListModel::flags(const QModelIndex &index) const
{
	Q_UNUSED(index);
	return Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
}

QVariant LsGraphCurveListModel::headerData(int section,
		Qt::Orientation orientation, int role) const
{
	Q_UNUSED(section);
	Q_UNUSED(orientation);
	Q_UNUSED(role);
	return QVariant();
}

void LsGraphCurveListModel::fakeReset()
{
	beginResetModel();
	endResetModel();
}
