/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "add.h"

#include <QPushButton>
#include <QAbstractItemModel>

#include "widgets/table/table.h"
#include "extra/marker/model.h"
#include "table_model.h"
#include "table_tree.h"

LsGraphCurveAdd::LsGraphCurveAdd(LsBank *bank, QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);
	ui.buttonBox->button(QDialogButtonBox::Ok)->setText("Add");

	tables = bank->tables();

	LsGraphCurveTableModel *xModel = new LsGraphCurveTableModel(tables, this);
	LsGraphCurveTableModel *yModel = new LsGraphCurveTableModel(tables, this);

	ui.xAxis->setModel(xModel);
	ui.yAxis->setModel(yModel);

	ui.xAxis->setCurrentIndex(xModel->index(0, 0, xModel->index(0, 0)));
	ui.yAxis->setCurrentIndex(yModel->index(1, 0, yModel->index(0, 0)));

	connect(ui.xAxis->selectionModel(),
			SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
			this, SLOT(autoFillTitle()));
	connect(ui.yAxis->selectionModel(),
			SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
			this, SLOT(autoFillTitle()));

	connect(ui.yAxis->selectionModel(),
			SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
			this, SLOT(updateColor()));

	ui.xAxis->expandAll();
	ui.yAxis->expandAll();

	for (int i = 0; i < 3; i++) {
		ui.xAxis->resizeColumnToContents(i);
		ui.yAxis->resizeColumnToContents(i);
	}

	ui.cmbMarker->setModel(new LsMarkerModel());

	QSize iconSize(24, 24);
	ui.cmbMarker->setIconSize(iconSize);
}

int LsGraphCurveAdd::exec()
{
	autoFillTitle();
	updateColor();
	return QDialog::exec();
}

void LsGraphCurveAdd::autoFillTitle()
{
	QModelIndex xIndex = ui.xAxis->currentIndex();
	QModelIndex yIndex = ui.yAxis->currentIndex();

	if (xIndex.isValid() && yIndex.isValid()) {
		LsGraphCurveTableTreeColumn *x = static_cast<LsGraphCurveTableTreeColumn *>(xIndex.internalPointer());
		LsGraphCurveTableTreeColumn *y = static_cast<LsGraphCurveTableTreeColumn *>(yIndex.internalPointer());
		ui.title->setText(y->title() +  " vs " + x->title());
	}
}

QString LsGraphCurveAdd::title()
{
	return ui.title->text();
}

static LsGraphCurveAdd::AxisInfo extractAxis(QModelIndex index)
{
	LsGraphCurveTableTreeColumn *row = static_cast<LsGraphCurveTableTreeColumn *>(index.internalPointer());
	LsGraphCurveTableTreeTable *table = static_cast<LsGraphCurveTableTreeTable *>(row->parent());

	LsTableData *tableData = static_cast<LsTable *>(table->pointer())->tableData;
	LsTableDataColumn *column = static_cast<LsTableDataColumn *>(row->pointer());

	return LsGraphCurveAdd::AxisInfo(tableData, column);
}

LsGraphCurveAdd::AxisInfo LsGraphCurveAdd::x()
{
	return extractAxis(ui.xAxis->currentIndex());
}

LsGraphCurveAdd::AxisInfo LsGraphCurveAdd::y()
{
	return extractAxis(ui.yAxis->currentIndex());
}

bool LsGraphCurveAdd::lineWidthEnabled()
{
	return ui.chkWidth->isChecked();
}

qreal LsGraphCurveAdd::lineWidth()
{
	return ui.spinWidth->value();
}

QColor LsGraphCurveAdd::color()
{
	return ui.btnColor->color();
}

bool LsGraphCurveAdd::markerEnabled()
{
	return ui.chkMarker->isChecked();
}

LsMarker::Type LsGraphCurveAdd::marker()
{
	return (LsMarker::Type) ui.cmbMarker->currentData().toInt();
}

void LsGraphCurveAdd::updateColor()
{
	QModelIndex yIndex = ui.yAxis->currentIndex();
	if (yIndex.isValid()) {
		LsGraphCurveTableTreeColumn *y = static_cast<LsGraphCurveTableTreeColumn *>(yIndex.internalPointer());
		QColor color = y->color();
		ui.btnColor->setColor(color);
	}
}
