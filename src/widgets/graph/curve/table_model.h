/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_CURVE_TABLE_MODEL_H
#define LIAB_STUDIO_WIDGETS_GRAPH_CURVE_TABLE_MODEL_H

#include <QObject>
#include <QWidget>
#include <QAbstractItemModel>
#include "widgets/table/table.h"
#include "widgets/table/table_data.h"

#include "table_tree.h"

class LsGraphCurveTableModel : public QAbstractItemModel
{
	Q_OBJECT

	public:
		LsGraphCurveTableModel(const QList<QPointer<LsTable>> &tbls,
			QObject *parent = Q_NULLPTR);
		~LsGraphCurveTableModel();

		int columnCount(const QModelIndex &parent = QModelIndex()) const;
		int rowCount(const QModelIndex &parent = QModelIndex()) const;
		QVariant data(const QModelIndex &index, int role) const;
		Qt::ItemFlags flags(const QModelIndex &index) const;
		QVariant headerData(int section, Qt::Orientation orientation,
				int role = Qt::DisplayRole) const;
		QModelIndex index(int row, int column,
				const QModelIndex &parent = QModelIndex()) const;
		QModelIndex parent(const QModelIndex & index) const;

	private:
		LsGraphCurveTableTreeRoot *root;
};

#endif
