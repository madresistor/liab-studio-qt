/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "table_tree.h"

/*
 *  this is used to simulate a tree of design:
 *
 * LsGraphCurveTableTreeRoot (-> LsGraphCurveTableTree)
 * 		|
 * 		+--- LsGraphCurveTableTreeTable (-> LsGraphCurveTableTree)
 * 				|
 * 				+----- LsGraphCurveTableTreeColumn (-> LsGraphCurveTableTree)
 */

/* --- column --- */

LsGraphCurveTableTreeColumn::LsGraphCurveTableTreeColumn(LsGraphCurveTableTreeTable *parent, LsTableDataColumn *col) :
	LsGraphCurveTableTree(),
	m_parent(parent),
	m_column(col)
{
}

QVariant LsGraphCurveTableTreeColumn::data(int column, int role)
{
	if (role == Qt::ForegroundRole) {
		return m_column->color;
	} else if (role == Qt::DisplayRole) {
		switch(column) {
		case 0:
		return m_column->long_name;

		case 1:
		return m_column->short_name;

		case 2:
		return m_column->unit;
		}
	}

	return QVariant();
}

/* --- table --- */

LsGraphCurveTableTreeTable::LsGraphCurveTableTreeTable(LsGraphCurveTableTreeRoot *parent, LsTable *table) :
	LsGraphCurveTableTree (),
	m_parent(parent),
	m_table(table)
{
	for (int i = 0; i < table->tableData->columns.size(); i++) {
		LsTableDataColumn *col = table->tableData->columns.at(i);
		m_columns.append(new LsGraphCurveTableTreeColumn(this, col));
	}
}

/* LsGraphCurveTableTreeColumn* */ LsGraphCurveTableTree* LsGraphCurveTableTreeTable::child(int index)
{
	Q_ASSERT(index >= 0 && index < m_columns.size());
	return m_columns.at(index);
}

QVariant LsGraphCurveTableTreeTable::data(int column, int role)
{
	return (column == 0 && role == Qt::DisplayRole) ?
		m_table->title() : QVariant();
}

/* --- root --- */

LsGraphCurveTableTreeRoot::LsGraphCurveTableTreeRoot(
	const QList<QPointer<LsTable>> &tables) :
	LsGraphCurveTableTree()
{
	for (int i = 0; i < tables.size(); i++) {
		m_tables.append(new LsGraphCurveTableTreeTable(this, tables.at(i)));
	}
}

/* LsGraphCurveTableTreeTable* */ LsGraphCurveTableTree* LsGraphCurveTableTreeRoot::child(int index)
{
	Q_ASSERT(index >= 0 && index < m_tables.size());
	return m_tables.at(index);
}

const static QVariant headers[] = {"Name", "Short name", "Unit"};

/* return header data
 * header moved here because it was convient to manage */
QVariant LsGraphCurveTableTreeRoot::data(int column, int role)
{
	if (role == Qt::DisplayRole && column >= 0 && column < 3) {
		return headers[column];
	}

	return QVariant();
}
