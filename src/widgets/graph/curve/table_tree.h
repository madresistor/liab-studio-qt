/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_CURVE_TABLE_TREE_H
#define LIAB_STUDIO_WIDGETS_GRAPH_CURVE_TABLE_TREE_H

#include <QVariant>
#include <Qt>

#include "widgets/table/table.h"
#include "widgets/table/table_data.h"

/* the base class */
class LsGraphCurveTableTree
{
	public:
		LsGraphCurveTableTree() = default;
		virtual ~LsGraphCurveTableTree() = default;

		virtual Qt::ItemFlags flags() { return Qt::NoItemFlags; }
		virtual QVariant data(int column, int role) = 0;
		virtual int columnCount() = 0;

		virtual LsGraphCurveTableTree* child(int num) { Q_UNUSED(num); return Q_NULLPTR; }
		virtual int childCount() { return 0; }
		virtual int childIndexOf(LsGraphCurveTableTree *child) { Q_UNUSED(child); return -1; }
		int row() { return parent()->childIndexOf(this); }

		virtual LsGraphCurveTableTree* parent() { return Q_NULLPTR; }
		virtual void* pointer() { return Q_NULLPTR; }
};

class LsGraphCurveTableTreeTable;
class LsGraphCurveTableTreeColumn : public LsGraphCurveTableTree
{
	public:
		LsGraphCurveTableTreeColumn(LsGraphCurveTableTreeTable *parent, LsTableDataColumn *col);
		int columnCount() { return 3; }
		QVariant data(int column, int role);
		/* LsGraphCurveTableTreeTable* */ LsGraphCurveTableTree* parent() { return (LsGraphCurveTableTree *)(m_parent); }
		/* LsTableDataColumn* */ void* pointer() { return m_column; }
		Qt::ItemFlags flags() { return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemNeverHasChildren; }
		QString title() { return m_column->long_name; }
		QColor color() { return m_column->color; }

	private:
		LsGraphCurveTableTreeTable *m_parent;
		LsTableDataColumn *m_column;
};

class LsGraphCurveTableTreeRoot;
class LsGraphCurveTableTreeTable : public LsGraphCurveTableTree
{
	public:
		LsGraphCurveTableTreeTable(LsGraphCurveTableTreeRoot *parent, LsTable *table);
		~LsGraphCurveTableTreeTable() { qDeleteAll(m_columns); }

		int columnCount() { return 3; }
		int childCount() { return m_columns.size(); }
		/* LsGraphCurveTableTreeRoot* */ LsGraphCurveTableTree* parent() { return (LsGraphCurveTableTree *)(m_parent); }
		/* LsTable* */ void* pointer() { return m_table; }
		/* LsGraphCurveTableTreeColumn* */ LsGraphCurveTableTree* child(int index);
		int childIndexOf(LsGraphCurveTableTree *child) { return m_columns.indexOf(static_cast<LsGraphCurveTableTreeColumn *>(child)); }
		QVariant data(int column, int role);

	private:
		LsGraphCurveTableTreeRoot *m_parent;

		LsTable *m_table;
		QList<LsGraphCurveTableTreeColumn *> m_columns;
};

class LsGraphCurveTableTreeRoot : public LsGraphCurveTableTree
{
	public:
		LsGraphCurveTableTreeRoot(const QList<QPointer<LsTable>> &tables);
		~LsGraphCurveTableTreeRoot() { qDeleteAll(m_tables); }

		int childCount() { return m_tables.size(); }
		int columnCount() { return 3; }
		/* LsGraphCurveTableTreeTable* */ LsGraphCurveTableTree* child(int index);
		QVariant data(int column, int role);
		int childIndexOf(LsGraphCurveTableTree *child) { return m_tables.indexOf(static_cast<LsGraphCurveTableTreeTable *>(child)); }

	private:
		QList<LsGraphCurveTableTreeTable *> m_tables;
};

#endif
