/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_CURVE_ADD_H
#define LIAB_STUDIO_WIDGETS_GRAPH_CURVE_ADD_H

#include <QObject>
#include <QDialog>
#include <QWidget>

#include "bank.h"
#include "widgets/table/table_data.h"
#include "widgets/graph/curve.h"

#include "ui_graph_curve_add.h"

class LsGraphCurveAdd : public QDialog
{
	Q_OBJECT

	public:
		LsGraphCurveAdd(LsBank *bank, QWidget *parent = Q_NULLPTR);

		typedef QPair<LsTableData*, LsTableDataColumn*> AxisInfo;

		QString title();
		qreal lineWidth();
		bool lineWidthEnabled();
		QColor color();
		LsMarker::Type marker();
		bool markerEnabled();
		AxisInfo x();
		AxisInfo y();

		int exec();

	private Q_SLOTS:
		void autoFillTitle();
		void updateColor();

	private:
		Ui::LsGraphCurveAdd ui;
		QList<QPointer<LsTable>> tables;
};

#endif
