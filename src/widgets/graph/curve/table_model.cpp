/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "table_model.h"
#include "table_tree.h"

LsGraphCurveTableModel::LsGraphCurveTableModel(
		const QList<QPointer<LsTable>> &tables, QObject *parent) :
	QAbstractItemModel(parent),
	root(new LsGraphCurveTableTreeRoot(tables))
{
}

LsGraphCurveTableModel::~LsGraphCurveTableModel()
{
	delete root;
}

int LsGraphCurveTableModel::columnCount(const QModelIndex &parent) const
{
	LsGraphCurveTableTree *parentItem = root;
	if (parent.isValid()) {
		parentItem = static_cast<LsGraphCurveTableTree*>(parent.internalPointer());
	}

	return parentItem->columnCount();
}

int LsGraphCurveTableModel::rowCount(const QModelIndex &parent) const
{
	LsGraphCurveTableTree *parentItem = root;
	if (parent.isValid()) {
		parentItem = static_cast<LsGraphCurveTableTree*>(parent.internalPointer());
	}

	return parentItem->childCount();
}

QVariant LsGraphCurveTableModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}

	LsGraphCurveTableTree *item = static_cast<LsGraphCurveTableTree*>(index.internalPointer());
	return item->data(index.column(), role);
}

Qt::ItemFlags LsGraphCurveTableModel::flags(const QModelIndex &index) const
{
	LsGraphCurveTableTree *item = root;
	if (index.isValid()) {
		item = static_cast<LsGraphCurveTableTree*>(index.internalPointer());
	}

	return item->flags();
}

QVariant LsGraphCurveTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	return (orientation == Qt::Horizontal) ?
		root->data(section, role) :
		QVariant();
}

QModelIndex LsGraphCurveTableModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	LsGraphCurveTableTree *parentItem = root;

	if (parent.isValid()) {
		parentItem = static_cast<LsGraphCurveTableTree*>(parent.internalPointer());
	}

	LsGraphCurveTableTree *childItem = parentItem->child(row);
	if (childItem) {
		return createIndex(row, column, childItem);
	} else {
		return QModelIndex();
	}
}

QModelIndex LsGraphCurveTableModel::parent(const QModelIndex & index) const
{
	if (!index.isValid()) {
		return QModelIndex();
	}

	LsGraphCurveTableTree *childItem =
		static_cast<LsGraphCurveTableTree*>(index.internalPointer());
	LsGraphCurveTableTree *parentItem = childItem->parent();

	if (parentItem == root) {
		return QModelIndex();
	}

	return createIndex(parentItem->row(), 0, parentItem);
}
