/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_CURVE_LIST_MODEL_H
#define LIAB_STUDIO_WIDGETS_GRAPH_CURVE_LIST_MODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QAbstractItemView>
#include "widgets/graph/plot.h"
#include "bank.h"

class LsGraphCurveListModel : public QAbstractTableModel
{
	Q_OBJECT

	public:
		LsGraphCurveListModel(QList<QPointer<LsGraphCurve>> *curve,
			QObject *parent = Q_NULLPTR);

		int columnCount(const QModelIndex &parent = QModelIndex()) const;
		int rowCount(const QModelIndex &parent = QModelIndex()) const;
		QVariant data(const QModelIndex &index, int role) const;
		Qt::ItemFlags flags(const QModelIndex &index) const;
		QVariant headerData(int section, Qt::Orientation orientation,
			int role = Qt::DisplayRole) const;
		bool setData(const QModelIndex & index, const QVariant & value,
			int role = Qt::EditRole);

		void fakeReset();

	private:
		QList<QPointer<LsGraphCurve>> *m_curves;
};

#endif
