/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_H
#define LIAB_STUDIO_WIDGETS_GRAPH_H

#include <QObject>
#include <QWidget>
#include <QVector>
#include "widgets/widget/widget.h"
#include "curve.h"
#include "curve/list_model.h"
#include "extra/marker/marker.h"
#include <QRect>

class LsTable;
class LsTableData;
class LsTableDataColumn;

#include "ui_graph.h"

class LsGraph : public LsWidget
{
	Q_OBJECT

	public:
		LsGraph(LsBank *bank, QWidget *parent = Q_NULLPTR);

		void addCurve(const QString &title,
				LsTable *table_x, int col_x,
				LsTable *table_y, int col_y,
				const QColor &color,
				bool line_show = true,
				qreal lineWidth = LsGraphCurve::DEF_LINE_WIDTH,
				bool marker_show = false,
				LsMarker::Type marker = LsMarker::DEFAULT);

		void addCurve(const QString &title,
				LsTableData *tableDataX, LsTableDataColumn *columnX,
				LsTableData *tableDataY, LsTableDataColumn *columnY,
				const QColor &color, bool line_show = true,
				qreal lineWidth = LsGraphCurve::DEF_LINE_WIDTH,
				bool marker_show = false,
				LsMarker::Type marker = LsMarker::DEFAULT);

		void addCurve(const QString &title,
				LsTableDataColumn *x, LsTableDataColumn *y,
				const QColor &color,
				bool line_show = false,
				qreal lineWidth = LsGraphCurve::DEF_LINE_WIDTH,
				bool marker_show = false,
				LsMarker::Type = LsMarker::DEFAULT);

	public Q_SLOTS:
		void save();
		void addCurve();
		void copyToClipboard();
		void update();

	private Q_SLOTS:
		void editCurve();
		void removeCurve();
		void userDidFitting();

	private:
		Ui::LsGraph ui;
		LsGraphCurveListModel *curves_model;
};

#endif
