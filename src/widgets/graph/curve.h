/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_CURVE_H
#define LIAB_STUDIO_WIDGETS_GRAPH_CURVE_H

#include <QObject>
#include <QVector>
#include <QColor>
#include <QString>
#include <QPointer>
#include "extra/replot/cartesian.h"
#include "widgets/table/table_data.h"
#include "extra/marker/marker.h"

class LsGraphCurve : public QObject
{
	Q_OBJECT

	public:
		QPointer<LsTableDataColumn> x = Q_NULLPTR, y = Q_NULLPTR;
		QString title = QString::null;
		bool visible = false;
		QColor color;
		bool line_show = true;
		qreal line_width = DEF_LINE_WIDTH;
		bool marker_show = false;
		LsMarker::Type marker = LsMarker::DEFAULT;

		static constexpr qreal DEF_LINE_WIDTH = 2.0;
};

#endif
