/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_GRAPH_PLOT_FIT_H
#define LIAB_STUDIO_GRAPH_PLOT_FIT_H

#include <QObject>
#include <QWidget>
#include "extra/draggable_widget/draggable_widget.h"
#include <QMap>
#include <QRect>

#include "ui_graph_plot_fit.h"

class LsGraphPlotFit : public LsDraggableWidget
{
	Q_OBJECT

	Q_PROPERTY(bool compress READ isCompressed NOTIFY compressed);

	public:
		LsGraphPlotFit(QWidget *parent = Q_NULLPTR);


		bool isCompressed() { return m_compress; }
		QString equation() { return m_equation; }
		QMap<QString, qreal> coef() { return m_coef; }
		void xRange(qreal *start, qreal *end) {
			*start = m_xrange.start;
			*end = m_xrange.end;
		}

	public Q_SLOTS:
		void setTitle(const QString &title);
		void setEquation(const QString &eq);
		void setCoef(const QMap<QString, qreal> &coef);
		void setXRange(qreal start, qreal end);

	private Q_SLOTS:
		void compressClicked();

	Q_SIGNALS:
		void compressed(bool yes);

	private:
		void buildDesc();

		Ui::LsGraphPlotFit ui;
		QString m_equation;
		QMap<QString, qreal> m_coef;
		struct {
			qreal start, end;
		} m_xrange;
		bool m_compress = false;
};

#endif
