/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plot_fit.h"

LsGraphPlotFit::LsGraphPlotFit(QWidget *parent) :
	LsDraggableWidget(parent)
{
	ui.setupUi(this);

	connect(ui.compress, SIGNAL(clicked()), SLOT(compressClicked()));
	connect(ui.close, SIGNAL(clicked()), SLOT(deleteLater()));
}

void LsGraphPlotFit::setTitle(const QString &title)
{
	ui.title->setText(title);
	ui.title->adjustSize();
	adjustSize();
}

void LsGraphPlotFit::setEquation(const QString &eq)
{
	m_equation = eq;
	buildDesc();
}

void LsGraphPlotFit::setCoef(const QMap<QString, qreal> &coef)
{
	m_coef = coef;
	buildDesc();
}

void LsGraphPlotFit::setXRange(qreal start, qreal end)
{
	m_xrange.start = start;
	m_xrange.end = end;
}

void LsGraphPlotFit::buildDesc()
{
	QString desc;
	desc.append(m_equation);

	QMapIterator<QString,qreal> i(m_coef);
	while (i.hasNext()) {
		i.next();
		desc.append(QString("\n  %1 = %2").arg(i.key()).arg(i.value()));
	}

	ui.desc->setText(desc);
	adjustSize();
}

void LsGraphPlotFit::compressClicked()
{
	m_compress = !m_compress;
	ui.desc->setVisible(!m_compress);
	QString id = m_compress ? "arrow-out" : "arrow-in";
	QString file = QString(":/curve-fit/%1").arg(id);
	ui.compress->setIcon(QIcon(file));
	adjustSize();
	Q_EMIT compressed(m_compress);
}
