/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "graph.h"
#include <QMessageBox>
#include <QDateTime>
#include <Qt>
#include <QGridLayout>
#include <cmath>

#include <QLabel>
#include <QAction>
#include <QMenu>
#include <QPixmap>
#include <QFileDialog>
#include "widgets/table/table_data.h"
#include "widgets/table/table.h"

#include "curve.h"

#include "curve/add.h"
#include "curve/edit.h"

using namespace std;

LsGraph::LsGraph(LsBank *bank, QWidget *parent) :
	LsWidget(bank, parent)
{
	ui.setupUi(this);

	curves_model = new LsGraphCurveListModel(&ui.bind->m_curves, this);
	ui.curves->setModel(curves_model);

	/* update graph when model changes */
	connect(curves_model,
		SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)),
		ui.bind, SLOT(update()));

	/* update graph when model rest */
	connect(curves_model, SIGNAL(modelReset()), ui.bind, SLOT(update()));

	/* when user double click a item, edit dialog is shown */
	connect(ui.curves, SIGNAL(doubleClicked(const QModelIndex &)),
				SLOT(editCurve()));

	/* when user click "edit", edit dialog is shown */
	connect(ui.edit, SIGNAL(clicked()), SLOT(editCurve()));

	/* when user click remove, remove the curve */
	connect(ui.remove, SIGNAL(clicked()), SLOT(removeCurve()));

	connect(ui.actionSave, SIGNAL(triggered()), this, SLOT(save()));
	connect(ui.actionAdd, SIGNAL(triggered()), this, SLOT(addCurve()));
	connect(ui.add, SIGNAL(clicked()), this, SLOT(addCurve()));
	connect(ui.actionZoomFitBest, SIGNAL(triggered()),
				ui.bind, SLOT(zoomFitBest()));
	connect(ui.actionZoomIn, SIGNAL(triggered()), ui.bind, SLOT(zoomIn()));
	connect(ui.actionZoomOut, SIGNAL(triggered()), ui.bind, SLOT(zoomOut()));
	connect(ui.actionCopy, SIGNAL(triggered()), this, SLOT(copyToClipboard()));

	/* edit title facility */
	ui.title->setText(title());
	connect(ui.title, SIGNAL(userWantToChange(const QString &)),
		SLOT(setTitle(const QString &)));
	connect(this, SIGNAL(titleChanged(const QString &)),
		ui.title, SLOT(setText(const QString &)));

	/* curve fitting enable goes to plot for enabling user interaction */
	connect(ui.actionCurveFit, SIGNAL(toggled(bool)),
		ui.bind, SLOT(enableCurveFit(bool)));
	connect(ui.bind, SIGNAL(userDidFitting()), SLOT(userDidFitting()));

	/* update graph when curve model is reset */
	connect(curves_model, SIGNAL(modelReset()), ui.bind, SLOT(update()));

	/* show the coordinate of the point under cursor */
	QLabel *coordLabel = new QLabel(ui.bind);
	statusBar()->addPermanentWidget(coordLabel);
	ui.bind->setCoordLabel(coordLabel);

	/* pass plot status messages to status bar */
	connect(ui.bind, SIGNAL(showStatusMessage(const QString &, int)),
		ui.statusBar, SLOT(showMessage(const QString &, int)));
}

/**
 * Capture and save the graph to disk
 */
void LsGraph::save()
{
	QPixmap qPix = ui.centralwidget->grab();
	if (qPix.isNull()){
		qDebug("Failed to capture the plot for saving");
		return;
	}

	QString types(
		"PNG file (*.png);;"
		"SVG file (*.svg);;"
		"JPEG file (*.jpeg *.jpg);;"
		"Bitmap file (*.bmp)"
	);

	QStringList extList;
	extList << "png";
	extList << "svg";
	extList << "jpeg";
	extList << "bmp";
	extList << "jpg"; /* extra */

	QString path = QString("%1/%2.png").arg(pictureDir()).arg(title());

	QString selectedFilter;
	QString dialogTitle = tr("Save Graph as Image");
	QString filename = QFileDialog::getSaveFileName(this, dialogTitle, path,
		types, &selectedFilter);

	/* do we need to save? */
	if (filename.isEmpty()) {
		return;
	}

	if (QFileInfo(filename).suffix().isEmpty() /* no ext */
		or extList.indexOf(QFileInfo(filename).suffix()) < 0 /* unknown ext */ ) {
		filename += "." + extList.at(types.split(";;").indexOf(selectedFilter));
	}

	qDebug() << "save to" << filename;

	/* save it
	 * TODO: ask user if for quality (the 100 value)
	 * 		their could be a setting where user can specify the level of quality while saving
	 */
	qPix.save(filename, 0, 100);
}

void LsGraph::addCurve()
{
	if (!m_bank->hasTables()) {
		qDebug() << "tables list is empty";
		return;
	}

	LsGraphCurveAdd dialog(m_bank, this);

	if (dialog.exec() != QDialog::Accepted) {
		return;
	}

	LsGraphCurveAdd::AxisInfo x = dialog.x();
	LsGraphCurveAdd::AxisInfo y = dialog.y();

	addCurve(dialog.title(),
			x.first, x.second, y.first, y.second, dialog.color(),
			dialog.lineWidthEnabled(), dialog.lineWidth(),
			dialog.markerEnabled(), dialog.marker());

	ui.bind->zoomFitBest();
}

void LsGraph::addCurve(const QString &title,
		LsTable *table_x, int col_x,
		LsTable *table_y, int col_y,
		const QColor &color, bool line_show, qreal line_width,
		bool marker_show, LsMarker::Type marker)
{
	addCurve(title, table_x->tableData, table_x->tableData->columns.at(col_x),
			table_y->tableData, table_y->tableData->columns.at(col_y),
			color, line_show, line_width, marker_show, marker);
}

void LsGraph::addCurve(const QString &title,
		LsTableData *tableDataX, LsTableDataColumn *columnX,
		LsTableData *tableDataY, LsTableDataColumn *columnY,
		const QColor &color, bool line_show, qreal line_width,
		bool marker_show, LsMarker::Type marker)
{
	addCurve(title, columnX, columnY, color, line_show, line_width,
		marker_show, marker);

	/* X connect */
	connect(tableDataX, SIGNAL(modelReset()),
		ui.bind, SLOT(update()), Qt::UniqueConnection);
	connect(tableDataX,
		SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &, const QVector<int> &)),
		ui.bind, SLOT(update()), Qt::UniqueConnection);

	/* Y connect */
	connect(tableDataY, SIGNAL(modelReset()),
		ui.bind, SLOT(update()), Qt::UniqueConnection);
	connect(tableDataY,
		SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &, const QVector<int> &)),
		ui.bind, SLOT(update()), Qt::UniqueConnection);

	/* tell that we have update curves */
	curves_model->fakeReset();
}

void LsGraph::addCurve(const QString &title,
		LsTableDataColumn *x, LsTableDataColumn *y,
		const QColor &color, bool line_show, qreal line_width,
		bool marker_show, LsMarker::Type marker)
{
	LsGraphCurve *c = new LsGraphCurve();

	c->title = title;
	c->x = x;
	c->y = y;
	c->color = color;
	c->visible = true;
	c->line_show = line_show;
	c->line_width = line_width;
	c->marker_show = marker_show;
	c->marker = marker;

	ui.bind->m_curves.append(c);
}

/**
 * Capture and copy the image to clipboard
 */
void LsGraph::copyToClipboard()
{
	QApplication::clipboard()->setPixmap(ui.bind->grab());
	statusBar()->showMessage(tr("Graph image copied to clipboard."), 2000);
}

void LsGraph::update()
{
	ui.bind->update();
}

void LsGraph::editCurve()
{
	int row = ui.curves->selectedRow();
	if (row == -1) {
		return;
	}

	LsGraphCurve *curve = ui.bind->m_curves.at(row);

	if (curve == Q_NULLPTR) {
		qDebug() << "curve is NULL, cannot show edit dialog";
		return;
	}

	LsGraphCurveEdit edit(curve, 0);
	if (edit.exec()) {
		/* tell that we have update curves */
		curves_model->fakeReset();
	}
}

void LsGraph::removeCurve()
{
	int row = ui.curves->selectedRow();
	if (row == -1) {
		return;
	}

	ui.bind->m_curves.removeAt(row);
	curves_model->fakeReset();
	ui.curves->selectRow(row - (row > 0 ? 1 : 0));
}

void LsGraph::userDidFitting()
{
	ui.actionCurveFit->setChecked(false);
}
