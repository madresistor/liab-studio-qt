/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_GRAPH_PLOT_H
#define LIAB_STUDIO_WIDGETS_GRAPH_PLOT_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include "extra/replot/cartesian.h"
#include <libreplot/cartesian/curve.h>
#include <libreplot/extra/rectangle.h>
#include <libreplot/extra/line.h>
#include "curve.h"
#include "plot_fit.h"
#include <QPoint>

class LsTableDataColumn;

class LsGraphPlot : public Cartesian
{
	Q_OBJECT

	public:
		LsGraphPlot(QWidget *parent = Q_NULLPTR);
		~LsGraphPlot();

		void setCoordLabel(QLabel *label);

	public Q_SLOTS:
		void zoomFitBest();
		void enableCurveFit(bool);

	Q_SIGNALS:
		void userDidFitting();
		void showStatusMessage(const QString &message, int timeout = 0);

	protected Q_SLOTS:
		void userWantToFit(const QRectF &rect);
		void userWantToFit(LsGraphCurve *crv, const QRectF &rect);

	protected:
		void paintGL() Q_DECL_OVERRIDE;
		void initializeGL() Q_DECL_OVERRIDE;

		void mousePressEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
		void mouseReleaseEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
		void mouseMoveEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
		void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

	public:
		QList<QPointer<LsGraphCurve>> m_curves;

	private:
		void mouseFitCoord(QMouseEvent *e, QPointF *res);
		void updateDataCoord(QMouseEvent *e);

		QList<QPointer<LsGraphPlotFit>> m_fits;
		lp_cartesian_curve *m_curve;
		lp_line *m_line;

		QHash<LsMarker::Type, lp_texture*> m_textures;

		enum MouseFitInputState {
			DISABLED,
			ENABLED,
			STATE1,
			STATE2
		};

		/* curve fitting */
		struct {
			enum MouseFitInputState state;
			lp_rectangle *rect;
			lp_line *line;
			QPointF start, end;
		} m_fit;

		QLabel *m_coord_label = Q_NULLPTR;

};

#endif
