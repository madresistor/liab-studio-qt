/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bank.h"
#include <QMdiSubWindow>

#include "main_window.h"

#include "widgets/widget/widget.h"
#include "widgets/table/table.h"
#include "widgets/import_table/import_table.h"
#include "widgets/graph/graph.h"
#include "widgets/guage/guage.h"
#include "widgets/manometer/manometer.h"
#include "widgets/note/note.h"
#include "widgets/text/text.h"
#include "widgets/thermometer/thermometer.h"

/**
 * LsBank act as a interface between LsMainWindow and Widget's/Interface's
 * All Widget's/Interface's share a common instance of LsBank that they can used
 *  to show widgets to user.
 *
 * It abstract out the work of adding+show widgets to user for Widgets/Interface
 */

LsBank::LsBank(LsMainWindow *parent) :
	QObject(parent),
	window(parent)
{
}

/**
 * Return a list of tables that are currently available to user
 * @return list of table
 */
QList<QPointer<LsTable>> LsBank::tables()
{
	tablesCollection.removeAll(0);
	return tablesCollection;
}

/**
 * Add a table
 * @param def_col add X and Y column if true
 */
LsTable* LsBank::addTable()
{
	LsTable *widget = new LsTable(this, window);
	addWidget(widget, 230, 480);
	tablesCollection.append(widget);

	QString title = QString("Table %1").arg(inc.table++);
	widget->setTitle(title);

	return widget;
}

/**
 * Add a table widget
 * @param filePath file to read and show the data in it
 * @return a table widget instance
 */
LsTable* LsBank::addTable(const QString &filePath)
{
	LsTable *widget = new LsImportTable(filePath, this, window);
	addWidget(widget, 230, 480);
	tablesCollection.append(widget);

	/* show filename (without extension) as title */
	QString title = QFileInfo(filePath).baseName();
	widget->setTitle(title);

	return widget;
}

/**
 * Add a graph widget
 * @return a graph widget instance
 */
LsGraph* LsBank::addGraph()
{
	LsGraph *widget = new LsGraph(this, window);
	addWidget(widget, 786, 480);

	QString title = QString("Graph %1").arg(inc.graph++);
	widget->setTitle(title);

	return widget;
}

/**
 * Add a guage widget
 * @todo guage widget support
 * @return a guage widget instance
 */
LsGuage* LsBank::addGuage()
{
	LsGuage *widget = new LsGuage(this, window);
	addWidget(widget, 220, 240);
	return widget;
}

/**
 * Add a manometer widget
 * @todo manometer widget support
 * @return a manometer widget instance
 */
LsManometer* LsBank::addManometer()
{
	LsManometer *widget = new LsManometer(this, window);
	addWidget(widget, 230, 480);
	return widget;
}

/**
 * Add a note widget
 * @return a note widget instance
 */
LsNote* LsBank::addNote()
{
	LsNote *widget = new LsNote(this, window);
	addWidget(widget, 640, 480);
	return widget;
}

/**
 * Add a text widget
 * @todo text widget support
 * @return a text widget instance
 */
LsText* LsBank::addText()
{
	LsText *widget = new LsText(this, window);
	addWidget(widget, 240, 160);
	return widget;
}

/**
 * Add a thermometer widget
 * @todo thermometer widget support
 * @return a thermometer widget instance
 */
LsThermometer* LsBank::addThermometer()
{
	LsThermometer *widget = new LsThermometer(this, window);
	addWidget(widget, 160, 320);
	return widget;
}

/**
 * Add @a widget to widget list
 * @param widget Widget to add
 * @param width Width of widget
 * @param height Height of widget
 */
void LsBank::addWidget(LsWidget *widget, int width, int height)
{
	window->addWidgetToMdi(widget, width, height);
}

/**
 * Check if bank has tables.
 * @return true if contains atleast one table
 */
bool LsBank::hasTables()
{
	tablesCollection.removeAll(0);
	return !tablesCollection.isEmpty();
}

/**
 * Remove @a widget
 * @param widget Widget to remove
 */
void LsBank::removeWidget(LsWidget *widget)
{
	window->removeWidgetFromMdi(widget);
}
