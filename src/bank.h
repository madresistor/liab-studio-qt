/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_COLLECTION_H
#define LIAB_STUDIO_COLLECTION_H

#include <QList>
#include <QPointer>
#include <QObject>
#include <QWidget>

class LsMainWindow;
class LsWidget;
class LsTable;
class LsGraph;
class LsGuage;
class LsManometer;
class LsNote;
class LsText;
class LsThermometer;

class LsBank : public QObject
{
	Q_OBJECT

	public:
		LsBank(LsMainWindow *parent);
		QList<QPointer<LsTable>> tables();
		bool hasTables();
		void removeWidget(LsWidget *widget);

	public Q_SLOTS:
		LsTable* addTable();
		LsTable* addTable(const QString &filePath);
		LsGraph* addGraph();
		LsGuage* addGuage();
		LsManometer* addManometer();
		LsNote* addNote();
		LsText* addText();
		LsThermometer* addThermometer();

	private:
		void addWidget(LsWidget *widget, int width, int height);

		/** store a list of tables */
		QList<QPointer<LsTable>> tablesCollection;

		/** the main window that contain act as user interface */
		LsMainWindow *window;

		/**
		 * Store the increment number of tables.
		 * when ever a widget is added, the number of used to keep a default name.
		 *  and crossponding number is incremented
		 */
		struct {
			uint table = 1, graph = 1;
		} inc;
};

#endif
