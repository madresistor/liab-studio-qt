/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_DATA_LOGGER_H
#define LIAB_STUDIO_DATA_LOGGER_H

#include <QObject>
#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include "bank.h"
#include "widgets/widget/widget.h"
#include "interfaces/interface/interface.h"

#include "ui_main_window.h"

class LsMainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		LsMainWindow(QWidget *parent = Q_NULLPTR);
		void addWidgetToMdi(LsWidget *widget, int width, int height);
		void removeWidgetFromMdi(LsWidget *widget);

	Q_SIGNALS:
		void aboutToClose();

	protected:
		void closeEvent(QCloseEvent *event);

	private Q_SLOTS:
		void addInterface();
		void import();
		void showPopup(const QPoint &pos);
		void showAbout();

	private:
		void addWidget(LsWidget *widget, int width, int height);
		void addStartupWidget();
		void arrangeNicely();

		/* store a common instance of bank for the session */
		LsBank *bank;

		Ui::LsMainWindow ui;
};

#endif
