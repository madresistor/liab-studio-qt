/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_WIDGETS_EXTRA_TITLE_LABEL_H
#define LIAB_STUDIO_WIDGETS_EXTRA_TITLE_LABEL_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QMouseEvent>

class LsTitleLabel : public QLabel
{
	Q_OBJECT

	public:
		LsTitleLabel(QWidget *parent = Q_NULLPTR);

	protected:
		void mouseDoubleClickEvent(QMouseEvent * event);

	Q_SIGNALS:
		/** emitted when user want to change title to @a newTitle
		 * @param newTitle New title
		 */
		void userWantToChange(const QString &newTitle);
};

#endif
