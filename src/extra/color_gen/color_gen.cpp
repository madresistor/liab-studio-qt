/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "color_gen.h"

/**
 * Return a visually appealing color
 * @return color
 */
QColor LsColorGen::color()
{
	/*
	 * values[0] : Hue (0 .. 360)
	 * values[1] : Saturation (0...1)
	 * values[2] : Value (0...1)
	 */
	qreal hue = (qrand() / (qreal) RAND_MAX);
	return QColor::fromHsvF(hue, 0.8f, 0.5f);
}
