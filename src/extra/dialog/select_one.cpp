/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "select_one.h"
#include "ui_select_one_dialog.h"

LsSelectOneDialog::LsSelectOneDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::LsSelectOneDialog)
{
	ui->setupUi(this);
}

/**
 * Add a entry
 * @param text Entry text
 * @return index of the entry
 */
int LsSelectOneDialog::addEntry(const QString &text)
{
	int index = m_entries.count();

	QRadioButton *rb = new QRadioButton(text, ui->itemsParent);
	ui->itemsLayout->setWidget(index, QFormLayout::SpanningRole, rb);

	m_entries.append(rb);

	if (!index) {
		rb->setChecked(true);
	}

	return index;

}

/**
 * Get the Radio button widget at index @a i
 * @param i Index
 * @return Radio button
 * @return Q_NULLPTR if out of range
 */
QRadioButton *LsSelectOneDialog::widget(int i)
{
	if (i >= 0 && i < m_entries.count()) {
		return m_entries.at(i);
	}

	return Q_NULLPTR;
}

void LsSelectOneDialog::setSelected(int i)
{
	if (i >= 0 && i < m_entries.count()) {
		m_entries.at(i)->setChecked(true);
	}
}

/**
 * @return index of selected item.
 * @return -1 if nothing is selected (corner case)
 */
int LsSelectOneDialog::selected()
{
	for (int i = 0; i < m_entries.count(); i++) {
		if (m_entries.at(i)->isChecked()) {
			return i;
		}
	}

	return -1;
}
