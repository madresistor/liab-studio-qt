/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include <QIcon>

LsMarkerModel::LsMarkerModel(QObject *parent) :
	QAbstractItemModel(parent)
{
}

int LsMarkerModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 1;
}

int LsMarkerModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return LsMarker::ICON.count();
}

QVariant LsMarkerModel::data(const QModelIndex &index, int role) const
{
	if (! index.isValid()) {
		return QVariant();
	}

	LsMarker::Type marker =
		static_cast<LsMarker::Type>(index.row());

	switch(role) {
	case Qt::UserRole:
		return QVariant((int) marker);
	break;
	case Qt::DecorationRole:
		return LsMarker::valueToIcon(marker);
	}

	return QVariant();
}

Qt::ItemFlags LsMarkerModel::flags(const QModelIndex &index) const
{
	Q_UNUSED(index);
	return Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
}

QVariant LsMarkerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(section);
	Q_UNUSED(orientation);
	Q_UNUSED(role);
	return QVariant();
}

QModelIndex LsMarkerModel::index(int row, int column, const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	Q_ASSERT(column == 0);
	return createIndex(row, column, (void *) 0);
}

QModelIndex LsMarkerModel::parent(const QModelIndex & index) const
{
	Q_UNUSED(index);
	return QModelIndex();
}
