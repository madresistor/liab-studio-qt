/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "marker.h"
#include <libreplot/extra/font.h>

static QString marker_icon(const char *name)
{
	return QString(":/data-logger/graph/marker/%1").arg(name);
}

/**
 * @note assume that marker start from 0 and increment by 1 for new one.
 */
static inline QMap<LsMarker::Type,QString> build_marker_icon_table()
{
	QMap<LsMarker::Type,QString> table;

	table.insert(LsMarker::PLUS, marker_icon("plus"));
	table.insert(LsMarker::CROSS, marker_icon("cross"));

	table.insert(LsMarker::CIRCLE_FILLED,
		marker_icon("circle-filled"));
	table.insert(LsMarker::RECTANGLE_FILLED,
		marker_icon("rectangle-filled"));
	table.insert(LsMarker::DIAMOND_FILLED,
		marker_icon("diamond-filled"));
	table.insert(LsMarker::TRIANGLE_UP_FILLED,
		marker_icon("triangle-up-filled"));
	table.insert(LsMarker::TRIANGLE_DOWN_FILLED,
		marker_icon("triangle-down-filled"));
	table.insert(LsMarker::TRIANGLE_LEFT_FILLED,
		marker_icon("triangle-left-filled"));
	table.insert(LsMarker::TRIANGLE_RIGHT_FILLED,
		marker_icon("triangle-right-filled"));
	table.insert(LsMarker::STAR_5SIDE_FILLED,
		marker_icon("star-5side-filled"));

	table.insert(LsMarker::CIRCLE_EMPTY,
		marker_icon("circle-empty"));
	table.insert(LsMarker::RECTANGLE_EMPTY,
		marker_icon("rectangle-empty"));
	table.insert(LsMarker::DIAMOND_EMPTY,
		marker_icon("diamond-empty"));
	table.insert(LsMarker::TRIANGLE_UP_EMPTY,
		marker_icon("triangle-up-empty"));
	table.insert(LsMarker::TRIANGLE_DOWN_EMPTY,
		marker_icon("triangle-down-empty"));
	table.insert(LsMarker::TRIANGLE_LEFT_EMPTY,
		marker_icon("triangle-left-empty"));
	table.insert(LsMarker::TRIANGLE_RIGHT_EMPTY,
		marker_icon("triangle-right-empty"));
	table.insert(LsMarker::STAR_5SIDE_EMPTY,
		marker_icon("star-5side-empty"));

	return table;
}

static inline QMap<LsMarker::Type,const char*> build_marker_text_table()
{
	QMap<LsMarker::Type,const char*> table;

	table.insert(LsMarker::PLUS, "+");
	table.insert(LsMarker::CROSS, "✕");

	table.insert(LsMarker::CIRCLE_FILLED, "●");
	table.insert(LsMarker::RECTANGLE_FILLED, "■");
	table.insert(LsMarker::DIAMOND_FILLED, "◆");
	table.insert(LsMarker::TRIANGLE_UP_FILLED, "▲");
	table.insert(LsMarker::TRIANGLE_DOWN_FILLED, "▼");
	table.insert(LsMarker::TRIANGLE_LEFT_FILLED, "◀");
	table.insert(LsMarker::TRIANGLE_RIGHT_FILLED, "▶");
	table.insert(LsMarker::STAR_5SIDE_FILLED, "★");

	table.insert(LsMarker::CIRCLE_EMPTY, "○");
	table.insert(LsMarker::RECTANGLE_EMPTY, "□");
	table.insert(LsMarker::DIAMOND_EMPTY, "◇");
	table.insert(LsMarker::TRIANGLE_UP_EMPTY, "△");
	table.insert(LsMarker::TRIANGLE_DOWN_EMPTY, "▽");
	table.insert(LsMarker::TRIANGLE_LEFT_EMPTY, "◁");
	table.insert(LsMarker::TRIANGLE_RIGHT_EMPTY, "▷");
	table.insert(LsMarker::STAR_5SIDE_EMPTY, "☆");

	return table;
}

const QMap<LsMarker::Type,const char*>
	LsMarker::TEXT = build_marker_text_table();

const QMap<LsMarker::Type,QString>
	LsMarker::ICON = build_marker_icon_table();

QHash<LsMarker::Type, lp_texture*> LsMarker::buildTexture(const QOpenGLWidget *w)
{
	QHash<Type, lp_texture*> textures;

	/* curves marker */
	lp_vec2f dpi;
	dpi.x = w->physicalDpiX();
	dpi.y = w->physicalDpiY();

	lp_font *font = lp_font_gen();

	QMapIterator<Type, const char *> iter(TEXT);
	while (iter.hasNext()) {
		iter.next();
		LsMarker::Type i = iter.key();
		const char *s = iter.value();
		lp_texture *t = lp_texture_gen_text((uint8_t *) s, 14, font, &dpi);
		textures.insert(i, t);
	}

	lp_font_del(font); /* we are done with font */

	return textures;
}

lp_texture *LsMarker::buildTexture(const QOpenGLWidget *w, Type type)
{
	/* curves marker */
	lp_vec2f dpi;
	dpi.x = w->physicalDpiX();
	dpi.y = w->physicalDpiY();

	lp_font *font = lp_font_gen();
	const char *s = TEXT.value(type);
	lp_texture *tex = lp_texture_gen_text((uint8_t *) s, 14, font, &dpi);
	lp_font_del(font);

	return tex;
}

void LsMarker::freeTexture(QHash<Type, lp_texture*> &list)
{
	Q_FOREACH(lp_texture *t, list) {
		lp_texture_del(t);
	}

	list.clear();
}
