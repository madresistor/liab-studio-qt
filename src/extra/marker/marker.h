/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_MARKER_H
#define LIAB_STUDIO_MARKER_H

#include <QString>
#include <QIcon>
#include <QMap>
#include <QHash>
#include <QOpenGLWidget>
#include <libreplot/extra/texture.h>

class LsMarker
{
	public:
		enum Type {
			PLUS = 0, /* + */
			CROSS = 1, /* ✕ */

			CIRCLE_FILLED = 2, /* ● */
			RECTANGLE_FILLED = 3, /* ■ */
			DIAMOND_FILLED = 4, /* ◆ */
			TRIANGLE_UP_FILLED = 5, /* ▲ */
			TRIANGLE_DOWN_FILLED = 7, /* ▼ */
			TRIANGLE_LEFT_FILLED = 7, /* ◀ */
			TRIANGLE_RIGHT_FILLED = 8, /* ▶ */
			STAR_5SIDE_FILLED = 9, /* ★ */

			CIRCLE_EMPTY = 10, /* ○ */
			RECTANGLE_EMPTY = 11, /* □ */
			DIAMOND_EMPTY = 12, /* ◇ */
			TRIANGLE_UP_EMPTY = 13, /* △ */
			TRIANGLE_DOWN_EMPTY = 14, /* ▽ */
			TRIANGLE_LEFT_EMPTY = 15, /* ◁ */
			TRIANGLE_RIGHT_EMPTY = 16, /* ▷ */
			STAR_5SIDE_EMPTY = 17, /* ☆ */
		};

		static const Type DEFAULT = PLUS;

		static const QMap<Type,const char*> TEXT;
		static const QMap<Type,QString> ICON;

		static QIcon valueToIcon(Type marker) {
			return ICON.contains(marker) ? QIcon(ICON.value(marker)) : QIcon();
		}

		static lp_texture *buildTexture(const QOpenGLWidget *w, Type type);
		static QHash<Type, lp_texture*> buildTexture(const QOpenGLWidget *w);
		static void freeTexture(QHash<Type, lp_texture*> &list);
};

#endif
