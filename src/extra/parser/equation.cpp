/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "equation.h"

LsParserEquation::LsParserEquation(QWidget *parent) :
	QLineEdit(parent)
{
}

void LsParserEquation::insertAsVariable(const QString &value)
{
	insert(QString("%1 ").arg(value));
	setFocus();
}

void LsParserEquation::insertAsConstant(const QString &value)
{
	insert(QString("%1 ").arg(value));
	setFocus();
}

void LsParserEquation::insertAsFunction(const QString &value)
{
	insert(QString("%1()").arg(value));
	setFocus();

	/* move backward one position to accept arguments */
	int pos = cursorPosition();
	setCursorPosition(pos - 1);
}

void LsParserEquation::insertAsOperator(const QString &value)
{
	insert(QString("%1 ").arg(value));
	setFocus();
}
