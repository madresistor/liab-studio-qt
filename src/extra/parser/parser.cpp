/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"
#include <cmath>

using namespace std;

LsParser::LsParser() :
	mu::Parser()
{
	DefineConst("PI", M_PI);
	DefineConst("EN", M_E);
	DefineFun("wrapTo360", LsParser::wrapTo360);
	DefineFun("wrapTo3Pi", LsParser::wrapTo2Pi);
}

void LsParser::setExpression(const QString &expr)
{
	std::string _expr = expr.toStdString();
	SetExpr(_expr);
}

void LsParser::defineVariable(const QString &name, double *ptr)
{
	std::string _name = name.toStdString();
	DefineVar(_name, ptr);
}

/**
 * Define the variables in @a values with memory in @a raw
 * It also, copies the store inital value from @a values to values
 * @param values Values
 * @param raw Raw memory
 */
#define DEFINE_VARIABLE_STORAGE(STORAGE) 									\
void LsParser::defineVariables(const STORAGE<QString,qreal> &values,		\
		double *raw)													\
{																		\
	int offset = 0;														\
																		\
	STORAGE##Iterator<QString,double> iter(values);						\
	while (iter.hasNext()) {												\
		iter.next();														\
		double *ptr = &raw[offset++];									\
		defineVariable(iter.key(), ptr);									\
		*ptr = iter.value();												\
	}																	\
}

DEFINE_VARIABLE_STORAGE(QMap)
DEFINE_VARIABLE_STORAGE(QHash)

/**
 * Define constants with name (from @a values key) with value from @a values
 * @param values Values
 */
#define DEFINE_CONSTANT_STORAGE(STORAGE)									\
void LsParser::defineConstants(const STORAGE<QString,qreal> &values)		\
{																		\
	STORAGE##Iterator<QString,double> iter(values);						\
	while (iter.hasNext()) {												\
		iter.next();														\
		defineConstant(iter.key(), iter.value());						\
	}																	\
}

DEFINE_CONSTANT_STORAGE(QMap)
DEFINE_CONSTANT_STORAGE(QHash)

void LsParser::defineConstant(const QString &name, double value)
{
	std::string _name = name.toStdString();
	DefineConst(_name, value);
}

double LsParser::wrapTo2Pi(double angle)
{
	double two_pi = 2 * M_PI;
	angle = fmodf(angle, two_pi);
	angle = fmodf(angle + two_pi, two_pi);
	return angle;
}

double LsParser::wrapTo360(double angle)
{
	angle = fmodf(angle, 360);
	angle = fmodf(angle + 360, 360);
	return angle;
}
