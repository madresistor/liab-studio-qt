/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "function.h"

#define FUNCTION_VARIABLE_ARGS UINT_MAX

#define FUNCTION_DATA_COUNT 28

static struct function_data_value {
	const char *name;
	uint arg;
	const char *desc;
} function_data[FUNCTION_DATA_COUNT] = {
	{"sin", 1, "Sine"},
	{"cos", 1, "Cosine"},
	{"tan", 1, "Tangent"},

	{"asin", 1, "Arcus Sine"},
	{"acos", 1, "Arcus Cosine"},
	{"atan", 1, "Arcus Tangent"},

	{"hsin", 1, "Hyperbolic Sine"},
	{"hcos", 1, "Hyperbolic Cosine"},
	{"htan", 1, "Hyperbolic Tangent"},

	{"asinh", 1, "Hyperbolic Arcus Tangent"},
	{"asinh", 1, "Hyperbolic Arcus Sine"},
	{"acosh", 1, "Hyperbolic Arcus Cosine"},
	{"atanh", 1, "Hyperbolic Arcus Tangent"},

	{"log2", 1, "logarithm to the base 2"},
	{"log10", 1, "logarithm to the base 10"},
	{"log", 1, "logarithm to the base 10"},
	{"ln", 1, "logarithm to base E (2.71828...)"},
	{"exp", 1, "E raised to the power of x"},

	{"sqrt", 1, "square root of a value"},
	/* TODO: we need pow() function */
	/* TODO: we need cbrt() function */
	/* TODO: we need mod() function */
	/* TODO: we need rand() function  [really?, usecase?] */

	{"sign", 1, "sign function -1 if x<0; 1 if x>0"},
	{"rint", 1, "round to nearest integer"},
	{"abs", 1, "absolute value"},

	{"min", FUNCTION_VARIABLE_ARGS, "Minimum of all inputs"},
	{"max", FUNCTION_VARIABLE_ARGS, "Maximum of all inputs"},
	{"sum", FUNCTION_VARIABLE_ARGS, "Sum of all inputs"},
	{"avg", FUNCTION_VARIABLE_ARGS, "Average (Mean) value of all inputs"},

	{"wrapTo2Pi", 1, "Wrap angle in radians to [0 2π]"},
	{"wrapTo360", 1, "Wrap angle in degree to [0 360]"},
};

LsParserFunction::LsParserFunction(QWidget *parent) :
	LsParserMenu(parent)
{
	for (uint i = 0; i < FUNCTION_DATA_COUNT; i++) {
		struct function_data_value *value = &function_data[i];
		addEntry(value->name, value->desc);
	}
}
