/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "constant.h"
#include <cmath>

using namespace std;

#define CONSTANT_DATA_COUNT 2

static struct constant_data_value {
	const char *name;
	const char *desc;
	bool irrational;
	double value;
} constant_data[CONSTANT_DATA_COUNT] = {
	{"EN", "Eular Number", true, M_E},
	{"PI", "π", true, M_PI},
};

LsParserConstant::LsParserConstant(QWidget *parent) :
	LsParserMenu(parent)
{
	for (uint i = 0; i < CONSTANT_DATA_COUNT; i++) {
		struct constant_data_value *value = &constant_data[i];
		QString desc = QString("%1 (%2%3)").arg(value->desc)
			.arg(value->value, 6).arg(value->irrational ? "..." : "");
		addEntry(value->name, desc);
	}
}
