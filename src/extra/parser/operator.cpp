/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "operator.h"

#define OPERATOR_DATA_COUNT 5

static struct operator_data_value {
	const char *name;
	int priority;
	const char *desc;
} operator_data[OPERATOR_DATA_COUNT] = {
	//{"=", -1, "Assignement"},
	//{"&&", 1, "Logical AND"},
	//{"||", 2, "Logical OR"},
	//{"<=", 4, "Less or Equal"},
	//{">=", 4, "Greater or Equal"},
	//{"!=", 4, "NOT Equal"},
	//{"==", 4, "Equal"},
	//{">", 4, "Greater than"},
	//{"<", 4, "Less than"},
	{"+", 5, "Addition"},
	{"-", 5, "Subtraction"},
	{"*", 6, "Multiplication"},
	{"/", 6, "Division"},
	{"^", 7, "Raise x to the power of y"},
};

LsParserOperator::LsParserOperator(QWidget *parent) :
	LsParserMenu(parent)
{
	for (uint i = 0; i < OPERATOR_DATA_COUNT; i++) {
		struct operator_data_value *value = &operator_data[i];
		addEntry(value->name, value->desc);
	}
}
