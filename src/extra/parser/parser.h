/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_PARSER_H
#define LIAB_STUDIO_PARSER_H

#include <muParser.h>
#include <QString>
#include <QHash>
#include <QMap>

class LsParser : public mu::Parser
{
	public:
		LsParser();

		void setExpression(const QString &expr);
		void defineVariable(const QString &name, double *ptr);
		void defineConstant(const QString &name, double value);

		void defineVariables(const QMap<QString,qreal> &values, double *raw);
		void defineVariables(const QHash<QString,qreal> &values, double *raw);
		void defineConstants(const QMap<QString,qreal> &values);
		void defineConstants(const QHash<QString,qreal> &values);

		static double wrapTo2Pi(double angle);
		static double wrapTo360(double angle);
};

#endif
