/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstract_item_view.h"

LsAbstractItemView::LsAbstractItemView(QWidget *parent) :
	QAbstractItemView(parent)
{
}

/**
 * Get the index of the selected row
 * @param view Target view
 * @return -1 if multiple rows are selected
 * @return -1 if no row selected
 * @return Row number (starting from 0)
 */
int LsAbstractItemView::selectedRow(QAbstractItemView *view)
{
	Q_ASSERT(view != Q_NULLPTR);

	/* http://stackoverflow.com/a/6840656/1500988 */
	QItemSelectionModel *select = view->selectionModel();
	if (select == Q_NULLPTR || !select->hasSelection()) {
		return -1;
	}

	QModelIndexList indexList = select->selectedIndexes();
	if (indexList.isEmpty()) {
		return -1;
	}

	int row = indexList[0].row();
	Q_FOREACH(const QModelIndex &i, indexList) {
		if (i.row() != row) {
			/* multiple row selected */
			return -1;
		}
	}

	return row;
}
