/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_EXTRA_MULTI_PROC_H
#define LIAB_STUDIO_EXTRA_MULTI_PROC_H

#if defined(USE_MULTI_PROC)
/* try reading: http://en.wikipedia.org/wiki/OpenMP */
# include <omp.h>
# define MULTI_PROC_OPENMP(x) _Pragma(#x)
# define MULTI_PROC_FOR_LOOP MULTI_PROC_OPENMP(omp parallel for)
#else
# define MULTI_PROC_OPENMP(x)
# define MULTI_PROC_FOR_LOOP
#endif

#endif
