/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fft.h"
#include <complex.h>
#include <fftw3.h>

size_t ls_fft_forward(float sampling_freq, float *in, size_t N,
	float *freq, float *amp)
{
	size_t N_2 = (N + 1) / 2;
	float freq_res = sampling_freq / N;
	fftwf_complex *out = fftwf_alloc_complex(N_2);
	fftwf_plan plan = fftwf_plan_dft_r2c_1d(N, in, out,
		/* FFTW_MEASURE  */ FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);

	fftwf_execute(plan);

	freq[0] = 0;
	freq[0] = cabsf(out[0]) / N;
	for (size_t i = 1; i < N_2; i++) {
		freq[i] = i * freq_res;
		amp[i] = cabsf(out[i]) / N_2;
	}

	fftwf_destroy_plan(plan);
	fftwf_free(out);

	return N_2;
}
