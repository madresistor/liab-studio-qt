/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2015-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "array.h"
#include <math.h>

/**
 * Find the average value of samples in @a data
 * @param data Data
 * @param count Number of samples in @a data
 * @param offset Number of samples to skip from start
 * @param stride Number of samples to skip for next sample
 * @return value
 * @return NAN if no data found
 */
float ls_array_avg(float *data, size_t count, size_t offset, size_t stride)
{
	float value = 0;
	size_t len = 0;

	for (size_t i = offset; i < count; i += stride) {
		len += 1;
		value += data[i];
	}

	return (len > 0) ? (value / len) : NAN;
}

/**
 * Find the range of @a min and @a max that is in @a x
 * @param min Minimum value
 * @param max Maximum value
 * @param data Data to search into
 * @param count Number of points in @a data
 * @param[out] result_first First index that fulfill the criteria
 * @param[out] result_last Last index that fulfill the criteria
 * @return true if a region is found
 */
bool lp_array_find_region(float min, float max,
	float *data, size_t count, ssize_t *result_first, ssize_t *result_last)
{
	ssize_t f = -1, l = -1;

	for (size_t i = 0; i < count; i++) {
		float d = data[i];

		if (f == -1) {
			if (d >= min) {
				f = i;
			}
		} else if (l == -1) {
			if (d >= max) {
				l = i;
			}
		} else {
			break;
		}
	}

	if (f == -1) {
		return false;
	}

	/* could not reach any last item */
	if (l == -1) {
		l = count - 1;
	}

	/* both points having same index do not make sense */
	if (f == l) {
		return false;
	}

	*result_first = f;
	*result_last = l;
	return true;
}
