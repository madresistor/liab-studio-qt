/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_SIGPROC_FFT_H
#define LIAB_STUDIO_SIGPROC_FFT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

/**
 * Perform FFT (Forward)
 * @param sampling_freq Sampling frequency of @a input
 * @param in Input data (cannot be NULL)
 * @param N Number of points in @a input
 * @param[out] freq Frequency (cannot be NULL)
 * @param[out] amp Amplitude (cannot be NULL)
 * @warning @a freq should be of length @a N / 2
 * @warning @a amp should be of length @a N / 2
 * @param Number of valid data points in @a freq and @a amp
 */
size_t ls_fft_forward(float sampling_freq, float *in, size_t N,
	float *freq, float *amp);

#ifdef __cplusplus
}
#endif

#endif
