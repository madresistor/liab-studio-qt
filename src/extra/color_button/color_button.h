/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_COLOR_BUTTON_H
#define LIAB_STUDIO_COLOR_BUTTON_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QColor>

class LsColorButton : public QPushButton
{
	Q_OBJECT

	Q_PROPERTY(QColor color
		READ color
		WRITE setColor
		NOTIFY colorChanged
		USER true)

	public:
		explicit LsColorButton(const QColor &color, QWidget *parent = Q_NULLPTR);
		explicit LsColorButton(QWidget *parent = Q_NULLPTR):
			LsColorButton(Qt::black, parent) {};
		QColor color();
		void setIconSize(const QSize &size);

	Q_SIGNALS:
		void colorChanged(const QColor &color);

	public Q_SLOTS:
		void setColor(const QColor &color);
		void chooseColor();

	private:
		void updateIcon();
		QColor m_color;
};

#endif
