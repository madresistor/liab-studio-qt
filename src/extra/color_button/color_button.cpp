/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "color_button.h"
#include <QColorDialog>

/* http://stackoverflow.com/a/283262/1500988 */

static const QSize DEF_ICON_SIZE(16, 16);

LsColorButton::LsColorButton(const QColor &color, QWidget *parent) :
	QPushButton(parent),
	m_color(color)
{
	setIconSize(DEF_ICON_SIZE);
	connect(this, SIGNAL(clicked()), SLOT(chooseColor()));
}

void LsColorButton::updateIcon()
{
	QPixmap pm(iconSize());
	pm.fill(m_color);
	setIcon(pm);
}

QColor LsColorButton::color()
{
	return m_color;
}

void LsColorButton::setColor(const QColor &color)
{
	if (m_color != color) {
		m_color = color;
		updateIcon();
		Q_EMIT colorChanged(color);
	}
}

void LsColorButton::chooseColor()
{
	QColorDialog colorDialog(m_color, this);
	if (colorDialog.exec() == QDialog::Accepted) {
		setColor(colorDialog.selectedColor());
	}
}

void LsColorButton::setIconSize(const QSize &size)
{
	QPushButton::setIconSize(size);
	updateIcon();
}
