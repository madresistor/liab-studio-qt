/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "draggable_widget.h"

LsDraggableWidget::LsDraggableWidget(QWidget *parent) :
	QWidget(parent)
{
}

void LsDraggableWidget::mousePressEvent(QMouseEvent *event)
{
	m_offset = event->pos();
	raise();
}

void LsDraggableWidget::mouseMoveEvent(QMouseEvent *event)
{
	if(event->buttons() & Qt::LeftButton) {
		move(mapToParent(event->pos() - m_offset));
	}
}

void LsDraggableWidget::move(const QPoint &pos)
{
	QWidget *p = parentWidget();

	if (p == Q_NULLPTR) {
		QWidget::move(pos);
		return;
	};

	QSize ps = p->size();
	QSize s = size();

	int x = qMax(0, pos.x());
	int y = qMax(0, pos.y());

	if ((x + s.width()) > ps.width()) {
		x -= x + s.width() - ps.width();
	}

	if ((y + s.height()) > ps.height()) {
		y -= y + s.height() - ps.height();
	}

	QWidget::move(QPoint(x, y));
}

void LsDraggableWidget::mouseReleaseEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
	m_offset = QPoint();
}
