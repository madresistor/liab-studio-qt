/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_ABOUT_H
#define LIAB_STUDIO_ABOUT_H

#include <QDialog>
#include <QObject>
#include <QWidget>

#include "ui_about.h"

class LsAbout : public QDialog
{
	Q_OBJECT

	public:
		LsAbout(QWidget *parent = Q_NULLPTR);

	private:
		Ui::LsAbout ui;
};

#endif
