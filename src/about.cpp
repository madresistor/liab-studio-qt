/*
 * This file is part of liab-studio-qt.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-qt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-qt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-qt.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "about.h"
#include <QSysInfo>

LsAbout::LsAbout(QWidget *parent) :
	QDialog(parent)
{
	ui.setupUi(this);

	const char *info_page =
		"<html>"
		"<body>"
			"<div><b>Version</b>: %1<div>"
			"<div><b>Qt</b>: %2<div>"
			"<div><b>Platform</b>: %3<div>"
		"</body>"
		"</html>";

	QString info = QString(info_page)
		.arg(LIAB_STUDIO_VERSION)
		.arg(qVersion())
		.arg(QSysInfo::prettyProductName());

	ui.info->setText(info);
}
