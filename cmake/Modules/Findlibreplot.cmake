# - Try to find libreplot
#
#  LIBREPLOT_FOUND - system has libreplot
#  LIBREPLOT_INCLUDE_DIRS - the libreplot include directory
#  LIBREPLOT_LIBRARIES - Link these to use libreplot
#  LIBREPLOT_DEFINITIONS - Compiler switches required for using libreplot
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(LIBREPLOT_INCLUDE_DIR
	NAMES libreplot/libreplot.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "libreplot.so"
FIND_LIBRARY(LIBREPLOT_LIBRARY
	NAMES replot
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (LIBREPLOT_INCLUDE_DIR AND LIBREPLOT_LIBRARY)
	SET(LIBREPLOT_FOUND TRUE)
	SET(LIBREPLOT_INCLUDE_DIRS ${LIBREPLOT_INCLUDE_DIR})
	SET(LIBREPLOT_LIBRARIES ${LIBREPLOT_LIBRARY})
ENDIF (LIBREPLOT_INCLUDE_DIR AND LIBREPLOT_LIBRARY)

IF (LIBREPLOT_FOUND)
	IF (NOT libreplot_FIND_QUIETLY)
		MESSAGE(STATUS "Found libreplot:")
		MESSAGE(STATUS " - Includes: ${LIBREPLOT_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${LIBREPLOT_LIBRARIES}")
	ENDIF (NOT libreplot_FIND_QUIETLY)
ELSE (LIBREPLOT_FOUND)
	IF (libreplot_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find libreplot")
	ENDIF (libreplot_FIND_REQUIRED)
ENDIF (LIBREPLOT_FOUND)

# show the LIBREPLOT_INCLUDE_DIRS and LIBREPLOT_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(LIBREPLOT_INCLUDE_DIRS LIBREPLOT_LIBRARIES)
