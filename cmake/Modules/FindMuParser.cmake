# - Try to find MuParser
# Once done this will define
#
#  MUPARSER_FOUND - system has MuParser
#  MUPARSER_INCLUDE_DIRS - the MuParser include directory
#  MUPARSER_LIBRARIES - Link these to use MuParser
#  MUPARSER_DEFINITIONS - Compiler switches required for using MuParser
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(MUPARSER_INCLUDE_DIR
	NAMES muParser.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
	PATH_SUFFIXES
		muParser
)

FIND_LIBRARY(MUPARSER_LIBRARY
	NAMES muparser
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (MUPARSER_INCLUDE_DIR AND MUPARSER_LIBRARY)
	SET(MUPARSER_FOUND TRUE)
	SET(MUPARSER_INCLUDE_DIRS ${MUPARSER_INCLUDE_DIR})
	SET(MUPARSER_LIBRARIES ${MUPARSER_LIBRARY})
ENDIF (MUPARSER_INCLUDE_DIR AND MUPARSER_LIBRARY)

IF (MUPARSER_FOUND)
	IF (NOT MuParser_FIND_QUIETLY)
		MESSAGE(STATUS "Found MuParser:")
		MESSAGE(STATUS " - Includes: ${MUPARSER_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${MUPARSER_LIBRARIES}")
	ENDIF (NOT MuParser_FIND_QUIETLY)
ELSE (MUPARSER_FOUND)
	IF (MuParser_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find MuParser")
	ENDIF (MuParser_FIND_REQUIRED)
ENDIF (MUPARSER_FOUND)

MARK_AS_ADVANCED(MUPARSER_INCLUDE_DIRS MUPARSER_LIBRARIES)
