# Find libcsv
# ~~~~~~~~
# Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
# Copyright (c) 2010 Tim Sutton <tim at linfiniti.com>
# Copyright (c) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
#
# Once run this will define:
# LIBCSV_FOUND        = system has CSV lib
# LIBCSV_LIBRARIES    = full path to the CSV library
# LIBCSV_INCLUDE_DIRS = where to find headers
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(LIBCSV_INCLUDE_DIR
	NAMES csv.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

FIND_LIBRARY(LIBCSV_LIBRARY
	NAMES csv
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (LIBCSV_INCLUDE_DIR AND LIBCSV_LIBRARY)
	SET(LIBCSV_FOUND TRUE)
	SET(LIBCSV_INCLUDE_DIRS ${LIBCSV_INCLUDE_DIR})
	SET(LIBCSV_LIBRARIES ${LIBCSV_LIBRARY})
ENDIF (LIBCSV_INCLUDE_DIR AND LIBCSV_LIBRARY)

IF (LIBCSV_FOUND)
	IF (NOT libcsv_FIND_QUIETLY)
		MESSAGE(STATUS "Found libcsv:")
		MESSAGE(STATUS " - Includes: ${LIBCSV_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${LIBCSV_LIBRARIES}")
	ENDIF (NOT libcsv_FIND_QUIETLY)
ELSE (LIBCSV_FOUND)
	IF (libcsv_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find libcsv")
	ENDIF (libcsv_FIND_REQUIRED)
ENDIF (LIBCSV_FOUND)

MARK_AS_ADVANCED(LIBCSV_INCLUDE_DIRS LIBCSV_LIBRARIES)
